tdaq_package()

remove_definitions(-DERS_NO_DEBUG)

tdaq_generate_dal(data/schema/emon.schema.xml data/schema/athena-mon.schema.xml
  NAMESPACE emon::dal 
  INCLUDE emon/dal
  INCLUDE_DIRECTORIES dal
  CPP cpp 
  JAVA java
  CPP_OUTPUT dal_cpp_srcs 
  JAVA_OUTPUT dal_java_srcs
  NOINSTALL)

tdaq_add_header_directory(${CMAKE_CURRENT_BINARY_DIR}/emon/dal DESTINATION emon PATTERN *.h)

tdaq_add_schema(data/schema/emon.schema.xml)
tdaq_add_schema(data/schema/athena-mon.schema.xml)

tdaq_generate_isinfo(emon_ISINFO data/emon.xml
  CPP_OUTPUT is_cpp_srcs 
  JAVA_OUTPUT is_java_srcs 
  OUTPUT_DIRECTORY emon
  NAMED SIMPLE
  PACKAGE emon)

tdaq_add_is_schema(data/emon.xml)

tdaq_add_library(emon-dal DAL
  ${dal_cpp_srcs}
  INCLUDE_DIRECTORIES emon
  LINK_LIBRARIES daq-core-dal config)

tdaq_add_library(emon 
  idl/emon.idl
  src/*.cc
  ${is_cpp_srcs}
  IDL_DEPENDS ipc
  LINK_LIBRARIES emon-dal is config TBB)

tdaq_add_library(emonpy python/*.cxx
  LINK_LIBRARIES emon tdaq-common::eformat Boost::python Python::Development)

set(common_linkopts emon emon-dal cmdline)

tdaq_add_executable(emon_conductor 
  bin/*.cc 
  LINK_LIBRARIES ${common_linkopts} pmgsync)

tdaq_add_executable(emon_push_sampler
  examples/PushSamplerMain.cc
  LINK_LIBRARIES ${common_linkopts} tdaq-common::DataReader tdaq-common::eformat)

tdaq_add_executable(emon_pull_sampler
  examples/PullSamplerMain.cc
  LINK_LIBRARIES ${common_linkopts} tdaq-common::DataReader tdaq-common::eformat)

tdaq_add_executable(emon_task
  examples/EventMonitorMain.cc
  LINK_LIBRARIES ${common_linkopts})

tdaq_add_jar(jemon
  idl/emon.idl
  jsrc/emon/*.java
  ${is_java_srcs}
  ${dal_java_srcs}
  IDL_DEPENDS ipc
  INCLUDE_JARS dal/dal.jar is/is.jar ipc/ipc.jar Jers/ers.jar config/config.jar TDAQExtJars/external.jar
  OUTPUT_NAME emon)

tdaq_add_python_package(emon)

tdaq_add_data(data/*.data.xml DESTINATION examples)
tdaq_add_examples(examples/*.cc examples/*.h)
tdaq_add_examples(jexamples/*.java)
