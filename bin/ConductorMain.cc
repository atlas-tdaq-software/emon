//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    ConductorMain.cc
//    Creates Conductor_impl and publishes it in IPC
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Nov. 28 2006
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <cmdl/cmdargs.h>

#include <ipc/core.h>
#include <ipc/signal.h>

#include <emon/exceptions.h>
#include <emon/Conductor_impl.h>

#include <pmg/pmg_initSync.h>

int main( int argc, char ** argv ) {
	
    try {
	std::list< std::pair< std::string, std::string > > opt = IPCCore::extractOptions( argc, argv );
	opt.push_front( std::make_pair( std::string("clientCallTimeOutPeriod"), std::string("5000") ) );
	opt.push_front( std::make_pair( std::string("threadPerConnectionPolicy"), std::string("0") ) );
        IPCCore::init( opt );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::fatal( ex );
        return 1;
    }
    
    // Take care about command line arguments
    CmdArgStr	partition_name ( 'p', "partition", "partition-name", "partition to work in." );
    CmdArgBool  pmg_sync ('s', "sync", "use PMG synchronisation.");

    CmdLine cmd( *argv, &partition_name, &pmg_sync, NULL );
    CmdArgvIter arg_iter( --argc, ++argv );    
    cmd.description(	"Event Monitoring application which manages connections between event samplers and monitoring tasks."
    			"Each partition must have exactly one emon conductor application running." );

    cmd.parse( arg_iter );

    IPCPartition partition( partition_name );

    // Check if there is already a Conductor running in this partition
    if ( emon::Conductor_impl::isExist( partition ) ) {
        ers::fatal( emon::Exception( ERS_HERE, "Another Emon Conductor has already been started in this partition." ) );
        return 1;
    }
    
    emon::Conductor_impl * conductor = 0;

    try {
	conductor = new emon::Conductor_impl( partition );
    }
    catch( emon::Exception & ex ) {
        ers::fatal(ex);
        return 2;
    }

    if ( pmg_sync ) {
    	pmg_initSync();
    }

    ERS_LOG( "Emon conductor for partition \"" << partition.name() << "\" has been started." );

    int sig = daq::ipc::signal::wait_for();

    ERS_LOG( "Signal " << sig << " received, stopping emon conductor ... " );

    conductor -> _destroy();

    ERS_LOG( "done" );

    return 0;
}
