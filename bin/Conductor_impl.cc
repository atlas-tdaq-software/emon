////////////////////////////////////////////
//    Conductor_impl.cc
//    Implementation of the Emon Conductor
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Nov. 28 2006       
////////////////////////////////////////////

#include <ipc/exceptions.h>
#include <ipc/signal.h>

#include <emon/Conductor_impl.h>
#include <emon/Util.h>
#include <emon/exceptions.h>

namespace emon {
    std::ostream& operator<<(std::ostream &out, const EventMonitoring::SamplingAddress &address) {
        out << (const char*) address.type << "[";
        if (address.names.length()) {
            for (size_t i = 0; i < address.names.length(); ++i) {
                out << (const char*) address.names[i];
                if (i != address.names.length() - 1) {
                    out << ",";
                }
            }
        } else {
            out << address.number;
        }
        out << "]";
        return out;
    }
}

using namespace emon;

/*!
 * This will ping all samplers and monitors currently known
 * to this Conductor, to see whether they are still alive or not.
 * If a ping fails then the corresponding object will be removed.
 * \return always returns true
 */
bool Conductor_impl::pingLoop() {
    ERS_DEBUG(1, "Start validation procedure");

    std::unique_lock lock(m_mutex);
    SamplerTypes samplers = m_samplers;

    lock.unlock();
    for (SamplerTypes::iterator it = samplers.begin(); it != samplers.end(); ++it) {
        it->second->validate();
    }

    ERS_DEBUG(1, "Finished validation");
    return true;
}

void Conductor_impl::updateStatistics() {
    std::unique_lock lock(m_mutex);
    m_active_samplers = m_active_monitors = m_active_channels = 0;
    for (SamplerTypes::iterator it = m_samplers.begin(); it != m_samplers.end(); ++it) {
        m_active_samplers += it->second->samplersNumber();
        m_active_channels += it->second->channelsNumber();
        m_active_monitors += it->second->monitorsNumber();
    }
}

/*!
 * Creates a new Conductor_impl object, using the specified parameters.
 * The constructor does some initialisation of values in IS server and takes care
 * of all necessary initialisation. The constructor will also care about
 * obtaining information about all eventually running EventMonitoring applications.
 * \param p the IPCPartition to work in
 */
Conductor_impl::Conductor_impl(const IPCPartition &partition) :
        IPCNamedObject<POA_EventMonitoring::Conductor>(partition, EventMonitoring::Conductor::Name),
        ConductorInfoNamed(partition, std::string(EventMonitoring::StatisticsServerName)
                                      + "." + EventMonitoring::Conductor::Name),
        InfoPublisher(*this, 10), m_ping(60, std::bind(&Conductor_impl::pingLoop, this))
{
    std::map<std::string, EventMonitoring::EventSampler_var> samplers;
    try {
        partition.getObjects<
            EventMonitoring::EventSampler, ipc::no_cache, ipc::non_existent>(samplers);
    } catch (daq::ipc::InvalidPartition &ex) {
        throw Exception( ERS_HERE, ex);
    }

    for (auto it = samplers.begin(); it != samplers.end(); ++it) {
        try {
            EventMonitoring::SamplingAddress_var address;
            it->second->get_address(address);
            registerSampler(address, it->second, true);
        }
        catch (CORBA::SystemException &ex) {
            ERS_DEBUG(0, "Can't register the '" << it->first << "' sampler : " << ex._name());
        }
    }

    try {
        publish();
    }
    catch (daq::ipc::Exception &ex) {
        throw Exception( ERS_HERE, ex);
    }

    updateStatistics();
}

Conductor_impl::~Conductor_impl() {
    try {
        withdraw();
    } catch (daq::ipc::Exception &ex) {
        ers::warning(ex);
    }
}

void Conductor_impl::registerSampler(const EventMonitoring::SamplingAddress &address,
        EventMonitoring::EventSampler_ptr sampler, bool restore) {
    std::string name = construct_name(address);
    std::string type(address.type);

    std::unique_lock lock(m_mutex);

    SamplerTypes::iterator it = m_samplers.find(type);
    if (it == m_samplers.end()) {
        it = m_samplers.insert(
                std::make_pair(type, SamplerClass::SharedPtr(new SamplerClass(type)))).first;
    }

    it->second->addSampler(address, sampler, restore);
    ++m_active_samplers;

    ERS_LOG("Sampler '" << name << "' has been registered, total number of samplers = "
            << m_active_samplers);
}

/*!
 * This will register a new EventSampler in the Conductors local list.
 * This is an IPC published method.
 * \param address the sampling address of the calling EventSampler
 * \param sampler a CORBA object reference to the calling EventSampler
 */
void Conductor_impl::add_sampler(const EventMonitoring::SamplingAddress &address,
        EventMonitoring::EventSampler_ptr sampler) {
    registerSampler(address, sampler);
}

/*!
 * This method may be invoked either by SamplerTypes themselves or by the thread
 * responsible for pinging all SamplerTypes in the Conductor in periodic intervals.
 * In any case it notifies the Conductor about the exit of an EventSampler, so it can
 * delete the EventSampler from its local list, delete the Monitoring Tree
 * and tell the root Monitoring Task that the Event Sampler has exited.
 * This is an IPC published method.
 * \param address the sampling address of the EventSampler to be deleted
 * \param sampler a CORBA object reference to the EventSampler to be deleted
 */
void Conductor_impl::remove_sampler(const EventMonitoring::SamplingAddress &address) {
    std::string name = construct_name(address);
    ERS_LOG("Request to disconnect sampler '" << name << "' received");

    std::string type(address.type);

    std::unique_lock lock(m_mutex);
    SamplerTypes::iterator it = m_samplers.find(type);

    if (it == m_samplers.end()) {
        ERS_LOG("Event Sampler '" << name << "' is not registered");
        throw EventMonitoring::NotFound();
    }

    it->second->removeSampler(address);
    --m_active_samplers;
    ERS_LOG("Sampler '" << name << "' has been disconnected, total number of samplers is "
            << m_active_samplers);
}

/*!
 * This cancels a subscription of a Monitoring Task. This method
 * takes care of cancellation of the root subscription in the EventSampler.
 * This method might be called by Monitoring Tasks themselves having determined, that a child
 * has exited or by the SamplerTypes having noticed the crash of a Monitor Task.
 * This is an IPC published method.
 * \param address the sampling address the monitor was connected to
 * \param criteria the selection criteria of this subscription
 * \param group_name the group name shared by monitors that want to receive different events from the same criteria
 * \param monitor the CORBA object reference of the Monitoring Task, that shall be removed
 */
void Conductor_impl::remove_monitor(const EventMonitoring::SamplingAddress &address,
        const EventMonitoring::SelectionCriteria &criteria, const char *group_name,
        EventMonitoring::EventMonitor_ptr monitor)
{
    std::string monid = IPCCore::objectToString(monitor, IPCCore::Corbaloc);
    ERS_LOG("Disconnecting " << monid << " monitor from " << address << " sampler(s)");

    std::string type(address.type);

    SamplerClass::SharedPtr sampler_type;
    {
        std::unique_lock lock(m_mutex);
        SamplerTypes::iterator it = m_samplers.find(type);

        if (it != m_samplers.end()) {
            sampler_type = it->second;
        } else {
            ERS_DEBUG(0, "There are no Event Samplers of '" << type << "' type available");
            throw EventMonitoring::NotFound();
        }
    }

    sampler_type->removeMonitor(address, criteria, group_name, monitor);

    m_active_monitors--;
    ERS_LOG("Monitor " << monid << " has been disconnected from the " << address
            << " sampler(s), total number of monitors = " << m_active_monitors);
}

/*! This either sends a new root subscription to the appropriate EventSampler
 * or attaches the calling Monitoring Task to an already existing Monitoring Tree.
 * It will also update some status information about the EventMonitoring Conductor in the IS server.
 * This is a CORBA IDL declared method.
 * \param address the sampling address of the EventSampler the Monitoring Task wants to sample events from
 * \param criteria the selection criteria, that all sampled events shall satisfy
 * \param group_name the group name shared by monitors that want to receive different events from the same criteria
 * \param monitor a CORBA object reference to the calling Monitoring Task
 */
void Conductor_impl::add_monitor(EventMonitoring::SamplingAddress &address,
        const EventMonitoring::SelectionCriteria &criteria, const char *group_name,
        EventMonitoring::EventMonitor_ptr monitor)
{
    std::string monid = IPCCore::objectToString(monitor, IPCCore::Corbaloc);
    ERS_LOG("Connecting " << monid << " monitor to " << address << " sampler(s)");

    std::string type(address.type);

    SamplerClass::SharedPtr sampler_type;

    {
        std::unique_lock lock(m_mutex);
        SamplerTypes::iterator it = m_samplers.find(type);

        if (it != m_samplers.end()) {
            sampler_type = it->second;
        } else {
            ERS_LOG("There are no Event Samplers of '" << type << "' type available");
            throw EventMonitoring::BadAddress();
        }
    }
    sampler_type->addMonitor(address,
            MonitorNode::SharedPtr(new MonitorNode(criteria, std::string(group_name), monitor)));

    m_active_monitors++;
    ERS_LOG("Monitor " << monid << " has been connected to the " << address
            << " sampler(s), total number of monitors = " << m_active_monitors);
}

/*!
 * \param p the partition in which to look for Conductor objects
 */
bool Conductor_impl::isExist(const IPCPartition &p) {
    try {
        return p.isObjectValid<EventMonitoring::Conductor>(EventMonitoring::Conductor::Name);
    } catch (daq::ipc::Exception &ex) {
        return false;
    }
}

void Conductor_impl::shutdown() {
    daq::ipc::signal::raise();
}
