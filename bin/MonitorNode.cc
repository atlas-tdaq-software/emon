//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    MonitorNode.cc
//    Implementation of the MonitorNode object the conductor uses for tree management
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Nov. 28 2006       
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <emon/MonitorNode.h>
#include <emon/Conductor_impl.h>
#include <emon/Util.h>

namespace emon {
    extern std::ostream& operator<<(std::ostream &out,
            const EventMonitoring::SamplingAddress &address);
}

using namespace emon;

/*!
 * Creates a new MonitorNode representing a Monitoring Task
 * \param monitor a CORBA object reference to the associated Monitoring Task
 */
MonitorNode::MonitorNode(const EventMonitoring::SelectionCriteria &criteria,
        const std::string group_name, EventMonitoring::EventMonitor_ptr monitor) :
        m_criteria(criteria),
        m_group_name(group_name),
        m_monitor(EventMonitoring::EventMonitor::_duplicate(monitor)),
        m_exception(new EventMonitoring::BadAddress())
{ }

MonitorNode::MonitorNode(const EventMonitoring::MonitorInfo &info) :
        m_criteria(info.criteria),
        m_group_name(info.group_name),
        m_monitor(EventMonitoring::EventMonitor::_duplicate(info.reference)),
        m_exception(new EventMonitoring::BadAddress())
{ }

bool MonitorNode::ping() const {
    try {
        m_monitor->ping();
        return true;
    } catch (...) {
        return false;
    }
}

void MonitorNode::samplerExit(const std::string &name) {
    if (CORBA::is_nil(m_monitor)) {
        return;
    }

    try {
        std::string n = "D:" + name;
        m_monitor->sampler_exit(n.c_str());
    } catch (CORBA::Exception &ex) {
        ERS_DEBUG(1, "Can't inform monitor about the sampler death: "<< ex._name());
    }
}

void MonitorNode::samplerEnter(const std::string &name) {
    if (CORBA::is_nil(m_monitor)) {
        return;
    }

    try {
        std::string n = "R:" + name;
        m_monitor->sampler_exit(n.c_str());
    } catch (CORBA::Exception &ex) {
        ERS_DEBUG(1, "Can't inform monitor about the sampler resurrection: "<< ex._name());
    }
}

bool MonitorNode::connect(const EventMonitoring::EventSampler_var &sampler,
        const EventMonitoring::SamplingAddress &address) {
    try {
        sampler->connect_monitor(m_criteria, m_group_name.c_str(), m_monitor);
        std::unique_lock lock(m_mutex);
        m_samplers_connected.type = address.type;
        m_samplers_connected.names.length(m_samplers_connected.names.length() + 1);
        m_samplers_connected.names[m_samplers_connected.names.length() - 1] = address.names[0];
        ERS_DEBUG(1, "Monitor is connected to " << m_samplers_connected << " samplers");
        return true;
    }
    catch (EventMonitoring::AlreadyConnected &ex) {
        ERS_LOG("This monitor is already connected to the given sampler");
    }
    catch (EventMonitoring::NoResources &ex) {
        ERS_LOG("Can't connect to the sampler, the event channels limit is reached " << ex.limit);
        std::unique_lock lock(m_mutex);
        m_exception.reset(new EventMonitoring::NoResources(ex));
    }
    catch (EventMonitoring::BadCriteria &ex) {
        ERS_LOG("Can't connect to the sampler, invalid selection criteria was provided. ");
        std::unique_lock lock(m_mutex);
        m_exception.reset(new EventMonitoring::BadCriteria(ex));
    }
    catch (CORBA::SystemException &ex) {
        ERS_LOG("Can't attach Monitoring Task to Monitoring Tree: " << ex._name());
        std::unique_lock lock(m_mutex);
        m_exception.reset(new EventMonitoring::BadAddress());
    }
    return false;
}

bool MonitorNode::disconnect(const EventMonitoring::EventSampler_var &sampler,
        EventMonitoring::EventMonitor_ptr monitor) {
    try {
        ERS_DEBUG(3, "disconnecting from the sampler");
        sampler->disconnect_monitor(m_criteria, m_group_name.c_str(), monitor);
        ERS_DEBUG(3, "successfully disconnected");
    }
    catch (EventMonitoring::NotFound &ex) {
        ERS_DEBUG(1, "not connected");
    }
    catch (CORBA::SystemException &ex) {
        ERS_DEBUG(1, "can't remove: " << ex._name());
    }

    return true;
}
