//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    SamplerClass.cc
//    Implementation of the SamplerClass object the conductor uses for tree management
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Mar. 8 2008       
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <ipc/core.h>

#include <emon/SamplerClass.h>
#include <emon/Util.h>

namespace emon {
    extern std::ostream& operator<<(std::ostream &out,
            const EventMonitoring::SamplingAddress &address);
}

using namespace emon;

/*!
 * Creates a new SamplerClass objects that will represents samplers of a certain type
 * \param name Sampler type name
 */
SamplerClass::SamplerClass(const std::string &name) :
        m_name(name), m_thread_pool(10) {
}

size_t SamplerClass::samplersNumber() const {
    std::unique_lock lock(m_mutex);
    return m_samplers.size();
}

size_t SamplerClass::monitorsNumber() const {
    std::unique_lock lock(m_mutex);

    std::unordered_set<MonitorNode::SharedPtr> monitors;
    for (auto it = m_samplers.begin(); it != m_samplers.end(); ++it) {
        it->second->getMonitors(monitors);
    }
    return monitors.size();
}

size_t SamplerClass::channelsNumber() const {
    std::unique_lock lock(m_mutex);
    std::unordered_set<std::string> channels;
    for (auto it = m_samplers.begin(); it != m_samplers.end(); ++it) {
        it->second->getChannels(channels);
    }
    return channels.size();
}

void SamplerClass::removeOne(const std::string &name,
        const EventMonitoring::SelectionCriteria &criteria, std::string group_name,
        EventMonitoring::EventMonitor_ptr monitor) {
    ERS_DEBUG(1, "Removing monitoring task from the '" << name << "' sampler");

    std::unique_lock lock(m_mutex);
    SamplersMap::iterator it = m_samplers.find(name);
    if (it == m_samplers.end()) {
        ERS_LOG("Sampler '" << name << "' is not found");
    } else {
        SamplerNode::SharedPtr s = it->second;
        lock.unlock();
        s->removeMonitor(criteria, group_name, monitor);
    }
}

/*
 * \param address address criteria for the monitoring task to be removed
 * \param criteria selection criteria for the monitoring task to be removed
 * \param group_name the group name shared by monitors that want to receive different events from the same criteria 
 * \param child the child Monitoring Task to remove from this Monitoring Tree
 */
void SamplerClass::removeMonitor(const EventMonitoring::SamplingAddress &address,
        const EventMonitoring::SelectionCriteria &criteria, std::string group_name,
        EventMonitoring::EventMonitor_ptr monitor)
{
    ERS_LOG("Request to remove monitor connected to " << address << " with criteria " << criteria);

    for (size_t i = 0; i < address.names.length(); ++i) {
        try {
            removeOne(std::string(address.names[i]), criteria, group_name, monitor);
        } catch (CORBA::UserException &ex) {
            ERS_DEBUG(1, "Got exception " << ex._name());
        }
    }
    ERS_LOG("Monitoring Task has been successfully removed");
}

void SamplerClass::addSampler(const EventMonitoring::SamplingAddress &address,
        EventMonitoring::EventSampler_ptr sampler_ptr, bool restore) {
    std::string name(address.names[0]);

    SamplerNode::SharedPtr existingSampler;
    {
        std::unique_lock lock(m_mutex);
        SamplersMap::iterator it = m_samplers.find(name);

        if (it != m_samplers.end()) {
            if (it->second->isValid()) {
                ERS_LOG("Event Sampler '" << name << "' already registered");
                throw EventMonitoring::SamplerAlreadyExists();
            } else {
                it->second->resurrect(sampler_ptr);
            }
        } else {
            auto zit = m_zombie_samplers.find(name);

            if (zit != m_zombie_samplers.end()) {
                zit->second->resurrect(sampler_ptr);
                m_samplers[name] = zit->second;
                m_zombie_samplers.erase(zit);
                return;
            }

            if (restore) {
                std::unordered_set<MonitorNode::SharedPtr> monitors;
                for (auto it = m_samplers.begin(); it != m_samplers.end(); ++it) {
                    it->second->getMonitors(monitors);
                }
                m_samplers[name] = SamplerNode::SharedPtr(
                        new SamplerNode(address, sampler_ptr, monitors));
            } else {
                m_samplers[name] = SamplerNode::SharedPtr(new SamplerNode(address, sampler_ptr));
            }
            ERS_DEBUG(1, "Event Sampler '" << name << "' has been registered");
        }
    }
}

void SamplerClass::removeSampler(const EventMonitoring::SamplingAddress &address) {
    std::string name(address.names[0]);
    ERS_DEBUG(1, "Request to disconnect sampler '" << name << "' received");

    std::unique_lock lock(m_mutex);
    SamplersMap::iterator sampler = m_samplers.find(name);

    if (sampler != m_samplers.end()) {
        sampler->second->samplerExit();
        m_zombie_samplers[sampler->first] = sampler->second;
        m_samplers.erase(sampler);
        ERS_DEBUG(1, "Event Sampler '" << name << "' was removed");
    } else {
        ERS_DEBUG(1, "Event Sampler '" << name << "' is not registered");
        throw EventMonitoring::NotFound();
    }
}

void SamplerClass::addMonitor(EventMonitoring::SamplingAddress &address,
        MonitorNode::SharedPtr monitor) {
    ERS_LOG("Request to add new monitor to " << address << " with criteria " << monitor->criteria());

    if (address.names.length()) {
        ERS_DEBUG(1, "Connect monitor to the given names");
        for (size_t i = 0; i < address.names.length(); ++i) {
            std::string name(address.names[i]);
            std::unique_lock lock(m_mutex);
            SamplersMap::iterator it = m_samplers.find(name);
            if (it != m_samplers.end()) {
                SamplerNode::SharedPtr s = it->second;
                lock.unlock();
                s->addMonitor(monitor, false);
            }
        }
    } else {
        if (!address.number) {
            throw EventMonitoring::BadAddress();
        }

        ERS_DEBUG(1, "Connect monitor to " << address.number << " sampler(s)");

        bool unique_channel = true;
        std::unique_lock lock(m_mutex);
        while (monitor->samplersConnected().names.length() != (uint32_t)address.number) {

            std::multimap<size_t, SamplerNode::SharedPtr> samplers_sorted;
            for (SamplersMap::iterator it = m_samplers.begin(); it != m_samplers.end(); ++it) {
                if (unique_channel) {
                    samplers_sorted.insert(std::make_pair(it->second->channelsNumber(), it->second));
                } else {
                    samplers_sorted.insert(std::make_pair(it->second->monitorsNumber(monitor), it->second));
                }
            }

            unsigned short counter = 0;
            for (auto it = samplers_sorted.begin(); it != samplers_sorted.end(); ++it) {
                m_thread_pool.addJob(std::bind(&SamplerNode::addMonitor, it->second, monitor, unique_channel));
                ++counter;
                ERS_DEBUG(1, "The monitor connection has been initiated, total counter is " << counter);

                if (counter == (unsigned short) address.number) {
                    ERS_DEBUG(1, "Wait before " << address.number << " sampler(s) are connected");
                    m_thread_pool.waitForCompletion();

                    const EventMonitoring::SamplingAddress &connected = monitor->samplersConnected();
                    ERS_DEBUG(1, "Finally " << connected.names.length() << " sampler(s) are connected");

                    if (connected.names.length() < counter) {
                        counter = connected.names.length();
                        continue;
                    }
                    break;
                }
            }
            ERS_DEBUG(1, "Wait before " << address.number << " sampler(s) are connected");
            m_thread_pool.waitForCompletion();

            if (not unique_channel) {
                break;
            } else {
                // One more iteration allowing to connect multiple monitors to the same sampler
                unique_channel = false;
            }
        }
    }

    address = monitor->samplersConnected();
    if (!address.names.length()) {
        ERS_LOG("Failed to connect monitor to " << address << " with criteria " << monitor->criteria());
        monitor->throwException();
    }
    ERS_LOG("A new monitor has been connected to " << address << " with criteria " << monitor->criteria());
}

void SamplerClass::validate() {
    SamplersMap samplers;
    {
        std::unique_lock lock(m_mutex);
        samplers = m_samplers;
    }

    SamplersMap toberemoved;
    SamplersMap::iterator it = samplers.begin();
    for (; it != samplers.end(); ++it) {
        if (!it->second->isValid()) {
            toberemoved.insert(std::make_pair(it->first, it->second));
        }
    }

    std::unique_lock lock(m_mutex);
    SamplersMap::iterator rit = toberemoved.begin();
    for (; rit != toberemoved.end(); ++rit) {
        SamplersMap::iterator sit = m_samplers.find(rit->first);
        if (sit != m_samplers.end() and sit->second == rit->second) {
            ERS_LOG("Removing non responding '" << sit->first << "' sampler");
            sit->second->samplerExit();
            m_zombie_samplers[sit->first] = sit->second;
            m_samplers.erase(sit);
        }
    }
}
