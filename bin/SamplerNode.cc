//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    SamplerNode.cc
//    Implementation of the SamplerNode object the conductor uses for tree management
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Mar. 8 2008       
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <ers/ers.h>

#include <emon/SamplerNode.h>
#include <emon/Util.h>

using namespace emon;

/*!
 * Creates a new SamplerNode representing a Monitoring Task
 * \param monitor a CORBA object reference to the associated Monitoring Task
 * \param max_children the maximum number of children this Monitoring Task may have
 * \param restore whether or not to use restore mode
 */
SamplerNode::SamplerNode(const EventMonitoring::SamplingAddress &address,
        EventMonitoring::EventSampler_ptr sampler) :
        m_address(address),
        m_sampler(EventMonitoring::EventSampler::_duplicate(sampler))
{
    ERS_ASSERT(m_address.names.length());
}

SamplerNode::SamplerNode(const EventMonitoring::SamplingAddress &address,
        EventMonitoring::EventSampler_ptr sampler,
        const std::unordered_set<MonitorNode::SharedPtr> & existing_monitors) :
        m_address(address),
        m_sampler(EventMonitoring::EventSampler::_duplicate(sampler))
{
    ERS_ASSERT(m_address.names.length());

    std::string sampler_name = construct_name(m_address);

    ERS_DEBUG(1, "Restoring monitoring channels for the " << sampler_name << " sampler");

    try {
        EventMonitoring::MonitorList_var monitors;
        m_sampler->get_monitors(monitors);

        for (CORBA::ULong i = 0; i < monitors->length(); i++) {
            MonitorNode::SharedPtr monitor_ptr;
            for (auto & m : existing_monitors) {
                if (m->match(monitors[i].reference)) {
                    monitor_ptr = m;
                    break;
                }
            }
            if (!monitor_ptr) {
                monitor_ptr = std::make_shared<MonitorNode>(monitors[i]);
            }

            std::string name = construct_name(monitor_ptr->criteria(), monitor_ptr->group_name());
            auto it = m_channels.find(name);
            if (it == m_channels.end()) {
                Monitors mm(1, monitor_ptr);
                m_channels.insert(std::make_pair(name, mm));
            } else {
                it->second.push_back(monitor_ptr);
            }
        }
        ERS_LOG(m_channels.size() << " monitoring channels have been restored for the "
                << sampler_name << " sampler");
    } catch (CORBA::SystemException &ex) {
        ERS_LOG("Can't restore monitoring channels for the "
                << sampler_name << " sampler : " << ex._name());
    }
}

void SamplerNode::samplerExit() {
    ERS_ASSERT(m_address.names.length());
    std::unique_lock lock(m_mutex);
    for (Channels::iterator it = m_channels.begin(); it != m_channels.end(); ++it) {
        Monitors::iterator mit = it->second.begin();
        for (; mit != it->second.end(); ++mit) {
            (*mit)->samplerExit(std::string((const char*) m_address.names[0]));
        }
    }
}

void SamplerNode::getChannels(std::unordered_set<std::string> & channels) const {
    std::unique_lock lock(m_mutex);
    for (auto it = m_channels.begin(); it != m_channels.end(); ++it) {
        channels.insert(it->first);
    }
}

void SamplerNode::getMonitors(std::unordered_set<MonitorNode::SharedPtr> & monitors) const {
    std::unique_lock lock(m_mutex);
    for (auto it = m_channels.begin(); it != m_channels.end(); ++it) {
        for (auto & m : it->second) {
            monitors.insert(m);
        }
    }
}

void SamplerNode::resurrect(EventMonitoring::EventSampler_ptr sampler_ptr) {
    std::unique_lock lock(m_mutex);
    m_sampler = EventMonitoring::EventSampler::_duplicate(sampler_ptr);
    ERS_LOG("Validating monitors of '" << construct_name( m_address ) << "' event sampler");
    for (auto it = m_channels.begin(); it != m_channels.end(); ++it) {
        for (auto monitor = it->second.begin(); monitor != it->second.end(); ) {
            if ((*monitor)->ping()) {
                if ((*monitor)->connect(m_sampler, m_address)) {
                    (*monitor)->samplerEnter(std::string((const char*) m_address.names[0]));
                }
                ++monitor;
            } else {
                ERS_LOG("Monitor of '" << it->first
                        << "' channel does not respond to ping, it will be removed");
                monitor = it->second.erase(monitor);
            }
        }
    }
}

/*
 * \param criteria selection criteria for the monitoring task to be removed
 * \param group_name the group name for the monitoring task to be removed
 * \param monitor the child Monitoring Task to remove from this Monitoring Tree
 */
void SamplerNode::removeMonitor(const EventMonitoring::SelectionCriteria &criteria,
        std::string group_name, EventMonitoring::EventMonitor_ptr monitor)
{
    std::string name = construct_name(criteria, group_name);

    std::unique_lock lock(m_mutex);
    Channels::iterator it = m_channels.find(name);

    if (it == m_channels.end()) {
        ERS_LOG("Channel '" << name << "' does not exist in the '"
                << construct_name( m_address ) << " sampler");
        return;
    }

    Monitors::iterator mit = it->second.begin();
    for (; mit != it->second.end(); ++mit) {
        if ((*mit)->match(monitor)) {
            (*mit)->disconnect(m_sampler, monitor);
            it->second.erase(mit);
            break;
        }
    }

    if (it->second.empty()) {
        m_channels.erase(it);
    }
}

void SamplerNode::addMonitor(MonitorNode::SharedPtr monitor, bool new_channel_only) {
    std::string name = construct_name(monitor->criteria(), monitor->group_name());

    std::unique_lock lock(m_mutex);

    Channels::iterator it = m_channels.find(name);
    if (it == m_channels.end()) {
        if (monitor->connect(m_sampler, m_address)) {
            Monitors mm;
            mm.push_back(monitor);
            m_channels.insert(std::make_pair(name, mm));
        }
    } else {
        if (new_channel_only) {
            ERS_DEBUG(1, "The " << construct_name( m_address ) << "' event sampler already has "
                    << name << "' event channel");
            return;
        }

        if (monitor->connect(m_sampler, m_address)) {
            it->second.push_back(monitor);
        }
    }

    ERS_DEBUG(1, "Monitor has been added to the '" << name << "' event channel of '"
            << construct_name( m_address ) << "' event sampler");
}

bool SamplerNode::isValid() const {
    try {
        m_sampler->ping();
        ERS_DEBUG(2, "Event Sampler " << construct_name( m_address )
                << " responded to ping.");
        return true;
    } catch (CORBA::SystemException &ex) {
        ERS_LOG("Event Sampler '" << construct_name( m_address )
                << "' does not respond to ping: " << ex._name());
    }
    return false;
}

size_t SamplerNode::monitorsNumber(MonitorNode::SharedPtr monitor) {
    std::string name = construct_name(monitor->criteria(), monitor->group_name());

    std::unique_lock lock(m_mutex);
    Channels::iterator it = m_channels.find(name);

    if (it == m_channels.end()) {
        return 0;
    }

    return it->second.size();
}
