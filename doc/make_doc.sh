# make c++ developers documentation 
doxygen config
cd latex
make refman.pdf > /dev/null
cd ..
cp ./latex/refman.pdf developers.pdf
rm -rf ./latex

# make java developers documentation
doxygen jconfig
cd latex
make refman.pdf > /dev/null
cd ..
cp ./latex/refman.pdf java_developers.pdf
rm -rf ./latex

# make c++ users documentation
doxygen config_users
cd latex
make refman.pdf > /dev/null
cd ..
cp ./latex/refman.pdf users.pdf
rm -rf ./latex

# make java users documentation
doxygen jconfig_users
cd latex
make refman.pdf > /dev/null
cd ..
cp ./latex/refman.pdf java_users.pdf
rm -rf ./latex
