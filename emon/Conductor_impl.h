///////////////////////////////////////////////////////////////////
//    Conductor_impl.h
//    Implementation of the EventConductor CORBA interface
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Apr 29 2020
///////////////////////////////////////////////////////////////////

#ifndef _EMON_CONDUCTOR_IMPL_H_
#define _EMON_CONDUCTOR_IMPL_H_

#include <map>
#include <memory>
#include <string>

#include <ipc/object.h>
#include <ipc/alarm.h>

#include <emon/ConductorInfoNamed.h>
#include <emon/SamplerClass.h>
#include <emon/InfoPublisher.h>

namespace emon {
    /*! An object of type Conductor_impl does connection management and error recovery
     *  for event samplers and monitors.
     *  \author Serguei Kolos
     */
    class Conductor_impl:
            public IPCNamedObject<POA_EventMonitoring::Conductor>,
            public ConductorInfoNamed,
            public InfoPublisher {
    public:

        //! Constructor.
        explicit Conductor_impl(const IPCPartition &partition);

        //! Destructor.
        ~Conductor_impl();

        //! Tells whether an Conductor already exists in this partition.
        static bool isExist(const IPCPartition &p);

    private:
        void updateStatistics() override;

        //! Registers a new EventSampler
        void add_sampler(const EventMonitoring::SamplingAddress &sa,
                EventMonitoring::EventSampler_ptr sampler) override;

        //! Delete an event sampler from the Conductors local EventSampler list.
        void remove_sampler(const EventMonitoring::SamplingAddress &sa) override;

        //! Connect a Monitoring Task to the EventMonitoring System.
        void add_monitor(EventMonitoring::SamplingAddress &sa,
                const EventMonitoring::SelectionCriteria &sc, const char *group_name,
                EventMonitoring::EventMonitor_ptr monitor) override;

        //! Disconnects a Monitoring Task from the EventMonitoring System.
        void remove_monitor(const EventMonitoring::SamplingAddress &sa,
                const EventMonitoring::SelectionCriteria &sc, const char *group_name,
                EventMonitoring::EventMonitor_ptr monitor) override;

        //! Shuts down the EventMonitoring Conductor.
        void shutdown() override;

        //! Sends a periodic ping to all SamplerTypes.
        bool pingLoop();

        void registerSampler(const EventMonitoring::SamplingAddress &address,
                EventMonitoring::EventSampler_ptr sampler, bool restore = false);
    private:
        typedef std::map<std::string, SamplerClass::SharedPtr> SamplerTypes;

        std::mutex m_mutex;             //! this mutex guards samplers map.
        SamplerTypes m_samplers;	//! A STL map containing EventSampler objects
        IPCAlarm m_ping;
    };
}
#endif
