////////////////////////////////////////////////////////////////////////
//	Event.h
//
//	Event class
//
//	Ingo Scholtes, 2005-10-12
//
////////////////////////////////////////////////////////////////////////

#ifndef _EMON_EVENT_H_
#define _EMON_EVENT_H_

#include <memory>

#include <emon/emon.hh>

namespace emon {

    //! A shared pointer type to an event.
    /*! 
     * \sa emon::Event
     */
    typedef std::shared_ptr<EventMonitoring::Event> smart_event_ptr;

    //! A class representing an event.
    /*!
     *
     * This class represents an user wrapper for the raw event.
     */
    class Event {
        friend class EventIterator;

    public:

        Event() : m_event(new EventMonitoring::Event(1)) {
            m_event->length(1);
        }

        //! Get the data of the event in a single contiguous memory chunk.
        /*!
         * This method returns the pointer to the event data array.
         */
        const unsigned int* data() const {
            return (unsigned int*) (*m_event)[0].get_buffer();
        }

        //! Get the data of the event in a single contiguous memory chunk.
        /*!
         * This method returns the pointer to the event data array. The
         * caller is responsible for releasing this array when it's not used any more.
         */
        unsigned int* yield_data() {
            return (unsigned int*) (*m_event)[0].get_buffer(true);
        }

        //! Returns the overall length of this event.
        /*!
         * This method returns the number of 4-byte words in the event.
         */
        unsigned int size() const {
            return (*m_event)[0].length();
        }

    private:
        //! Constructor.
        /*!
         * Creates a new event wrapper around the given event.
         * \param event event to wrap
         */
        explicit Event(const smart_event_ptr &event) : m_event(event) {}

        smart_event_ptr m_event;
    };
}

#endif
