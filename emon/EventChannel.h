////////////////////////////////////////////////////////////////////////
//    EventChannel.h
//
//    EventChannel class implementation
//
//    Serguei Kolos, 2020-04-31
//
//    description:
//			   
////////////////////////////////////////////////////////////////////////

#ifndef _EMON_EVENT_CHANNEL_H_
#define _EMON_EVENT_CHANNEL_H_

#include <sys/uio.h>

#include <chrono>
#include <condition_variable>
#include <map>
#include <mutex>
#include <thread>

#include <ipc/alarm.h>
#include <ipc/partition.h>

#include <emon/emon.hh>

#include <emon/PushSampling.h>
#include <emon/PullSampling.h>
#include <emon/SelectionCriteria.h>

namespace emon {
    class EventSampler_impl;

    /**
     * This class implements an event channel for EventSampler. The event channel
     * object is used to push events to the all connected monitors which share the
     * same event selection criteria.
     **/
    class EventChannel {
        friend class EventSampler_impl;

    public:
        ~EventChannel();

        //! Pushes event to the connected Monitors
        void pushEvent(unsigned int *event, size_t event_size);

        //! Pushes event to the connected Monitors
        void pushEvent(iovec *event, size_t count);

        //! Checks if the channel is ready to accept a new event.
        bool readyToSendEvent();

        //! Returns selection criteria associated with this EventChannel.
        SelectionCriteria getCriteria() const {
            return SelectionCriteria(m_criteria);
        }

        uint32_t eventsSampled() const {
            return m_events_sampled;
        }

        uint32_t eventsSent() const {
            return m_events_sent;
        }

        uint32_t eventsRejected() const {
            return m_events_rejected;
        }

    private:
        EventChannel(const EventChannel&) = delete;
        EventChannel& operator=(const EventChannel&) = delete;

        void pushEvent(const EventMonitoring::Event &event);

        void pushToOne(const EventMonitoring::Event &event);

        void pushToAll(const EventMonitoring::Event &event);

        //! Constructor for push model.
        EventChannel(const EventMonitoring::SelectionCriteria &criteria,
                const std::string group_name, EventMonitoring::EventMonitor_ptr monitor);

        //! Constructor for pull model.
        EventChannel(const EventMonitoring::SelectionCriteria &criteria,
                const std::string group_name, EventMonitoring::EventMonitor_ptr monitor,
                PullSampling *sampler);

        void connectMonitor(EventMonitoring::EventMonitor_ptr monitor);

        void disconnectMonitor(EventMonitoring::EventMonitor_ptr monitor);

        //! Tells whether the last pushing of an event to the Monitoring Task failed.
        bool idle() {
            std::unique_lock lock(m_mutex);
            return !m_active_monitors;
        }

        //! Sets the PushSampling object for this EventChannel.
        void setActivity(PushSampling *activity) {
            m_push_sampling = activity;
        }

        //! Returns the selection criteria associated with this EventChannel.
        const EventMonitoring::SelectionCriteria& criteria() const {
            return m_criteria;
        }

        //! Returns the monitor group name associated with this EventChannel.
        const std::string group_name() const {
            return m_group_name;
        }

        void getMonitors(std::vector<EventMonitoring::EventMonitor_var> &monitors);

        uint32_t monitorsConnected() const {
            std::unique_lock lock(m_mutex);
            return m_active_monitors;
        }

    private:
        void run();

        void wait();

    private:
        typedef std::chrono::steady_clock::time_point TimePoint;
        typedef std::chrono::steady_clock Clock;

        struct Monitor {
            Monitor(EventMonitoring::EventMonitor_ptr monitor) :
                    m_monitor(EventMonitoring::EventMonitor::_duplicate(monitor)) {
            }

            bool match(EventMonitoring::EventMonitor_ptr monitor) const {
                return m_monitor->_is_equivalent(monitor);
            }

            TimePoint sendEvent(const EventMonitoring::Event &event);

            EventMonitoring::EventMonitor_var monitor() const {
                return m_monitor;
            }

        private:
            EventMonitoring::EventMonitor_var m_monitor;
        };

    private:
        typedef std::multimap<TimePoint, std::shared_ptr<Monitor>> MonitorMap;

        const EventMonitoring::SelectionCriteria m_criteria; //! The selection criteria of this EventChannel.
        const std::string m_group_name;	//! The monitor group name of this EventChannel
        const bool m_split_events;      //! If true, each event is sent to a single monitor in the chain, if false each event is sent to all monitors

        mutable std::mutex m_mutex;
        uint32_t m_events_sampled;
        uint32_t m_events_sent;
        uint32_t m_events_rejected;
        uint32_t m_active_monitors;
        std::condition_variable m_condition;
        MonitorMap m_monitors;
        bool m_terminated;              //! A flag telling whether this channel is about to be destroyed
        PushSampling *m_push_sampling;  //! The push sampling object.
        PullSampling *m_pull_sampling;  //! A reference to the PullSampling object which will be used to sampleEvents from.
        std::thread m_pull_thread;
    };
}
#endif
