////////////////////////////////////////////////////////////////////////
//    EventIterator.h
//
//    Definition of the EventIterator class
//
//    Serguei Kolos, 2005-10-12
//
////////////////////////////////////////////////////////////////////////

#ifndef _EMON_EVENT_ITERATOR_H_
#define _EMON_EVENT_ITERATOR_H_

#include <emon/SamplingAddress.h>
#include <emon/SelectionCriteria.h>
#include <emon/EventMonitor_impl.h>
#include <emon/ImplWrapper.h>

#include <emon/dal/SamplingParameters.h>

namespace emon {
    //! An iterator object, that may be used to retrieve events.
    /*! An object of this class can be used to retrieve events from event samplers.
     */
    class EventIterator {
    public:
        //! Constructor.
        EventIterator(const IPCPartition &partition, const SamplingAddress &address,
                const SelectionCriteria &criteria, size_t buffer_size = 100,
                const std::string group_name = "");

        EventIterator(const IPCPartition &partition, const emon::dal::SamplingParameters &params);

        //! Retrieves the next event synchronously.
        /*! Retrieves the next event synchronously from the buffer, blocks until event is available or timeout is
         *  exceeded. If timeout is exceeded and no event could be retrieved, this function will throw NoMoreEvents.
         * If no timeout is specified, this function will never throw NoMoreEvents but will block until an event is received.
         * \param timeout_ms milliseconds to wait for an event (0 (default) means wait infinitely)
         * \return a smart pointer to an event
         * \exception emon::NoMoreEvents
         */
        Event nextEvent(size_t timeout_ms = 0) {
            return Event(m_monitor->nextEvent(false, timeout_ms));
        }
        ;

        //! Retrieves the next event (asynchronously).
        /*! Retrieves the next event asynchronously from the buffer. Throws a NoMoreEvents exception if no event
         *  is available.
         * \returns a smart pointer to an event
         * \exception emon::NoMoreEvents
         */
        Event tryNextEvent() {
            return Event(m_monitor->nextEvent(true));
        }
        ;

        //! Return the number of events which is currently available in the buffer.
        /*! Returns the number of events currently buffered in the event buffer of the Monitoring Task
         * \return the number of events currently buffered
         */
        size_t eventsAvailable() {
            return m_monitor->eventsAvailable();
        }
        ;

        //! Returns the number of events which have been dropped so far.
        /*! Returns how many events have been dropped by the Event Iterator, because
         * of no space left in the event buffer.
         * \return number of events been dropped
         */
        size_t eventsDropped() {
            return m_monitor->eventsDropped();
        }

    private:
        ImplWrapper<EventMonitor_impl> m_monitor;       //! This object will receive events.
    };
}
#endif
