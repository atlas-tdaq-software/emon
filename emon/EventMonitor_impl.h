/////////////////////////////////////////////////////////////////////////
//    EventMonitor_impl.h
//    Implementation of the EventMonitor CORBA interface
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Nov. 30 2006
/////////////////////////////////////////////////////////////////////////

#ifndef _EVENT_MONITOR_IMPL_H
#define _EVENT_MONITOR_IMPL_H

#include <chrono>
#include <condition_variable>
#include <mutex>
#include <string>

#include <tbb/concurrent_queue.h>

#include <ipc/object.h>

#include <emon/exceptions.h>
#include <emon/Event.h>
#include <emon/SamplingAddress.h>
#include <emon/SelectionCriteria.h>
#include <emon/MonitorInfoNamed.h>
#include <emon/InfoPublisher.h>

namespace emon {
    //! Main class for the Monitoring Task application.
    /*! 
     * \author Serguei Kolos
     *
     * This is the main class for the Monitoring Task application, implementing functionality that is
     * defined in the IDL declaration of EventMonitoring::EventMonitor interface.
     * \sa EventMonitoring::EventMonitor
     */
    class EventMonitor_impl:
            public IPCObject<POA_EventMonitoring::EventMonitor>,
            public MonitorInfoNamed,
            public InfoPublisher {
    public:
        EventMonitor_impl(const IPCPartition &partition, const SamplingAddress &address,
                const SelectionCriteria &criteria, std::string group_name, size_t buffer_size);

        size_t eventsAvailable() const {
            return m_main_buffer.size();
        }

        size_t eventsDropped() const {
            return m_dropped_events;
        }

        //! Establishes connection to event samplers. May throw exceptions.
        void connect();

        //! Retrieves the first event from the buffer and returns it to the user for processing.
        smart_event_ptr nextEvent(bool async, size_t timeout = 0);

        //! Does all the necessary cleanup actions for the object.
        void destroy();

    private:
        ~EventMonitor_impl();

        //! Used for pinging of root Monitoring Task by the EventSampler.
        void ping() override;

        //! Notifies the Monitoring Task, that the EventSampler has exitted for some reason.
        void sampler_exit(const char *name) override;

        //! Pushes an event to the local buffer.
        CORBA::ULong push_event(const EventMonitoring::Event &event) override;

        smart_event_ptr serialize(const EventMonitoring::Event &event) const;

    private:
        typedef tbb::concurrent_bounded_queue<smart_event_ptr> EventBuffer;
        typedef std::chrono::steady_clock::time_point TimePoint;
        typedef std::chrono::steady_clock Clock;

        struct RateAdapter {
            RateAdapter(const std::vector<std::string> &samplers) :
                    m_step(10), m_samplers(samplers), m_counter(0),
                    m_last_counter(0), m_period(0),
                    m_start(TimePoint::min())
            { }

            void eventConsumed(bool holdup);

            size_t eventReceived();

        private:
            const size_t m_step;
            const std::vector<std::string> &m_samplers;
            size_t m_counter;
            size_t m_last_counter;
            size_t m_period;
            TimePoint m_start;
        };

    private:
        std::mutex m_evt_mutex;
        std::condition_variable m_evt_condition;

        const IPCPartition m_partition;     //! The partition we work in.
        const SelectionCriteria m_criteria; //! The selection criteria, sampled events should satisfy.
        SamplingAddress m_address;	        //! The sampling address, this Monitoring Task is connected to.
        bool m_terminated;	                //! Used to stop forwarding thread.
        EventBuffer m_main_buffer;          //! A buffer containing events that have been received, but not yet processed.
        RateAdapter m_adapter;
    };
}
#endif
