/////////////////////////////////////////////////////////////////////////////
//    EventSampler.h
//    A Wrapper class for the EventSampler CORBA interface implementation
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Nov. 29 2006       
/////////////////////////////////////////////////////////////////////////////

#ifndef _EMON_EVENT_SAMPLER_H
#define _EMON_EVENT_SAMPLER_H

#include <emon/EventSampler_impl.h>
#include <emon/ImplWrapper.h>

namespace emon {
    //! A wrapper class for the user's SamplingFactory implementations.
    /*
     * This class takes care of CORBA's memory management.
     * It is the main class to be used by users of the framework.
     * \author Ingo Scholtes
     */
    class EventSampler {
    public:
        //! Creates a new push model EventSampler.
        EventSampler(const IPCPartition &partition, const SamplingAddress &address,
                PushSamplingFactory *factory, size_t max_channels = 10);

        //! Creates a new pull model EventSampler.
        EventSampler(const IPCPartition &partition, const SamplingAddress &address,
                PullSamplingFactory *factory, size_t max_channels = 10);

    private:
        //! An object reference to the main EventSampler_impl object.
        ImplWrapper<EventSampler_impl> m_sampler;
    };
}

#endif
