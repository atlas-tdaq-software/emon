////////////////////////////////////////////////////////////////////////
//    EventSampler_impl.h
//
//    CORBA EventSampler interface implementation
//
//    Ingo Scholtes & Serguei Kolos, 2004-08-31
//
//    description:
//	   
////////////////////////////////////////////////////////////////////////

#ifndef _EVENT_SAMPLER_IMPL_H_
#define _EVENT_SAMPLER_IMPL_H_

#include <map>
#include <memory>
#include <mutex>
#include <string>

#include <ipc/alarm.h>

#include <emon/SamplingAddress.h>
#include <emon/SelectionCriteria.h>
#include <emon/SamplerInfoNamed.h>
#include <emon/PushSamplingFactory.h>
#include <emon/PullSamplingFactory.h>
#include <emon/EventChannel.h>
#include <emon/InfoPublisher.h>

namespace emon {
    /*! This class implements EventMonitoring::EventSampler CORBA interface.
     * \author Serguei Kolos
     */
    class EventSampler_impl:
            public IPCNamedObject<POA_EventMonitoring::EventSampler>,
            public SamplerInfoNamed,
            public InfoPublisher
    {
    public:

        //! Constructor for pull model.
        EventSampler_impl(const IPCPartition &partition, const SamplingAddress &address,
                PullSamplingFactory * factory, size_t max_channels = 10);

        //! Constructor for push model.
        EventSampler_impl(const IPCPartition &partition, const SamplingAddress &address,
                PushSamplingFactory * factory, size_t max_channels = 10);

        //! Throws exceptions.
        void connect();

        //! Stops all sampling threads, unregisters the EventSampler and destroys this object.
        void destroy();

    private:
        //! Destructor
        ~EventSampler_impl();

        void updateStatistics() override;

        //! Used as a proof that this EventSampler is alive.
        void ping() override;

        //! Connects a Monitoring Task to this EventSampler.
        void connect_monitor(const EventMonitoring::SelectionCriteria &criteria,
                const char *group_name, EventMonitoring::EventMonitor_ptr monitor) override;

        //! Deletes a Monitoring Task from list of subscriptions.
        void disconnect_monitor(const EventMonitoring::SelectionCriteria &criteria,
                const char *group_name, EventMonitoring::EventMonitor_ptr monitor) override;

        //! Returns event channel information about this EventSampler.
        void get_monitors(EventMonitoring::MonitorList_out nodes) override;

        //! Returns address of this EventSampler.
        void get_address(EventMonitoring::SamplingAddress_out address) override;

        //! Terminates the owner process
        void shutdown() override;

    private:
        typedef std::map<std::string, std::shared_ptr<EventChannel>> SamplingChannels;

        std::mutex m_mutex;		//! A mutual exclusion object for internal use.
        uint32_t m_events_sampled_history;
        uint32_t m_events_sent_history;
        uint32_t m_events_rejected_history;
        SamplingAddress m_address;	//! The sampling address of this EventSampler.
        std::unique_ptr<PushSamplingFactory> m_push_factory;//! A factory object for the push sampling.
        std::unique_ptr<PullSamplingFactory> m_pull_factory;//! A factory object for the pull sampling.
        SamplingChannels m_channels;	//! Sampling channels map
    };
}

#endif
