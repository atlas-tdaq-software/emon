////////////////////////////////////////////////////////////////////////
//    ImplWrapper.h
//
//    Definition of the ImplWrapper class
//
//    Serguei Kolos, 2020-04-12
//
////////////////////////////////////////////////////////////////////////

#ifndef _EMON_OBJECT_WRAPPER_H_
#define _EMON_OBJECT_WRAPPER_H_

#include <ers/ers.h>

namespace emon {
    //! This class is used to delete the CORBA/IPC object by calling the destroy() function.
    /*! The CORBA/IPC objects must not be destroyed with the delete operator. One has to call
     *	the destroy() member function to delete them. The wrapper converts the delete operator
     *	to the call of the destroy() function.
     */
    template<class T>
    class ImplWrapper {
        T * m_object;

        ImplWrapper(const ImplWrapper & ) = delete;
        ImplWrapper& operator=(const ImplWrapper & ) = delete;

    public:
        explicit ImplWrapper(T *object) : m_object(object) {
            ERS_ASSERT(m_object);
        }

        ~ImplWrapper() {
            ERS_ASSERT(m_object);
            m_object->destroy();
        }

        T * operator->() {
            ERS_ASSERT(m_object);
            return m_object;
        }
    };
}

#endif
