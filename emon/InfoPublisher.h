////////////////////////////////////////////////////////////////////////
//   InfoPublisher.h
//
//   Helper class for the emon package
//
//   Ingo Scholtes, 2005-10-12
//
////////////////////////////////////////////////////////////////////////

#ifndef _EMON_INFO_PUBLISHER_H_
#define _EMON_INFO_PUBLISHER_H_

#include <ipc/alarm.h>
#include <emon/ProcessIdentityNamed.h>

namespace emon {
    class InfoPublisher {
    public:
        InfoPublisher(ProcessIdentityNamed &info, size_t period);

        virtual ~InfoPublisher();

        virtual void updateStatistics() {}

    private:
        bool executePublication();

        ProcessIdentityNamed &m_info;
        IPCAlarm m_alarm;
    };
}

#endif
