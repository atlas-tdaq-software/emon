/////////////////////////////////////////////////////////////
//    MonitorNode.h
//    Helper class for emon conductor implementation
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Oct. 12 2007
/////////////////////////////////////////////////////////////

#ifndef _EMON_MONITOR_NODE_H_
#define _EMON_MONITOR_NODE_H_

#include <memory>
#include <mutex>

#include <ipc/core.h>
#include <emon/emon.hh>

namespace emon {
    //! Represents a monitor in the Conductor's local memory
    /*! An instance of type MonitorNode contains all necessary information about an active event monitor.
     * \author Serguei Kolos
     */
    class MonitorNode {
    public:
        typedef std::shared_ptr<MonitorNode> SharedPtr;

        //! Constructor.
        MonitorNode(const EventMonitoring::SelectionCriteria & criteria, const std::string group_name,
                EventMonitoring::EventMonitor_ptr monitor);

        explicit MonitorNode(const EventMonitoring::MonitorInfo &monitor);

        bool match(EventMonitoring::EventMonitor_ptr monitor) const {
            return m_monitor->_is_equivalent(monitor);
        }

        const EventMonitoring::SelectionCriteria& criteria() const {
            return m_criteria;
        }

        const std::string& group_name() const {
            return m_group_name;
        }

        bool ping() const;

        //! Notifies Monitoring Task about death of the Event sapler which it is attached to.
        void samplerExit(const std::string &name);

        void samplerEnter(const std::string &name);

        bool connect(const EventMonitoring::EventSampler_var &sampler,
                const EventMonitoring::SamplingAddress &address);

        bool disconnect(const EventMonitoring::EventSampler_var &sampler,
                EventMonitoring::EventMonitor_ptr monitor);

        const EventMonitoring::SamplingAddress& samplersConnected() const {
            std::unique_lock lock(m_mutex);
            return m_samplers_connected;
        }

        bool isValid() const {
            return (!CORBA::is_nil(m_monitor));
        }

        void throwException() const {
            if (m_exception) {
                m_exception->_raise();
            }
        }

    private:
        mutable std::mutex m_mutex;		                //! this mutex guards concurrent operations
        const EventMonitoring::SelectionCriteria m_criteria;	//! Selection criteria for this monitor
        const std::string m_group_name;		                //! Monitor group name
        EventMonitoring::EventMonitor_var m_monitor;            //! A CORBA object reference to the monitor
        EventMonitoring::SamplingAddress m_samplers_connected;  //! The number of samplers, this monitor is connected
        std::unique_ptr<CORBA::UserException> m_exception;
    };
}
#endif
