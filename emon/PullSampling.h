////////////////////////////////////////////////////////////////////////
//    PullSampling.h
//
//    Base class for a pull-style sampling implementation.
//
//    Ingo Scholtes, 2004-09-01
//
//    description:
//			   
////////////////////////////////////////////////////////////////////////

#ifndef _PULL_SAMPLING_H
#define _PULL_SAMPLING_H

namespace emon {
    class EventChannel;

    //! Abstract class to be used by the user to implement pull model EventSamplers.
    /*! This class, containing a pure virtual function can  be used by the user 
     *  to implement custom EventSamplers following the pull model.
     *  Objects of the PullSampling inheriting type will be automatically 
     *  created by the PullSamplingFactory whenever a subscription of a Monitoring Task 
     *  arrives and a new EventChannel is created.
     *  \sa PullSamplingFactory
     *  \author Ingo Scholtes
     *  \example PullSamplerMain.cc
     */
    struct PullSampling {
        //! Destructor.
        /*! The object is destroyed when the sampling activity has
         *  to be stopped because it is no more necessary.
         */
        virtual ~PullSampling() = default;

        //! Sample one event from the hardware and push it to the Monitoring Tasks for distribution.
        /*! This virtual method has to be overwritten in order to be able to use the EventMonitoring framework.
         * \param ec EventChannel object, which shall be used to push events to the connected Monitoring Tasks
         * \sa EventChannel
         */
        virtual void sampleEvent(emon::EventChannel &ec) = 0;
    };
}

#endif
