////////////////////////////////////////////////////////////////////////
//    PullSamplingFactory.h
//
//    Base class for a pull-style sampling factory implementation.
//
//    Ingo Scholtes, 2004-09-01
//
//    description:
//
////////////////////////////////////////////////////////////////////////

#ifndef _PULL_SAMPLING_FACTORY_H_
#define _PULL_SAMPLING_FACTORY_H_

#include <emon/exceptions.h>
#include <emon/PullSampling.h>

namespace emon {
    //! Abstract base class for user's custom pull factory.
    /*! This abstract base class shall be used by the user in order to implement a
     * custom factory to create objects of type PullSampling
     * \author Ingo Scholtes
     * \sa PullSampling
     */
    struct PullSamplingFactory {
        //! Virtual destructor.
        virtual ~PullSamplingFactory() = default;

        //! Virtual method, implementation shall create a new PullSampling object upon request.
        /*! This method is invoked, whenever a new sampling thread is started.
         *  Each new thread will be associated with a new PullSampling object.
         * \param criteria selection criteria the events sampled by this thread will satisfy
         * \return a reference to a new PullSampling object responsible for event sampling
         * \exception emon::BadCriteria
         * \exception emon::NoResources
         */
        virtual PullSampling* startSampling(const SelectionCriteria & criteria) = 0;
    };
}
#endif
