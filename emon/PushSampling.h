////////////////////////////////////////////////////////////////////////
//    PushSampling.h
//
//    Base class for a push-style sampling implementation.
//
//    Ingo Scholtes, 2004-09-01
//
//    description:
//
////////////////////////////////////////////////////////////////////////

#ifndef _PUSH_SAMPLING_H
#define _PUSH_SAMPLING_H

namespace emon {
    //! Abstract class to be used by the user to implement push model EventSamplers.
    /*! An object of this class has to be created by the PushSamplingFactory
     *  when sampling is requested to be started. This object will be 
     *  automatically destroyed when sampling has to be stopped. When using this class, 
     * the user will have to ensure, that a new thread is started upon object creation and 
     * cleanly exited upon object destruction. It is the user's responsibility to push
     * events to the EventChannel using his custom thread.
     *  \sa PushSamplingFactory
     *  \author Ingo Scholtes
     *  \example PushSamplerMain.cc
     */
    struct PushSampling {
        //! Destructor.
        /*! The object is destroyed when the sampling activity has
         *  to be stopped because it is no more necessary.
         */
        virtual ~PushSampling() = default;
    };
}

#endif
