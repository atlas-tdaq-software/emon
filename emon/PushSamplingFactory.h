////////////////////////////////////////////////////////////////////////
//    PushSamplingFactory.h
//
//    Base class for a push-style sampling factory implementation.
//
//    Ingo Scholtes, 2004-09-01
//
//    description:
//
////////////////////////////////////////////////////////////////////////

#ifndef _PUSH_SAMPLING_FACTORY_H_
#define _PUSH_SAMPLING_FACTORY_H_

#include <emon/exceptions.h>
#include <emon/PushSampling.h>
#include <emon/EventChannel.h>

namespace emon {

    //! Abstract base class for user's custom push factory.
    /*! This class shall be used by the user in order to implement a custom factory
     *  to create objects of type PushSampling
     * \author Ingo Scholtes
     * \sa PushSampling
     */
    struct PushSamplingFactory {
        //! Virtual destructor.
        virtual ~PushSamplingFactory() = default;

        //! Virtual method, creates a new PushSampling object upon request.
        /*! This method is invoked, whenever a new sampling thread is started.
         *  Each new thread will be associated with a new PushSampling object.
         * \param criteria SelectionCriteria the events sampled by this thread will satisfy
         * \param channel an EventChannel object that may be used to push events to Monitoring Tasks
         * \return a reference to a new PushSampling object responsible for event sampling
         * \exception emon::BadCriteria
         * \exception emon::NoResources
         */
        virtual PushSampling* startSampling(
                const SelectionCriteria & criteria, EventChannel * channel) = 0;
    };
}
#endif
