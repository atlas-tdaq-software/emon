////////////////////////////////////////////////////////////////
//    SamplerClass.h
//    Helper class for emon conductor implementation
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Oct. 12 2008
////////////////////////////////////////////////////////////////

#ifndef _EMON_SAMPLER_CLASS_H_
#define _EMON_SAMPLER_CLASS_H_

#include <map>
#include <memory>
#include <mutex>
#include <string>

#include <ipc/threadpool.h>

#include <emon/SamplerNode.h>

namespace emon {
    /*! An instance of type SamplerClass contains all records about samplers of a given type.
     *  \author Serguei Kolos
     */
    class SamplerClass {
    public:
        typedef std::shared_ptr<SamplerClass> SharedPtr;
        typedef std::map<std::string, SamplerNode::SharedPtr> SamplersMap;

        //! Constructor.
        explicit SamplerClass(const std::string &name);

        //! Adds a new monitor.
        void addMonitor(EventMonitoring::SamplingAddress &address, MonitorNode::SharedPtr monitor);

        //! Removes monitor.
        void removeMonitor(const EventMonitoring::SamplingAddress &address,
                const EventMonitoring::SelectionCriteria &criteria, std::string group_name,
                EventMonitoring::EventMonitor_ptr child);

        void addSampler(const EventMonitoring::SamplingAddress &address,
                EventMonitoring::EventSampler_ptr sampler, bool restore);

        void removeSampler(const EventMonitoring::SamplingAddress &address);

        void validate();

        size_t channelsNumber() const;

        size_t samplersNumber() const;

        size_t monitorsNumber() const;

    private:
        void removeOne(const std::string &name, const EventMonitoring::SelectionCriteria &criteria,
                std::string group_name, EventMonitoring::EventMonitor_ptr monitor);

    private:
        mutable std::mutex m_mutex;	 //! this mutex guards samplers map
        const std::string m_name;        //! Name of the samplers type
        SamplersMap m_samplers;	         //! A map containing SamplerNodes representing samplers of that class
        SamplersMap m_zombie_samplers;   //! A map containing SamplerNodes representing dead samplers of that class
        IPCThreadPool m_thread_pool;
    };
}

#endif
