////////////////////////////////////////////////////////////
//    SamplerNode.hh
//    Helper class for emon conductor implementation
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Oct. 12 2008
////////////////////////////////////////////////////////////

#ifndef _EMON_SAMPLER_NODE_H_
#define _EMON_SAMPLER_NODE_H_

#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <unordered_set>

#include <emon/MonitorNode.h>

namespace emon {
    //! Represents a sampler in the Conductor's local memory.
    /*! An instance of type SamplerNode contains all necessary information about the
     *  monitors connected to this sampler.
     *  \author Serguei Kolos
     */
    class SamplerNode {

    public:

        typedef std::list<MonitorNode::SharedPtr> Monitors;
        typedef std::map<std::string, Monitors> Channels;
        typedef std::shared_ptr<SamplerNode> SharedPtr;

        //! Constructor.
        SamplerNode(const EventMonitoring::SamplingAddress &address,
                EventMonitoring::EventSampler_ptr sampler);

        SamplerNode(const EventMonitoring::SamplingAddress &address,
                EventMonitoring::EventSampler_ptr sampler,
                const std::unordered_set<MonitorNode::SharedPtr> & monitors);

        bool isValid() const;

        const EventMonitoring::SamplingAddress& getAddress() const {
            return m_address;
        }

        size_t channelsNumber() {
            std::unique_lock lock(m_mutex);
            return m_channels.size();
        }

        size_t monitorsNumber(MonitorNode::SharedPtr monitor);

        void getChannels(std::unordered_set<std::string> & channels) const;

        void getMonitors(std::unordered_set<MonitorNode::SharedPtr> & monitors) const;

        //! Adds a new monitor to the Monitoring Tasks tree attached to the Sampler represented by this object.
        void addMonitor(MonitorNode::SharedPtr monitor, bool new_channel_only);

        //! Removes a monitor from the tree of Monitoring Tasks attached to this sampler.
        void removeMonitor(const EventMonitoring::SelectionCriteria &criteria,
                std::string group_name, EventMonitoring::EventMonitor_ptr child);

        void resurrect(EventMonitoring::EventSampler_ptr sampler_ptr);

        void samplerExit();

    private:
        mutable std::mutex m_mutex;	                 //! this mutex guards channels map.
        const EventMonitoring::SamplingAddress m_address;//! Address of this sampler object
        EventMonitoring::EventSampler_var m_sampler;     //! A CORBA object reference to the Event Sampler
        Channels m_channels;                             //! A list of active event channels for this sampler
    };
}

#endif
