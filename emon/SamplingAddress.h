////////////////////////////////////////////////////////////////////////
//  SamplingAddress.h
//
//  Address of an event sampler
//
//  Serguei Kolos, 2008-10-12
//
////////////////////////////////////////////////////////////////////////

#ifndef _EMON_SAMPLING_ADDRESS_H_
#define _EMON_SAMPLING_ADDRESS_H_

#include <emon/emon.hh>
#include <emon/dal/SamplingAddress.h>

namespace emon {
    //! Class representing a sampling address.
    /*!
     *  This class implements a way of addressing event sampler(s). It inherits the class
     *  generated from the IDL declaration of EventMonitoring::SamplingAddress.
     * \author Sergei Kolos
     */
    struct SamplingAddress: public EventMonitoring::SamplingAddress {
        //! Constructor
        SamplingAddress(const emon::dal::SamplingAddress &address);

        //! Constructor
        SamplingAddress(const EventMonitoring::SamplingAddress &address);

        //! Constructor
        SamplingAddress(const std::string &key, const std::string &value);

        //! Constructor
        SamplingAddress(const std::string &key, const std::vector<std::string> &values);

        //! Constructor
        SamplingAddress(const std::string &key, unsigned short number = 1);
    };
}

#endif
