////////////////////////////////////////////////////////////////////////
//  SelectionCriteria.h
//
//  Classes for implementing event selection criteria
//
//  Serguei Kolos, 2008-10-12
//
////////////////////////////////////////////////////////////////////////

#ifndef _EMON_SELECTION_CRITERIA_H_
#define _EMON_SELECTION_CRITERIA_H_

#include <emon/emon.hh>
#include <emon/dal/SelectionCriteria.h>

namespace emon {
    typedef EventMonitoring::Logic Logic;
    typedef EventMonitoring::Origin Origin;

    namespace logic {
        static const Logic IGNORE = EventMonitoring::IGNORE;
        static const Logic AND = EventMonitoring::AND;
        static const Logic OR = EventMonitoring::OR;
    }

    namespace origin {
        static const Origin BEFORE_PRESCALE = EventMonitoring::BEFORE_PRESCALE;
        static const Origin AFTER_PRESCALE = EventMonitoring::AFTER_PRESCALE;
        static const Origin AFTER_VETO = EventMonitoring::AFTER_VETO;
    }

    //! A class representing a masked value.
    /*!
     *
     *  This class represents a masked value. It consists of a value of type T
     *  specifying the value of the selection criteria value and a boolean flag
     *  telling whether this value shall be ignored. This class inherits the class
     *  generated from the IDL declaration of EventMonitoring::MaskedValue
     * \author Serguei Kolos
     */
    template<class T>
    struct MaskedValue {
        //! Default Constructor.
        /*!
         * Creates a new MaskedValue which will be set to be ignored
         */
        MaskedValue() : m_value(T()), m_ignore(true) {}

        //! Constructor.
        /*!
         * Creates a new MaskedValue
         * \param value the long value to use for this masked value
         * \param ignore whether or not to ignore this value
         */
        MaskedValue(T value, bool ignore = false) {
            m_value = value;
            m_ignore = ignore;
        }

        //! Constructor
        /*!
         * Creates a copy of the value of type generated from the emon IDL file
         * \param value the masked value to copy
         */
        template<class I>
        MaskedValue(const I &value) : m_value(value.value), m_ignore(value.ignore) {}

        T m_value;
        bool m_ignore;
    };

    //! This class represents a smart value for stream tags.
    /*!
     *  This class represents a smart value for stream tags. It consists of stream type,
     *  list of stream names and a logic value which defines how to use those namse.
     *  Inherits from the EventMonitoring::SmartStreamValue which is generated from the emon IDL.
     * \author Serguei Kolos
     */
    struct SmartStreamValue {
        //! Default Constructor.
        /*!
         * Creates a new SmartValue which will be set to be ignored
         */
        SmartStreamValue() : m_logic(logic::IGNORE) {}

        //! Constructor.
        SmartStreamValue(const std::string &stream_type,
                const std::vector<std::string> &stream_names, Logic logic) :
                m_type(stream_type),
                m_names(stream_names),
                m_logic(logic)
        {}

        //! Constructor.
        SmartStreamValue(const EventMonitoring::SmartStreamValue &value);

        std::string m_type;
        std::vector<std::string> m_names;
        Logic m_logic;
    };

    //! This class represents a smart value for L1 bits.
    /*!
     *  This class represents a smart value for L1 bits. It consists of a list of bit positions
     *  and a logic value which defines how to use those bits.
     *  Inherits from the EventMonitoring::SmartBitValue which is generated from the emon IDL.
     * \author Serguei Kolos
     */
    struct SmartBitValue {
        //! Default Constructor.
        /*!
         * Creates a new SmartValue which will be set to be ignored
         */
        SmartBitValue() : m_origin(origin::AFTER_VETO), m_logic(logic::IGNORE) {}

        //! Constructor.
        SmartBitValue(const std::vector<unsigned short> &bit_positions,
                Logic logic, Origin origin = origin::AFTER_VETO) :
                m_bit_positions(bit_positions),
                m_origin(origin),
                m_logic(logic)
        {}

        //! Constructor.
        SmartBitValue(const EventMonitoring::SmartBitValue &value);

        std::vector<unsigned short> m_bit_positions;
        Origin m_origin;
        Logic m_logic;
    };

    typedef MaskedValue<unsigned char> L1TriggerType;
    typedef MaskedValue<unsigned int> StatusWord;

    //! A class representing a selection criteria.
    /*
     * This class represents a selection criteria consisting of a sequence of masked
     * values. It inherits the class generated from the IDL declaration of EventMonitoring::SelectionCriteria
     * \author Ingo Scholtes
     */
    struct SelectionCriteria {
        //! Default constructor.
        SelectionCriteria() = default;

        //! Constructor.
        SelectionCriteria(const emon::dal::SelectionCriteria &criteria);
        //! Constructor.
        /*! This can be used to construct Selection Criteria manually
         * \param lvl1_trigger_type MaskedValue specifying the type of the level1 trigger
         * \param lvl1_trigger_info smart value defining fine grain selection for L1 trigger
         * \param stream_tags smart value defining stream tags
         * \param status_word MaskedValue specifying status word in event header
         */
        SelectionCriteria(L1TriggerType lvl1_trigger_type, SmartBitValue lvl1_trigger_info,
                SmartStreamValue stream_tags, StatusWord status_word) :
                m_lvl1_trigger_type(lvl1_trigger_type),
                m_lvl1_trigger_info(lvl1_trigger_info),
                m_stream_tags(stream_tags),
                m_status_word(status_word)
        {}

        //! Constructor.
        /*!
         * Creates a copy of the value of type generated from the emon IDL file
         * \param criteria the selection criteria
         */
        SelectionCriteria(const EventMonitoring::SelectionCriteria &criteria) :
                m_lvl1_trigger_type(criteria.lvl1_trigger_type),
                m_lvl1_trigger_info(criteria.lvl1_trigger_info),
                m_stream_tags(criteria.stream_tags),
                m_status_word(criteria.status_word)
        {}

        EventMonitoring::SelectionCriteria operator()() const;

        L1TriggerType m_lvl1_trigger_type;
        SmartBitValue m_lvl1_trigger_info;
        SmartStreamValue m_stream_tags;
        StatusWord m_status_word;
    };

    //! Stream operator for selection criteria object.
    std::ostream& operator<<(std::ostream &out, const SelectionCriteria &sc);
}

#endif
