////////////////////////////////////////////////////////////////////////
//  Util.h
//
//  Common utility classes for the emon package
//
//  Ingo Scholtes, 2005-10-12
//
////////////////////////////////////////////////////////////////////////

#ifndef _EMON_UTIL_H_
#define _EMON_UTIL_H_

#include <emon/SamplingAddress.h>
#include <emon/SelectionCriteria.h>

namespace emon {
    //! Constructs IS object name for the given type of information object.
    std::string construct_info_name(const std::string &type);

    //! Returns a string representation of a sampling address.
    std::string construct_name(const SamplingAddress &address);

    //! Returns a string representation of a selection criteria.
    std::string construct_name(const SelectionCriteria &criteria);

    //! Returns a string representation of a selection criteria and group name
    std::string construct_name(const SelectionCriteria &criteria, const std::string group_name);

    //! Get a string representation of a sampling address and selection criteria.
    std::string construct_name(const SamplingAddress &address, const SelectionCriteria &criteria);

    //! Stream operator for masked value.
    template<class T>
    inline std::ostream& operator<<(std::ostream &out, const MaskedValue<T> &value) {
        if (value.m_ignore)
            out << "*";
        else
            out << value.m_value;
        return out;
    }

    template<>
    inline std::ostream& operator<<(std::ostream &out, const MaskedValue<unsigned char> &value) {
        if (value.m_ignore)
            out << "*";
        else
            out << (int) value.m_value;
        return out;
    }

    //! Stream operator for the Origin enumeration.
    std::ostream& operator<<(std::ostream &out, const emon::Origin &origin);

    //! Stream operator for the Logic enumeration.
    std::ostream& operator<<(std::ostream &out, const emon::Logic &logic);

    //! Stream operator for smart stream tag value.
    std::ostream& operator<<(std::ostream &out, const emon::SmartStreamValue &pv);

    //! Stream operator for smart bit value.
    std::ostream& operator<<(std::ostream &out, const emon::SmartBitValue &value);
}

#endif
