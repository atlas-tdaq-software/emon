////////////////////////////////////////////////////////////////////////
//	compress.h
//
//	Compression routines
//
//	Serguei Kolos, 2010-01-19
//
////////////////////////////////////////////////////////////////////////

#ifndef _EMON_COMPRESS_H_
#define _EMON_COMPRESS_H_

#include <emon/emon.hh>

namespace emon {
    bool compress(const EventMonitoring::Event &in, EventMonitoring::Event &out);

    bool compress(const EventMonitoring::EventFragment &in, EventMonitoring::EventFragment &out);

    bool uncompress(const EventMonitoring::EventFragment &in, EventMonitoring::EventFragment &out);

    bool uncompress(const EventMonitoring::Event &in, EventMonitoring::Event &out);
}

#endif
