////////////////////////////////////////////////////////////////////////
//	Exceptions.h
//
//	Common classes for the emon package
//
//	Ingo Scholtes, 2005-10-12
//
////////////////////////////////////////////////////////////////////////

#ifndef _EMON_EXCEPTIONS_H_
#define _EMON_EXCEPTIONS_H_

#include <ers/ers.h>

ERS_DECLARE_ISSUE(emon, Exception, ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(emon, SamplerStopped, Exception,
        "Event sampler '" << name << "' has been stopped",
        ERS_EMPTY,
        ((std::string)name))

ERS_DECLARE_ISSUE_BASE(emon, NoMoreEvents, Exception, "No more events available",
        ERS_EMPTY,
        ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(emon, BadAddress, Exception,
        "Event sampler '" << address << "' does not exist",
        ERS_EMPTY,
        ((std::string)address))

ERS_DECLARE_ISSUE_BASE(emon, SamplerAlreadyExists, Exception,
        "Event sampler '" << name << "' is already running",
        ERS_EMPTY,
        ((std::string)name))

ERS_DECLARE_ISSUE_BASE(emon, BadCriteria, Exception,
        "Event sampler '" << sampler << "' denies '" << criteria << "' selection criteria",
        ERS_EMPTY,
        ((std::string)sampler) ((std::string)criteria))

ERS_DECLARE_ISSUE_BASE(emon, NoResources, Exception,
        "Event sampler '" << sampler << "' reached the monitoring channels limit (" << channels << ")",
        ERS_EMPTY,
        ((std::string)sampler) ((size_t)channels))

#endif
