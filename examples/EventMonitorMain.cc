////////////////////////////////////////////
//    EventMonitorMain.cc
//    An example Monitoring Task implementation
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Nov. 29 2006
////////////////////////////////////////////

#include <fcntl.h>
#include <math.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <chrono>
#include <iostream>
#include <memory>
#include <random>

#include <emon/EventIterator.h>
#include <cmdl/cmdargs.h>
#include <owl/timer.h>
#include <ipc/core.h>
			
namespace
{
    int ReceivedSignal = 0;

    void signal_handler( int sig )
    {
	ReceivedSignal = sig;
    }

    void dump( const emon::Event & event, int = 0 )
    {
	std::cout << "Event size = " << std::dec << event.size( ) 
        	  << "\tEvent data :" << std::endl;

	const unsigned int * data = event.data( );
	std::cout << std::hex << std::setfill( '0' );
	for ( size_t i = 0; i < event.size( ); )
	{
	    std::cout << std::setw( 8 ) << std::dec << i << ": " 
            	      << std::setw( 2 ) << std::hex << (( data[i] >> 24 ) & 0xff )
            	      << std::setw( 2 ) << std::hex << (( data[i] >> 16 ) & 0xff )
            	      << std::setw( 2 ) << std::hex << (( data[i] >>  8 ) & 0xff )
            	      << std::setw( 2 ) << std::hex << (( data[i]       ) & 0xff ) << " ";
            ++i;
	    for ( ; i%10 && i < event.size( ); ++i )
		std::cout << std::setw( 8 ) << data[i] << " ";
	    std::cout << std::endl;
	}
	std::cout << std::dec << std::endl;
    }
}

int main( int argc, char ** argv )
{
    // initialize IPC
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
        ers::fatal( ex );
    }

    CmdArgStr		partition_name ('p', "partition", "partition-name", "partition to work in." );

    // Sampling Address
    CmdArgStr		type		('t', "sampler-type", "sampler-type", "sampler type", CmdArg::isREQ );
    CmdArgStrList	names		('n', "sampler-names", "sampler-names", "sampler names", CmdArg::isLIST );
    CmdArgInt		samplers_number ('m', "sampler-number", "sampler-number", "number of samplers to connect" );

    // Selection Criteria
    CmdArgInt		lvl1_type 	('L', "lvl1-type", "type", "lvl1_trigger type (default -1)" );
    CmdArgIntList	lvl1_bits	('B', "lvl1-bits", "bits", "non-zero bits positions in the LVL1 bit pattern", CmdArg::isLIST );
    CmdArgBool		lvl1_bits_logic ('l', "lvl1-bits-logic", "logic attribute for the LVL1 bit pattern , AND if given OR otherwise");
    CmdArgInt		lvl1_bits_origin('o', "lvl1-bits-origin", "origin", "origin of the LVL1 bit pattern, 0 - BEFORE_PRESCALE\n"
    									" 1 - AFTER_PRESCALE, 2 - AFTER_VETO (default)" );
    CmdArgInt		status_word 	('H', "header-word", "word", "status word (default 0 )");
    CmdArgStr		stream_type 	('T', "stream-type", "stream-type", "stream type name" );
    CmdArgStrList	stream_names	('N', "stream-names", "stream-names", "list of stream names", CmdArg::isLIST );
    CmdArgBool		stream_logic	('S', "stream logic", "logic attribute for the stream tags, AND if given OR otherwise");

    // Operational parameters
    CmdArgStr		group_name 	('g', "group-name", "group-name", "Monitor group name, monitors with the same group and selection criteria will receive different events" );
    CmdArgInt		wait		('W', "wait-for-connect", "wait-time", "delay in ms before another connection attempt (default 10000)");
    CmdArgInt		events		('e', "events", "events-number", "number of events to retrieve (default 10000)\n"
								 "-1 means work forever" );
    CmdArgInt		buffer_size	('b', "buffer-size", "buffer-size", "maximum number of events in the buffer (default 100)\n"
								 	"minimum value is 1" );
    CmdArgInt		repetitions	('r', "repetitions", "repetitions-number", "number of repetitions for the test (default 1)\n"
										"minimum value is 1" );
    CmdArgInt		async		('a', "async", "asynchronous", "1 if monitor should retrieve events asynchronous (default 0)");
    CmdArgInt		delay		('w', "delay", "delay-time", "delay in mks, the monitor should wait for each event to simulate processing (default 0)");
    CmdArgInt		timeout		('D', "timeout", "event-timeout", "timeout for the nextEvent call in synchronous mode (default 10000 ms)");
    CmdArgInt		verbosity	('v', "verbosity", "verbosity-level",	" 0 - print nothing (default)\n"
									" 1 - print event number and event size\n"
									" 2 - print event number, event size and event data.");
    
    // set default names for the command line parameters
    events = 10000;
    lvl1_type = -1;
    status_word = 0;
    group_name = "";
    repetitions = 1;
    buffer_size = 100;
    async = 0;
    delay = 0;
    verbosity = 0;
    lvl1_bits_origin = 2;
    stream_type = "";
    samplers_number = 1;
    timeout = 10000;
    wait = 10000;
    
    CmdArgvIter arg_iter( --argc, ++argv );
    CmdLine cmd( *argv, &partition_name, &type, &names, &samplers_number,
    			&lvl1_type, &lvl1_bits, &lvl1_bits_logic, &lvl1_bits_origin, &status_word, 
                        &stream_type, &stream_names, &stream_logic, &timeout, &wait,
			&events, &buffer_size, &async, &delay, &verbosity, &group_name, &repetitions, NULL );
    cmd.description( "Example of an Event Monitoring Task." );
    cmd.parse( arg_iter );

    std::vector<unsigned short> L1_bits;
    for ( size_t i = 0; i < lvl1_bits.count(); ++i )
    {
        L1_bits.push_back( lvl1_bits[i] );
    }
    
    std::vector<std::string> STREAM_names;
    for ( size_t i = 0; i < stream_names.count(); ++i )
    {
        STREAM_names.push_back( (const char*)stream_names[i] );
    }
    
    emon::Logic L1_logic = !( lvl1_bits.flags() && CmdArg::GIVEN ) ? emon::logic::IGNORE
    								   : ( lvl1_bits_logic.flags() && CmdArg::GIVEN ) 
                                                                   ? emon::logic::AND 
                                                                   : emon::logic::OR;
                                                                   
    emon::Logic STREAM_logic = !( stream_type.flags() && CmdArg::GIVEN ) ? emon::logic::IGNORE
								         : ( stream_logic.flags() && CmdArg::GIVEN )
								         ? emon::logic::AND
								         : emon::logic::OR;
    emon::SelectionCriteria criteria(	emon::L1TriggerType( (unsigned char)(int)lvl1_type, !( lvl1_type.flags() && CmdArg::GIVEN ) ),
                                        emon::SmartBitValue( L1_bits, L1_logic, (emon::Origin)(int)lvl1_bits_origin ),
                                        emon::SmartStreamValue( (const char*)stream_type, STREAM_names, STREAM_logic ),
                                        emon::StatusWord( status_word, !( status_word.flags() && CmdArg::GIVEN ) ) );
    
    std::vector<std::string> address_names;
    for ( unsigned int i = 0; i < names.count( ); i++ )
    {
        address_names.push_back( (const char *)names[i] );
    }
    
    emon::SamplingAddress address( (const char *)type, address_names );
    if ( !address_names.size() )
    {
    	address = emon::SamplingAddress( (const char *)type, samplers_number );
    }

    IPCPartition partition( partition_name );

    signal( SIGINT , signal_handler );
    signal( SIGTERM, signal_handler );

    std::default_random_engine generator (
            std::chrono::system_clock::now().time_since_epoch().count());
    std::normal_distribution<float> distribution (delay, ::sqrt((float)delay));

    std::unique_ptr<emon::EventIterator> it;
    for ( int i = 0; i < repetitions; i++ )
    {    	
        while (!ReceivedSignal) {
            try {
		// Connect to the samplers
		it.reset( new emon::EventIterator( partition, address, criteria, buffer_size, std::string(group_name) ) );
		break;
	    }
	    catch ( emon::Exception & ex ) {
		ers::error( ex );
		if (wait == 0) {
		    return 1;
		}
	    }

           usleep( wait * 1000 );
        }
        ERS_LOG( "Monitoring Task started with processing delay per event of " << delay << " ms" );

        // Start the timer
        OWLTimer timer;
        timer.start( );
        int eventCount = 0;
        while ( eventCount < events ) {
            
            if ( ReceivedSignal ) {
            	std::cout << "Received signal " << ReceivedSignal << ", exiting ... " << std::endl;
                break ;
            }
            
            emon::Event event;
            // wait some time to simulate event processing in our sample application
            if ( delay > 0 )
            {
                int64_t s = distribution(generator);
                usleep( s > 0 ? s : 0 );
            }
            try {
                // retrieve the event from the buffer, this either blocks (when async == 0)
                // or it will throw NoMoreEvents (when async == 1)
                // you can pass a timeout in milliseconds when in synchronous mode
                // after the timeout, NoMoreEvents will be thrown
                if ( verbosity > 1 ) {
                    if ( async )
                    	std::clog << "invoking tryNextEvent() ... ";
                    else
                    	std::clog << "invoking nextEvent(" << timeout << ") ... ";
                }
                if ( async )
                    event = it -> tryNextEvent( );
                else
                    event = it -> nextEvent( timeout );                
                	
                if ( verbosity > 1 ) {
                    std::cout << "done" << std::endl;
                }
            }
            catch ( emon::NoMoreEvents & ex ) {
                // output only when in synchronous case, because in asynchronous mode this will happen way too often!
                if ( !async ) {
                    ers::warning( ex );
                }
                else {
		    // We do nothing here, we just keep on trying
		    continue;
		}
            }
            catch ( emon::SamplerStopped & ex ) {
                // the sampler crashed or exited...
                ers::warning( ex );
                break;
            }
            catch ( emon::Exception & ex ) {
                ers::error( ex );
                break;
            }
            eventCount++;
            if ( verbosity > 0 ) {
                std::cout << "Event count = " << eventCount << " Buffer occupancy = [" 
                	  << it -> eventsAvailable() << "/" << buffer_size << "]" << std::endl;
            }
            if ( verbosity > 1 ) {
                dump( event );
            }
        }

        if (it.get()) {
            ERS_LOG( "End of run, monitor dropped " << it -> eventsDropped() << " events." );
        }
        
        //stop the timer
        timer.stop();
        ERS_LOG( "Total number of processed events : " << eventCount );
        ERS_LOG( "Total CPU Time : " << timer.userTime( ) + timer.systemTime( ) );
        ERS_LOG( "Total Time : " << timer.totalTime( ) );
        ERS_LOG( "Events/second : " << eventCount/timer.totalTime( ) );

	if ( ReceivedSignal ) {
	    break ;
	}
    }
    return 0;
}
