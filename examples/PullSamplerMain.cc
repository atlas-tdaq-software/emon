////////////////////////////////////////////////////////////////////////
//  EventSamplerMain.cc
//
//  Example of a pull-style Event Sampler implementation
//
//  Serguei Kolos, 2020-04-23
//
//  description:
////////////////////////////////////////////////////////////////////////

#include <fcntl.h>
#include <float.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/signal.h>

#include <EventStorage/pickDataReader.h>
#include <eformat/FullEventFragment.h>
#include <emon/EventSampler.h>

class MyPullSamplingFactory;

//! Pull model EventSampler implementation class.
/*!
 * This is an example of how to use the abstract PullSampling class.
 * Apart from the constructor, you will have to override the method sampleEvent.
 * The startSampling method of the PullSamplingFactory shall return instances of this class. For every emon::EventChannel
 * one instance of this class will be created.
 * \author Ingo Scholtes 
 * \author Sergei Kolos
 * \sa emon::PullSampling emon::PullSamplingFactory emon::EventSampler emon::EventChannel
 */
class MyPullSampling: public emon::PullSampling {
    MyPullSamplingFactory &m_factory;

public:

    //! Constructor of the example implementation.
    /*!
     * In real life, this method usually initializes hardware of the data flow system for event sampling
     * \param sc the selection criteria sampled events shall match
     * \param event example event to push to the event channel
     * \param size size of the example event
     */
    MyPullSampling(MyPullSamplingFactory &factory) :
            m_factory(factory) {
        ERS_DEBUG(1, "Initialization of PullSampling object completed.");
    }

    //! Example implementation of the sampling process.
    /*!
     * This method just pushes the example event to the Monitoring Tasks. When pushEvent of EventChannel
     * has finished, it is guaranteed, that the event buffer passed to EventChannel may be freed.
     * \sa EventChannel
     */
    void sampleEvent(emon::EventChannel &cc);

    //! Destructor of the example implementation.
    /*!
     * In real life, this method usually deallocates memory and prepares data flow for shutdown. Whenever an event channel
     * will be closed, the appropriate destructor will be called.
     */
    ~MyPullSampling() {
        ERS_DEBUG(1, "Finalization of PullSampling object.");
    }
};

//! Example implementation of the PullSamplingFactory.
/*!
 * This class is an example implementation of the PullSamplingFactory, it's main
 * purpose being the creation of MyPullSampling objects in the method startSampling
 * \author Ingo Scholtes
 */
class MyPullSamplingFactory: public emon::PullSamplingFactory {
public:

    //! Constructor.
    /*!
     * This example constructor simply allocates some memory for a new example event or uses 
     * the example event passed in the constructor
     * \param event the example event that shall be pushed to the EventChannel
     * \param size size of the example event
     */
    MyPullSamplingFactory(iovec *event, size_t size, DataReader *reader) :
            m_reader(reader), m_event(event), m_size(size) {
        ERS_DEBUG(1, "Initialization of PullSamplingFactory completed.");
    }

    ~MyPullSamplingFactory() {
        delete m_reader;
    }

    bool sendEvent(emon::EventChannel &channel) {
        std::unique_lock<std::mutex> lock(m_mutex);
        if (m_reader) {
            if (!m_reader->good()) {
                std::string filename = m_reader->fileName();
                delete m_reader;
                m_reader = pickDataReader(filename);
            }

            if (!m_reader || !m_reader->good()) {
                ers::fatal(emon::Exception(ERS_HERE, "Bad data file."));
                return false;
            }

            try {
                char *buf;
                unsigned int size = 0;

                DRError err = m_reader->getData(size, &buf);
                if (err != EventStorage::DROK) {
                    ers::fatal(emon::Exception(ERS_HERE, "Error when reading from data file."));
                    return false;
                }

                channel.pushEvent((unsigned int*) buf, size / 4);
                ERS_DEBUG(3, "Event (size = " << size << " Bytes) has been sent to the monitor");
                delete buf;
                return true;
            } catch (ers::Issue &ex) {
                ers::fatal(ex);
                return false;
            }
        }

        if (!m_event)
            return false;

        channel.pushEvent(m_event, m_size);

        return true;
    }

    //! This will be called whenever a new EventChannel needs to be created.
    /*!
     * This method will be called whenever a new EventChannel is required and a new
     * sampling thread needs to be created. It's main purpose is to enable users to pass
     * custom parameters to the MyPullSampling objects upon object creation time.
     * \param criteria the selection criteria, the events being sampled shall satisfy
     * \return a new instance of class MyPullSampling
     * \sa MyPullSampling emon::PullSampling emon::EventChannel
     */
    emon::PullSampling* startSampling(const emon::SelectionCriteria &criteria) {
        ERS_DEBUG(0, "Factory creating new PullSampling object with criteria: " << criteria);
        return new MyPullSampling(*this);
    }
    ;

private:
    std::mutex m_mutex;
    DataReader *m_reader;
    iovec *m_event;
    size_t m_size;
};

void MyPullSampling::sampleEvent(emon::EventChannel &cc) {
    ERS_DEBUG(3, "sending event to the consumer");
    m_factory.sendEvent(cc);
    ERS_DEBUG(3, "event was successfully sent");
}

int main(int argc, char **argv) {
    try {
        IPCCore::init(argc, argv);
    } catch (daq::ipc::Exception &ex) {
        ers::fatal(ex);
    }

    CmdArgStr partition_name('p', "partition", "partition-name", "partition to work in.");
    CmdArgStr type('t', "type", "type", "address type", CmdArg::isREQ);
    CmdArgStr name('n', "name", "name", "sampler name", CmdArg::isREQ);
    CmdArgInt event_size('S', "size", "event-size",
            "event size in 4-byte words ( 1024 by default )");
    CmdArgInt fragment_number('N', "number", "fragment-number",
            "number of fragments in the event ( 1 by default )");
    CmdArgStr event_file('f', "file", "data-file", "event data to be sent to monitor task");
    CmdArgInt max_channels('M', "channels", "max-channels",
            "maximum number of simultaneous sampling channels (default 10)\n");
    CmdArgStr raw_file('R', "raw-file", "raw-data-file", "real ATLAS raw data file");

    CmdLine cmd(*argv, &partition_name, &type, &name, &event_size, &event_file, &max_channels,
            &fragment_number, &raw_file, NULL);

    CmdArgvIter arg_iter(--argc, ++argv);

    cmd.description("pull model example event sampler");

    event_size = 1024;
    max_channels = 10;
    fragment_number = 1;

    cmd.parse(arg_iter);

    IPCPartition partition(partition_name);

    size_t size = fragment_number;
    iovec *event = new iovec[size];

    if (event_file.flags() & CmdArg::GIVEN) {
        std::string data_file_name((const char*) event_file);

        int fd = open(data_file_name.c_str(), O_RDONLY);
        if (fd == -1) {
            ers::fatal(emon::Exception(ERS_HERE, "Can't open '" + data_file_name + "' data file."));
            return 1;
        }

        size_t file_size = lseek(fd, 0, SEEK_END);

        if (file_size % 4) {
            ers::fatal(
                    emon::Exception(ERS_HERE,
                            "Bad event '" + data_file_name + "' file size given."));
            return 1;
        }

        ERS_DEBUG(1, "Event size is " << file_size/sizeof(size_t) << ".");
        lseek(fd, 0, SEEK_SET);

        size_t fragment_size = file_size / fragment_number;
        size_t rest_of_fragment_size = file_size % fragment_number;

        for (int i = 0; i < fragment_number; i++) {
            event[i].iov_len = fragment_size;
            if (i == (fragment_number - 1))
                event[i].iov_len += rest_of_fragment_size;

            event[i].iov_base = new caddr_t[event[i].iov_len];
            ssize_t bytes_read = read(fd, event[i].iov_base, event[i].iov_len);
            if (bytes_read == -1 || bytes_read != (ssize_t) event[i].iov_len) {
                ers::fatal(
                        emon::Exception(ERS_HERE,
                                "Can't read '" + data_file_name + "' data file."));
                return 1;
            }
        }

        close(fd);
    } else {
        // Calculate the size of each fragment
        size_t fragment_size = event_size / fragment_number;

        // Calculate the additional size of the last fragment
        size_t rest_of_fragment_size = event_size % fragment_number;

        for (int i = 0; i < fragment_number; i++) {
            event[i].iov_len = fragment_size * sizeof(size_t);
            if (i == (fragment_number - 1))
                event[i].iov_len += rest_of_fragment_size * sizeof(size_t);

            // Create and fill the actual buffer of this fragment
            event[i].iov_base = new caddr_t[event[i].iov_len];
            for (size_t j = 0; j < event[i].iov_len / sizeof(size_t); j++)
                ((size_t*) (event[i].iov_base))[j] = j * (i + 111111);
        }
    }

    DataReader *data_reader = 0;

    if (raw_file.flags() & CmdArg::GIVEN) {
        std::string data_file_name((const char*) raw_file);

        data_reader = pickDataReader(data_file_name);
        if (!data_reader || !data_reader->good()) {
            ers::fatal(
                    emon::Exception(ERS_HERE, "Cannot read '" + data_file_name + "' data file."));
            return 2;
        }

        try {
            char *buf;
            unsigned int size = 0;

            DRError err = data_reader->getData(size, &buf);
            if (err != EventStorage::DROK) {
                ers::fatal(
                        emon::Exception(ERS_HERE,
                                "Bad data file '" + data_file_name + "' is given."));
                return 2;
            }
            eformat::read::FullEventFragment f((unsigned int*) buf);
            f.check();
        } catch (ers::Issue &ex) {
            ers::error(
                    emon::Exception(ERS_HERE,
                            "The '" + data_file_name + "' file might be corrupted", ex));
        }
    }

    try {
        emon::SamplingAddress address((const char*) type, (const char*) name);

        emon::EventSampler temp(partition, address,
                new MyPullSamplingFactory(event, size, data_reader), max_channels);
        ERS_LOG("Event Sampler started in partition \"" << partition.name() << "\".");

        daq::ipc::signal::wait_for();

        ERS_LOG("Shutting down Event Sampler.");
    } catch (emon::Exception &ex) {
        ers::fatal(ex);
    }
    ERS_LOG("Shutdown of Event Sampler complete.");
    return 0;
}
