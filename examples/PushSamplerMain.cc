////////////////////////////////////////////////////////////////////////
//  EventSamplerMain.cc
//
//  Example of a push-style Event Sampler implementation
//
//  Serguei Kolos, 2020-04-23
//
//  description:
////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <iostream>
#include <thread>

#include <cmdl/cmdargs.h>
#include <emon/EventSampler.h>
#include <ipc/core.h>	
#include <ipc/signal.h>

#include <EventStorage/pickDataReader.h>
#include <eformat/FullEventFragment.h>

size_t *event_from_file = NULL;
size_t file_size = 0;

class MyPushSamplingFactory;

//! Example PushSampling implementation.
/*!
 * This is an example of how to use the abstract PushSampling class
 * You only will have to override the constructor and destructor of this class. In the constructor
 * you will have to create a new thread pushing events to the EventChannels passed by the constructor. In the
 * destructor you will have to make sure that the thread you creates cleanly exits.
 * \author Ingo Scholtes
 * \sa emon::PushSampling emon::EventSampler emon::PushSamplingFactory emon::EventChannel
 */
class MyPushSampling: public emon::PushSampling {
public:
    //! Constructor of the example implementation.
    /*! This example implementation simply starts a thread and uses custom parameters to pass an example event
     * in the constructor
     * \param sc the selection criteria the sampled events should satisfy
     * \param channel a reference to the emon::EventChannel you will want to use in order to push events to Monitoring Tasks
     * \param event an example event
     * \param size the size of the example event
     */
    MyPushSampling(MyPushSamplingFactory &factory, emon::EventChannel *channel) :
            m_factory(factory), m_channel(channel), m_terminated(false), m_thread(
                    std::bind(&MyPushSampling::run, this)) {
        ;
    }

    //! Destructor of the example implementation.
    /*!
     * Set the flag for thread termination, wait for the thread to terminate, clean up memory and exit
     */
    ~MyPushSampling() {
        ERS_DEBUG(0, "Finalization of PushSampling object started.");
        m_terminated = true;
        m_thread.join();
        ERS_DEBUG(0, "Finalization of PushSampling object completed.");
    }

private:

    //! This method will be executed in a separate thread.
    /*!
     * Its only purpose is to push events to the emon::EventChannel object
     */
    void run();

    MyPushSamplingFactory &m_factory;
    emon::EventChannel *m_channel;
    bool m_terminated;
    std::thread m_thread;
};

//! Example implementation of the PushSamplingFactory.
/*!
 * This class is an example implementation of the PushSamplingFactory, it's only
 * purpose being the creation of MyPushSampling objects in the method startSampling
 * \author Ingo Scholtes
 */
class MyPushSamplingFactory: public emon::PushSamplingFactory {
public:

    //! Constructor.
    /*!
     * This constructor allocates memory for a new event or uses
     * the event passed to the constructor
     * \param event the example event to use
     * \param size the size of the esample event
     */
    MyPushSamplingFactory(DataReader *dr, unsigned int *event, size_t size, size_t delay) :
            m_reader(dr), m_event(event), m_size(size), m_delay(delay) {
        ERS_DEBUG(1, "PushSamplingFactory is created");

        if (!m_event) {
            m_event = new unsigned int[m_size];
            memset(m_event, 'a', m_size * sizeof(unsigned int));
        }
    }

    ~MyPushSamplingFactory() {
        delete m_event;
        delete m_reader;
    }

    bool sendEvent(emon::EventChannel &channel) {
        if (m_delay)
            usleep(m_delay);

        std::unique_lock<std::mutex> lock(m_mutex);
        if (m_reader) {
            if (!m_reader->good()) {
                std::string filename = m_reader->fileName();
                delete m_reader;
                m_reader = pickDataReader(filename);
            }

            if (!m_reader || !m_reader->good())
                return false;

            try {
                char *buf;
                unsigned int size = 0;
                DRError err = m_reader->getData(size, &buf);
                if (err != EventStorage::DROK) {
                    ers::fatal(emon::Exception(ERS_HERE, "Error when reading from data file."));
                    return false;
                }

                channel.pushEvent((unsigned int*) buf, size / 4);
                ERS_DEBUG(3, "Event (size = " << size << " Bytes) has been sent to the monitor");
                delete buf;
                return true;
            } catch (ers::Issue &ex) {
                ers::fatal(ex);
                return false;
            }
        }

        if (!m_event)
            return false;

        channel.pushEvent(m_event, m_size);

        return true;
    }

    //!	This will be called whenever a new event channel needs to be created.
    /*! This method will be called whenever a new EventChannel
     * has to be created and it must return a new object of type MyPushSampling, custom parameters
     * may be passed to this object
     * \param criteria	    the selection criteria the sampled events should satisfy
     * \param channel	    the emon::EventChannel object we need to push events to
     * \return a new instance of MyPushSampling
     * \sa emon::PushSampling MyPushSampling
     */
    emon::PushSampling* startSampling(const emon::SelectionCriteria&, emon::EventChannel *channel) {
        ERS_DEBUG(0, "Factory creating new PushSampling object.");
        return new MyPushSampling(*this, channel);
    }

private:
    std::mutex m_mutex;
    DataReader *m_reader;
    unsigned int *m_event;
    size_t m_size;
    size_t m_delay;
};

void MyPushSampling::run() {
    while (!m_terminated) {
        if (!m_factory.sendEvent(*m_channel))
            break;
    }
}

int main(int argc, char **argv) {
    try {
        IPCCore::init(argc, argv);
    } catch (daq::ipc::Exception &ex) {
        ers::fatal(ex);
    }

    CmdArgStr partition_name('p', "partition", "partition-name", "partition to work in.");
    CmdArgStr type('t', "type", "type", "sampler type", CmdArg::isREQ);
    CmdArgStr name('n', "name", "name", "sampler name", CmdArg::isREQ);
    CmdArgInt event_size('S', "size", "event-size",
            "event size in 4-byte words ( 1024 by default )");
    CmdArgStr event_file('F', "file", "data-file",
            "simple file which has one event and no meta-data");
    CmdArgStr raw_file('R', "raw-file", "raw-data-file", "real ATLAS raw data file");
    CmdArgInt max_channels('M', "channels", "max-channels",
            "maximum number of sampling channels (default 10)\n");
    CmdArgInt delay('d', "delay", "delay",
            "delay between pushing events in microseconds (default 1000)\n");

    CmdLine cmd(*argv, &partition_name, &type, &name, &event_size, &event_file, &max_channels,
            &delay, &raw_file, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);
    cmd.description("push model example event sampler.");

    event_size = 1024;
    max_channels = 10;
    delay = 1000;
    cmd.parse(arg_iter);

    unsigned int *event = 0;
    size_t size = event_size;

    if (event_file.flags() & CmdArg::GIVEN) {
        std::string data_file_name((const char*) event_file);

        int fd = open(data_file_name.c_str(), O_RDONLY);
        if (fd == -1) {
            ers::fatal(
                    emon::Exception(ERS_HERE, "Cannot open '" + data_file_name + "' data file."));
            return 1;
        }

        int file_size = lseek(fd, 0, SEEK_END);
        if (file_size % 4) {
            ers::fatal(
                    emon::Exception(ERS_HERE,
                            "Bad '" + data_file_name + "' data file size given."));
            return 1;
        }
        size = file_size / 4;
        ERS_DEBUG(1, "event size is " << size);
        lseek(fd, 0, SEEK_SET);
        event = new unsigned int[size];
        if (read(fd, event, file_size) != file_size) {
            ers::fatal(
                    emon::Exception(ERS_HERE, "Cannot read '" + data_file_name + "' data file."));
            return 1;
        }
        close(fd);
    }

    DataReader *data_reader = 0;

    if (raw_file.flags() & CmdArg::GIVEN) {
        std::string data_file_name((const char*) raw_file);

        data_reader = pickDataReader(data_file_name);
        if (!data_reader || !data_reader->good()) {
            ers::fatal(
                    emon::Exception(ERS_HERE, "Cannot read '" + data_file_name + "' data file."));
            return 2;
        }

        try {
            char *buf;
            unsigned int size = 0;

            DRError err = data_reader->getData(size, &buf);
            if (err != EventStorage::DROK) {
                ers::fatal(
                        emon::Exception(ERS_HERE,
                                "Bad data file '" + data_file_name + "' is given."));
                return 2;
            }
            eformat::read::FullEventFragment f((unsigned int*) buf);
            f.check();
        } catch (ers::Issue &ex) {
            ers::error(
                    emon::Exception(ERS_HERE,
                            "The '" + data_file_name + "' file might be corrupted", ex));
        }
    }

    IPCPartition partition(partition_name);

    // it's as simple as that...;-)
    try {
        emon::SamplingAddress address((const char*) type, (const char*) name);

        emon::EventSampler temp(partition, address,
                new MyPushSamplingFactory(data_reader, event, size, delay), max_channels);
        ERS_LOG("Event Sampler started in partition \"" << partition.name() << "\".");
        daq::ipc::signal::wait_for();

        ERS_DEBUG(3, "Shutting down Event Sampler.");
    } catch (emon::Exception &ex) {
        ers::fatal(ex);
    }

    ERS_LOG("Shutdown of Event Sampler complete.");
    return 0;
}
