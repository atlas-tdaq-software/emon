
import ipc.*;
import emon.*;
import EventMonitoring.*;

//! Main class for JAVA event monitoring example task
public class EventMonitorMain {
    //! can be removed
    private static boolean stop = false;

    //! number of events that we already received
    private static int eventCount = 0;

    //! the number of events we want to receive
    private static int max_events = 10000;

    //! level trigger type for selection criteria
    private static byte lvl1_trigger_type = -1;

    //! status word for selection criteria
    private static int status_word = 0;

    //! maximum number of incoming events to buffer
    private static int buffer_limit = 100;

    //! event receiving timeout
    private static int timeout = 0;
    
    //! group_name, shared by monitors that want to receive different events from same criteria
    private static String group_name = "";

    public static void dumpUsage() {
        System.out.println("Usage: EventMonitorMain [partition] [sampler_type] [MaxEvents=10000] [Timeout=0] [BufferSize=100]");
    }

    public static void main(String[] args)
    {
        if (args.length < 3 || args.length > 6) {
            dumpUsage();
            return;
        }
        
	try {
	    if (args.length > 3)
		max_events = Integer.parseInt(args[3]);
	    if (args.length > 4)
		timeout = Integer.parseInt(args[4]);
	    if (args.length > 5)
		buffer_limit = Integer.parseInt(args[5]);
	} catch (NumberFormatException nfe) {
	    dumpUsage();
	}

        SamplingAddress address = new SamplingAddress( args[1], new String[0], (byte)1 );

        try {
            SelectionCriteria criteria = 
            	new SelectionCriteria(	new MaskedOctetValue( lvl1_trigger_type, false ),
                			new SmartBitValue( new short[0], Origin.AFTER_VETO, Logic.AND ),
                                        new MaskedLongValue( status_word, false ),
                                        new SmartStreamValue( new String(), new String[0], Logic.IGNORE ) );
            
            System.out.println( criteria );
                            
            EventIterator it = new EventIterator (new Partition(args[0]), address, criteria, group_name, buffer_limit);
            while (eventCount < max_events) {
                try {
                    int[] event;

                    if (timeout == 0)
                        event = it.tryNextEvent();
                    else
                        event = it.nextEvent(timeout);

                    eventCount++;
                    if (eventCount % 1000 == 0)
                        System.out.println("[EventMonitorMain::main] Event " + eventCount + " received");
                } catch (emon.NoMoreEvents ex) {
                    if (timeout != 0) System.out.println( "[EventMonitorMain::main] Warning: Event buffer empty after " + timeout + " ms timeout" );
                } catch (emon.SamplerStopped ex) {
                    System.out.println("[EventMonitorMain::main] Error: sampler stopped");
                    break;
                }
            }
            it.finalize();
        } catch (emon.CannotInitialize ci) {
            System.out
                    .println("[EventMonitorMain::main] Error: initialization failed! Wrong arguments?");
        } catch (emon.BadAddress ba) {
            System.out
                    .println("[EventMonitorMain::main] Error: bad sampling address");
        } catch (emon.BadCriteria bc) {
            System.out
                    .println("[EventMonitorMain::main] Error: bad selection criteria");
        } catch (emon.NoResources nr) {
            System.out
                    .println("[EventMonitorMain::main] Error: no resources in sampler");
        }
        System.out.println("[Monitor] ---------------------------------- ");
        System.out.println("[Monitor] Total number of events received : " + eventCount);
    }
};
