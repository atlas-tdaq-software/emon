package emon;

//! BadAddress Exception thrown by SamplingAddress class
/*!
 *  @author Serguei Kolos
 */
public final class BadAddress extends RuntimeException {
    
    //! public empty constructor
    public BadAddress() {
        super(EventMonitoring.BadAddressHelper.id());
    }
    //! public constructor, that may be used in order to give a detailed description
    /*!
     * \param value a more detailed description of this exception
     */
    public BadAddress(String value) {
        super(value);
    }
}