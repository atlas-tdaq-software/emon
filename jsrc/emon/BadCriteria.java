package emon;

//! BadCriteria Exception thrown by SelectionCriteria class
/*
 * ! @author Serguei Kolos
 */
public final class BadCriteria extends RuntimeException {
    //! public empty constructor
    public BadCriteria() {
        super(EventMonitoring.BadCriteriaHelper.id());
    }

    //! public constructor that may be used in order to give a more detailed description 
    /*!
     * \param value a more detailed description of this exception
     */
    public BadCriteria(String value) {
        super(value);
    }
}