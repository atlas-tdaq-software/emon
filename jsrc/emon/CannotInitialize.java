package emon;

//! BadAddress Exception thrown by SamplingAddress class
/*
 * ! @author Serguei Kolos
 */
public final class CannotInitialize extends RuntimeException {
    //! public empty constructor
    public CannotInitialize() {
    }
    /*! public constructor that may be used in order to give a more detailed description
     * \param value more detailed description of this exception
     */
    public CannotInitialize(String value) {
        super(value);
    }
}
