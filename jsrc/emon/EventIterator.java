package emon;

import ipc.Partition;
import EventMonitoring.SamplingAddress;
import EventMonitoring.SelectionCriteria;

//! An iterator object, that can be used to retrieve events
/*! An object of this type is returned by the select method in the EventMonitor and can be used
 *  to iterate through the events either asynchronous or synchronous
 * \sa EventMonitor select()
 * @author Ingo Scholtes
 * @version 02/09/2004
 */
public class EventIterator {

    //! The EventMonitor, from which we will receive the events
    private EventMonitor_impl monitor;

    //! Constructor
    /*
     * ! This constructor will try to connect to an event sampler, by sending a
     * connection request to the distributor, which will either connect this
     * monitor to an existing monitoring tree or install this monitor as new
     * root monitor and start the sampling thread in the event sampler. It is
     * automatically invoked on object construction. If this fails, it will
     * throw a CannotSelectException \param partition TDAQ partition, in which
     * the event monitoring system will be used 
     * \param partition partition where events will be sampled 
     * \param address address of the event sampler we want to connect to 
     * \param criteria defines what events to sample 
     * \param buffer_limit size of the local internal event buffer 
     * \param group_name the group name shared by monitors that want to receive different 
     *               events from the same criteria. If it's a non-empty string, events
     *               will be sent by samplers directly to each monitor, all receiving 
     *               different events. Otherwise events will also be moved between
     *               monitors; this iterator and all the other ones which are attached
     *               to it will see the same events.
     * \sa CannotInitialize BadAddress BadCriteria
     */
    public EventIterator(   Partition partition,
			    SamplingAddress address,
			    SelectionCriteria criteria,
			    int buffer_limit,
                            String group_name) throws CannotInitialize, BadAddress, BadCriteria
    {
        this.monitor = new EventMonitor_impl(partition, address,
						criteria, group_name, buffer_limit);
    }

    //! Constructor
    /*
     * ! This constructor will try to connect to an event sampler, by sending a
     * connection request to the distributor, which will either connect this
     * monitor to an existing monitoring tree or install this monitor as new
     * root monitor and start the sampling thread in the event sampler. It is
     * automatically invoked on object construction. If this fails, it will
     * throw a CannotSelectException \param partition TDAQ partition, in which
     * the event monitoring system will be used 
     * \param partition partition where events will be sampled 
     * \param address address of the event sampler we want to connect to 
     * \param criteria defines what events to sample 
     * \sa CannotInitialize BadAddress BadCriteria
     */
    public EventIterator(   Partition partition,
			    SamplingAddress address,
			    SelectionCriteria criteria ) throws CannotInitialize, BadAddress, BadCriteria
    {
        this.monitor = new EventMonitor_impl(partition, address,
						criteria,"", 100);
    }

    //!Retrieve next event (synchronously)
    /*! Retrieves the next event synchronously from the buffer, blocks until event is available
     * \return an event
     */
    public int[] nextEvent() throws SamplerStopped, NoMoreEvents {
        return monitor.nextEvent(false, 0);
    }

    //!Retrieve next event (synchronously)
    /*! Retrieves the next event synchronously from the buffer, blocks until event is available
     * \return an event
     */
    public int[] nextEvent(long timeout) throws SamplerStopped, NoMoreEvents {
        return monitor.nextEvent(false, timeout);
    }

    //! Retrieve next event (asynchronously)
    /*! Retrieves the next event asynchronously from the buffer.
     *  Throws a NotYetReadyException if no event
     *  is available.
     * \return an event
     * \sa NoMoreEvents SampledStopped
     */
    public int[] tryNextEvent() throws SamplerStopped, NoMoreEvents {
        return monitor.nextEvent(true);
    }

    //! Return the number of events currently in the buffer, ready to be requested
    /*! Returns the number of events currently buffered in the event buffer. 
     * Any events possibly buffered for sending to our children are not included in this number.
     * \return the number of events currently available
     */
    public int getAvailableEvents() {
        return monitor.bufferSize();
    }

    //! finalize this object and disconnect from the event monitoring system
    public void finalize() {
        System.out.println("[EventIterator::finalize] shutting down");
        monitor.destroy();
    }
}

