// //////////////////////////////////////////////////////////////////////
// EventMonitor_impl.java
//
// Java implementation of the EventMonitor
//
// Ingo Scholtes, 2005-07-01
//
////////////////////////////////////////////////////////////////////////

package emon;

import ipc.*;
import is.*;
import java.nio.*;
import java.util.*;
import org.omg.CORBA.*;
import java.util.zip.Inflater;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import EventMonitoring.SamplingAddress;
import EventMonitoring.SelectionCriteria;

//! Implementation of the event monitor application, used by the user for event
// processing
/*
 * ! The user will just have to create an instance of this class, in order to
 * receive sampled events. @author Ingo Scholtes
 * 
 * @version 07/01/2005
 */
public class EventMonitor_impl extends EventMonitoring.EventMonitorPOA
{        
    //! a class responsible for updating IS information in regular intervals
    class Publisher extends TimerTask
    {
	//! a reference to the EventMonitor_impl instance that has created this
	// instance
	EventMonitor_impl monitor = null;

	//! constructor
	/*
	 * ! \param self the EventMonitor_impl instance that created this
	 * instance
	 */
	public Publisher(EventMonitor_impl self)
	{
	    this.monitor = self;
	}

	//! the action to be performed when the timer takes action
	public void run()
	{
	    if ( monitor.isinfo == null )
		return;

	    try {
		monitor.isinfo.checkin();
	    }
	    catch( Exception e ) {
	    }
	}
    }

    private static final String UniqueName = getHost() + "/" + getPid();

    //! a reference to the IS information object
    MonitorInfoNamed isinfo;
    //! the sampling address to sample events from
    SamplingAddress address;
    //! the selection criteria events shall satisfy
    SelectionCriteria criteria;
    //! Group name that this monitor belongs to
    String group_name;
    //! the IPC partition we work in
    Partition partition;

    //! the incoming buffer limit of this application
    private int buffer_limit;
    //! whether to use dispersion or not
    private boolean dispersion;
    //! the incoming buffer of this application
    private Vector<int[]> EventBuffer;    
    //! a flag specifying whether the sampling application is still running
    private boolean SamplerRunning;
    //! a semaphore used to signal that another events is ready to be processed
    // by the user
    private Semaphore evt_sem;
    //! mutual exclusion for the children list
    private ReentrantLock chld_mutex;
    //! a list of child monitors
    private Vector<EventMonitoring.EventMonitor> Children;
    //! a reference to the thread publishing information in IS in regular
    // intervals
    private Timer publishing_;
    
    private int sub_tree_size = 1;
    
    //! buffer size in the past, neccessary to calculate sapling rate
    // adaptation requests
    private long last_buffer_size = 0;
    
    public static String getPid()
    {
	String processName =
		java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
	return processName.split("@")[0];
    }
    
    public static String getHost()
    {
	try {
	    java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
	    return localMachine.getHostName();
	}
	catch (java.net.UnknownHostException uhe) {
            return "unknown";
	}
    }
    
    //! Constructor
    /*
     * ! This creates an EventMonitor_impl object and does all neccessary
     * initialization. \param partition the IPCPartition object, this monitor
     * should use \param buffer_limit the maximum amount of events to buffer in
     * the Event- and Sendbuffer
     */
    public EventMonitor_impl(	Partition partition, 
				SamplingAddress address,
				SelectionCriteria criteria,
                                String group_name,
				int buffer_limit)
    { 
	this.partition = partition;
	this.criteria = criteria;
	this.buffer_limit = buffer_limit;
	this.isinfo = null;
	this.address = address;
        if (group_name == "")
        {
            this.dispersion = false;
        }
        else
        {
            this.dispersion = true;
        }
	publishing_ = new Timer();
	publishing_.schedule(new Publisher(this), 0, 10000);

	if ( buffer_limit < 1 )
	{
	    throw new CannotInitialize( "[EventMonitor_impl::EventMonitor_impl] Error: Event buffer size is wrong (" + buffer_limit + ")" );
	}
	SamplerRunning = false;
	EventBuffer = new Vector<int[]>();
	Children = new Vector<EventMonitoring.EventMonitor>();
	chld_mutex = new ReentrantLock();
	evt_sem = new Semaphore();
	start();
    }
    
    //! retrieve a CORBA object reference of this object
    public EventMonitoring.EventMonitor self()
    {
	return _this(Core.getORB());
    }
    
	//! Retrieve the first event from the buffer and return it to the user
	// for processing
    /*
     * ! This method will return the first event available in the event buffer.
     * Note: The behaviour of this function depends on whether or not this
     * object is configured for asynchronous behavior. If asynchronous event
     * retrieval is used, a call to NextEvent will throw a NoMoreEvents
     * exception when there is no event available. In case of synchronous event
     * retrieval, it will block until an event is available. If the sampler
     * crashes/stops in the middle of event sampling, this method will throw
     * SamplerStopped (even if in synchronous mode) \return event \sa
     * SamplerStopped NoMoreEvents
     */
    public int[] nextEvent(boolean async) throws SamplerStopped, NoMoreEvents
    {
	return nextEvent(async, 0);
    }
    
    //! Retrieve the first event from the buffer and return it to the user for
    // processing
    /*
     * ! This method will return the first event available in the event buffer.
     * Note: The behaviour of this function depends on whether or not this
     * object is configured for asynchronous behavior. If asynchronous event
     * retrieval is used, a call to NextEvent will throw a NoMoreEvents when
     * there is no event available. In case of synchronous event retrieval, it
     * will block until an event is available. If the sampler crashes/stops in
     * the middle of event sampling, this method will throw SamplerStopped (even
     * if in synchronous mode) \return event \sa SamplerStopped NoMoreEvents
     */
    public int[] nextEvent( boolean async, long timeout ) throws SamplerStopped, NoMoreEvents
    {
	if (!SamplerRunning)
	    throw new SamplerStopped();
	
	//asynchronous behaviour
	if (async)
	{
	    if (!evt_sem.TryWait())
	    {
		throw new NoMoreEvents();
	    }
	}
	else
	{
	if ( !evt_sem.Wait(timeout) )
	    throw new NoMoreEvents(); // timeout happens
	}
	int[] event;
	synchronized (EventBuffer)
	{
	    event = (int[]) EventBuffer.firstElement();
	    EventBuffer.remove(0);
	}
	return event;
    }
    
    //! Tells us that the event sampler has exitted for some reason
    /*
     * ! This will care about our child monitors, when the sampler we are
     * connected to has exitted. It will invoke sampler_exit of every child, it
     * will cause the forwarding thread to finish and it will cause an exception
     * on the next invocation of NextEvent to be thrown. This method needs to
     * signal the evt_condition to wake up sleeping nextEvent calls This is an
     * IPC published method.
     */
    public void sampler_exit(String message)
    {
	if (message.length() > 0) {
	    SamplerRunning = (message.charAt(0) == 'R');
	    evt_sem.Post();
	}
    }
    
    //! Ping method will be used by EventChannel in order to check if root monitor is still alive
    public void ping() {}
    
    int[] serialize( int[][] e )
    {        	
        Inflater zin = new Inflater();
	int[][] event = new int[e.length][];
	for ( int i=0; i< e.length; i++ )
        {
            try
            {
		int compressed_size   = e[i][e[i].length-1];
		int uncompressed_size = e[i][e[i].length-2];
		ByteBuffer cb = ByteBuffer.allocate( e[i].length * 4 );
		cb.order( ByteOrder.LITTLE_ENDIAN );
		cb.asIntBuffer().put( e[i] );
		zin.setInput( cb.array(), 0, compressed_size );

		event[i] = new int[uncompressed_size/4];
		ByteBuffer ucb = ByteBuffer.allocate( uncompressed_size );
                zin.inflate( ucb.array() );
		ucb.asIntBuffer().get( event[i] );
                zin.reset();
            }
            catch( java.lang.Exception ex )
            {
                event = e;
                break;
            }
	}
        
	int[] event_contig;
        if ( event.length > 1 )
	{
	    int size = 0;
	    //copy event to contiguous memory chunk
	    for (int i=0; i<event.length; i++)
		size+=event[i].length;
	    
	    event_contig = new int[size];
	    int s = 0;
	    for (int i=0; i<event.length; i++)
		for (int j=0; j<event[i].length; j++)
		{
		    event_contig[s] = event[i][j];
		    s++;
		}
	}
	else
        {
	    event_contig = event[0];
        }
        
        return event_contig;
    }
    
    //! Push an event to the local buffers
    /*
     * ! This method is invoked by the event sampler, a new thread is
     * automatically created for each invocation of this method. Events will be
     * stored in two local buffers, where they are waiting for being processed
     * (by the user) and forwarded to our child monitors (by the forwarding
     * thread). This method will wake up calls that might wait for event either
     * to forward them or to process them. The potential buffer size is 2 *
     * event_size * max_buffer_size. This is an IPC published method. \param
     * event event to push to the buffer \sa Forwarder
     */
    public int push_event(int[][] e)
    {	
	int[] event_contig = serialize( e );
	        	
	synchronized (EventBuffer)
	{
	    if (EventBuffer.size()<this.buffer_limit)
	    {
		isinfo.m_buffer_occupancy = EventBuffer.size();
		isinfo.m_received_events++;
		EventBuffer.add(event_contig);
		evt_sem.Post();
	    }
	    else
	    {
		isinfo.m_dropped_events++;
	    }
	}
        
        return 1000;
    }

    public int get_subtree_size( )
    {
	return sub_tree_size;
    }
        
    //! Send a connection request to the event conductor
    /*
     * ! This method will try to connect to an event sampler, by sending a
     * connection request to the conductor, which will either connect this
     * monitor to an existing monitoring tree or install this monitor as new
     * root monitor and start the sampling thread in the event sampler. It is
     * automatically invoked on object construction. \return whether or not the
     * connection to the sampler has been successfully established
     */
    public void start( )
    {    
	isinfo = new MonitorInfoNamed( partition, "Monitoring./Monitor/" + UniqueName );
        
	try
	{
	    EventMonitoring.Conductor conductor = partition.lookup(
            							EventMonitoring.Conductor.class,
								EventMonitoring.Conductor.Name );
	    if ( conductor == null )
	    {
		throw new CannotInitialize("conductor not present");
	    }
	    EventMonitoring.SamplingAddressHolder aholder = new EventMonitoring.SamplingAddressHolder( address );
            conductor.add_monitor( aholder, criteria, group_name, self() );
	    SamplerRunning = true;
	}
	catch (EventMonitoring.BadAddress ba)
	{
	    throw new BadAddress();
	}
	catch (EventMonitoring.NoResources nr)
	{
	    throw new NoResources();
	}
	catch (EventMonitoring.BadCriteria bc)
	{
	    throw new BadCriteria();
	}
	catch ( Exception ex )
	{
            throw new CannotInitialize( "can not add monitor " + ex );
	}
        try {
            isinfo.checkin();
        }
        catch( Exception ex )
        { }
    }
            
     //! Return the current size of the event buffer
    /*
     * ! Returns the number of events currently buffered in the event buffer.
     * Any events possibly buffered for sending to our children are not included
     * in this number. \return the number of events currently buffered
     */
    public int bufferSize()
    {
	synchronized ( EventBuffer )
	{
	    int size = EventBuffer.size();
	    return size;
	}	
    }
    
    
    //! Disconnect from the monitoring system
    /*
     * !
     */
    public void destroy()
    {
	publishing_.cancel();
            
	try
	{
	    EventMonitoring.Conductor conductor 
            	= partition.lookup( EventMonitoring.Conductor.class,
				    EventMonitoring.Conductor.Name );
	    conductor.remove_monitor( address, criteria, group_name, self() );
	    isinfo.checkin();
	}
	catch( Exception ex )
	{
	}
    }
}
