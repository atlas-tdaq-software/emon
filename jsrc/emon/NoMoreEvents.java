package emon;

//! Exception being thrown by the NextEvent method
/*
 * ! This exception is being thrown by the NextEvent method inside the
 * EventMonitor_impl class, in case we requested another event, before one has
 * been available 
 * @author Ingo Scholtes
 * @version 26/08/2004 \sa EventMonitor_impl
 */
public class NoMoreEvents extends Exception {
    //! empty constructor
    public NoMoreEvents() {
    }
}