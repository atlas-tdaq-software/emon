package emon;

//! NoResources Exception thrown by SamplingAddress class
/*!
*  @author Serguei Kolos
*/	
public final class NoResources extends RuntimeException
{
    //! empty constructor
  public NoResources()
  {
	super(EventMonitoring.NoResourcesHelper.id());
  }

  //! constructor, specifying an additional value, that may be used for detailed descriptions
  public NoResources(String value)
  {
	super(value);
  }
}
