package emon;

//! Exception being thrown by the NextEvent method
/*
 * ! This exception is being thrown by the NextEvent method inside the
 * EventMonitor_impl class, in case the sampler stopped/crashed before we
 * received the desired amount of events @author Ingo Scholtes
 * 
 * @version 26/08/2004 \sa EventMonitor_impl
 */
public class SamplerStopped extends Exception {
    //! constructor
    public SamplerStopped() {
    }
}