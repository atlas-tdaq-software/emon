package emon;

/*
 * ! This class provides a counting semaphore as proposed by Dijsktra,
 *
 * @author Ingo Scholtes
 *
 */
class Semaphore
{
  //! The Value of this Semaphore
  private int value;

  //! Constructor
  /*
   * ! Constructor creating a Semaphore object, setting the initial value of
   * this semaphore to the value specified. A value of n will allow to
   * perform n non-blocking wait-calls before performing any post-call.
   * \param value the initial value of this semaphore
   */
  public Semaphore(int value)
  {
      this.value = value;
  }

  //! Constructor
  /*
   * ! Constructor, setting initial value to 0. This will make the first
   * caller of Wait() to wait for the first Post()
   *
   */
  public Semaphore()

  {
      value = 0;
  }

  //! Increase the value and signal a waiting thread to wakeup
  /*
   * ! This method will increase the value of the semaphore by one and wake
   * up a waiting thread
   *
   */
  public synchronized void Post()
  {
      value++;
      notify();
  }

  //! An asynchronous version of Wait()
  /*
   * ! This method has the same functionality as Wait(), but instead of
   * blocking the thread it will return false immediately if the value of
   * the semaphore is zero. \sa Wait()
   */
  public synchronized boolean TryWait()
  {
      if (value==0)
	  return false;
      else
	  value--;
      return true;
  }

  //! Decrease the value of the semaphore
  /*
   * ! This method will decrease the value of the semaphore by one, causing
   * the calling thread to block if the value is zero or lower. The thread
   * will be unblocked, as soon as a Post() causes the semaphore counter to
   * be increased again. \sa TryWait()
   */
  public synchronized boolean Wait( long timeout )
  {
    if (value==0)
    {
	  try
	  {
	    wait( timeout );
	    if ( value == 0 )
		return false;
	  }
	  catch (InterruptedException ie)
	  {
		return false;
	  }
    }
    value--;
    return true;
  }
}
