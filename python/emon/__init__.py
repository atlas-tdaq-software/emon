"""Event monitoring service

This module is part of the Event Monitoring Service (EMON) 
package of the ATLAS TDAQ system.
"""

__author__ = "Serguei Kolos (Serguei.Kolos@cern.ch)"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2013/06/05 21:57:20 $"

import eformat
import sys

if sys.platform == 'linux2':
    import DLFCN
    flags = sys.getdlopenflags()
    sys.setdlopenflags( flags | DLFCN.RTLD_GLOBAL )
    from libemonpy import *
    sys.setdlopenflags( flags )
else:
    from libemonpy import *
