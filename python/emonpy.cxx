////////////////////////////////////////////
//    emonpy.cxx
//    Python wrappers for Event Monitoring classes
//    Serguei Kolos
//    Last modified: May. 12 2020
////////////////////////////////////////////
///
#include <boost/python.hpp>
#include <boost/python/enum.hpp>

#include <eformat/blob.h>
#include <emon/EventIterator.h>

using namespace emon;
using namespace std;
using namespace boost::python;

namespace
{
    template<typename T>
    struct vector_to_python_list
    {
	static PyObject * convert(std::vector<T> const& v)
	{
	    using namespace std;
	    using namespace boost::python;
	    using boost::python::list;
	    list l;
	    typename vector<T>::const_iterator p;
	    for(p=v.begin();p!=v.end();++p){
	      l.append(object(*p));
	    }
	    return incref(l.ptr());
	}
    };

    template<typename T>
    struct vector_from_python_list
    {
	vector_from_python_list()
	{
	    using namespace boost::python::converter;
            registry::push_back(
            	&vector_from_python_list<T>::convertible,
		&vector_from_python_list<T>::construct,
		type_id<vector<T>>());
	}

	// Determine if obj_ptr can be converted in a vector<T>
	static void* convertible(PyObject* obj_ptr)
	{
	    if (!PyList_Check(obj_ptr)){
		return 0;
	    }
	    return obj_ptr;
	}

	// Convert obj_ptr into a vector<T>
	static void construct( PyObject* obj_ptr,
			converter::rvalue_from_python_stage1_data* data)
	{
	    // Extract the character data from the python string
	    //	  const char* value = PyString_AsString(obj_ptr);
	    boost::python::list l(handle<>(borrowed(obj_ptr)));

	    // Verify that obj_ptr is a string (should be ensured by convertible())
	    // assert(value);

	    // Grab pointer to memory into which to construct the new vector<T>
	    void* storage = 
            	((converter::rvalue_from_python_storage<vector<T>>*)data)->storage.bytes;

	    // in-place construct the new vector<T> using the character data
	    // extraced from the python object
	    vector<T>& v = *(new (storage) vector<T>());

	    // populate the vector from list contains !!!
	    int le = len(l);
	    v.resize(le);
	    for(int i = 0;i!=le;++i){
		v[i] = extract<T>(l[i]);
	    }

	    // Stash the memory chunk pointer for later use by boost.python
	    data->convertible = storage;
	}
    };

    struct ContextManager
    {
    	ContextManager(
        	const IPCPartition & partition,
		const SamplingAddress & address,
		const SelectionCriteria & criteria )
	  : m_it(new emon::EventIterator(partition, address, criteria))
        { ; }
        
	eformat::helper::u32list next_event(size_t timeout)
	{
	    ERS_ASSERT(m_it);
	    emon::Event e = m_it->nextEvent( timeout );
	    size_t size = e.size();
	    return eformat::helper::u32list(e.yield_data(), 0, size);
	}

	eformat::helper::u32list try_next_event()
	{
	    ERS_ASSERT(m_it);
            emon::Event e = m_it->tryNextEvent();
	    size_t size = e.size();
	    return eformat::helper::u32list(e.yield_data(), 0, size);
	}

	ContextManager & __enter__()
	{
	    return *this;
	}

	bool __exit__(PyObject * /*ptype*/, PyObject * /*pvalue*/, PyObject * /*ptraceback*/)
	{
	    delete m_it;
	    m_it = 0;
	    return false;
	}
        
        emon::EventIterator * m_it;
    };
    
    
    typedef vector<string> SamplersList;
    
    PyObject * getEventSamplers(const IPCPartition & p)
    {
	map<string,EventMonitoring::EventSampler_var> samplers;
	p.getObjects<EventMonitoring::EventSampler,ipc::no_cache,ipc::non_existent>( 
        	samplers );

        boost::python::list l;
        map<string,EventMonitoring::EventSampler_var>::iterator it = samplers.begin();
	for ( ; it != samplers.end( ); ++it )
	{
	    l.append(object(it->first));
	}
        return incref(l.ptr());
    }
}

BOOST_PYTHON_MODULE(libemonpy)
{
    // register the from-python converter
    vector_from_python_list<unsigned short>();

    vector_from_python_list<string>();
    
    boost::python::def("getEventSamplers", &getEventSamplers);

    enum_<Logic>("Logic")
	    .value("IGNORE", logic::IGNORE)
	    .value("AND", logic::AND)
	    .value("OR", logic::OR)
	    .export_values();

    enum_<Origin>("Origin")
	    .value("BEFORE_PRESCALE", origin::BEFORE_PRESCALE)
	    .value("AFTER_PRESCALE", origin::AFTER_PRESCALE)
	    .value("AFTER_VETO", origin::AFTER_VETO)
	    .export_values();

    class_<SamplingAddress>( "SamplingAddress",
	    init<string,string>( args( "sampler_type", "sampler_name" ) ) )
	    .def(init<string,vector<string>>( args( "sampler_type", "sampler_names" ) ) );

    class_<MaskedValue<unsigned char>>( "L1TriggerType",
	    init<unsigned char, bool>( args( "value", "ignore" ) ) );

    class_<MaskedValue<unsigned int>>( "StatusWord",
	    init<unsigned int, bool>( args( "value", "ignore" ) ) );

    class_<SmartBitValue>( "L1TriggerBits",
	    init<vector<unsigned short>, Logic, Origin>( args( "bits", "logic", "origin" ) ) );

    class_<SmartStreamValue>( "StreamTags",
	    init<string, vector<string>, Logic>( args( "stream_type", "stream_names", "logic" ) ) );

    class_<SelectionCriteria>( "SelectionCriteria",
	    init<L1TriggerType, SmartBitValue, SmartStreamValue, StatusWord>(
		    args( "l1_trigger_type", "lvl1_trigger_bits", "stream_tags", "status_word" ) ) );

    class_<ContextManager, boost::noncopyable>( "EventIterator",
	    init<IPCPartition, SamplingAddress, SelectionCriteria>(
		    args( "partition", "sampling_address", "selection_criteria" ) ) )
            .def("nextEvent", &ContextManager::next_event)
            .def("tryNextEvent", &ContextManager::try_next_event)
            .def("__enter__", &ContextManager::__enter__, 
            	return_value_policy<reference_existing_object>())
            .def("__exit__", &ContextManager::__exit__);
}
