"""Event monitoring service

This is the test for Event Monitoring Service (EMON) 
package of the ATLAS TDAQ system.
"""

from __future__ import print_function

__author__ = "Serguei Kolos (Serguei.Kolos@cern.ch)"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2013/06/05 21:57:20 $"

import ers
import ipc
import emon

if __name__ == "__main__":
    partition = ipc.IPCPartition("test")
    try:
	samplers = emon.getEventSamplers(partition)
        print(samplers)
    except ers.Issue as e:
	ers.log( e )

    address = emon.SamplingAddress("DCM", "DCM-1")
    criteria = emon.SelectionCriteria(
    	emon.L1TriggerType(2, True),
        emon.L1TriggerBits([1, 2, 3], emon.Logic.AND, emon.Origin.AFTER_VETO),
        emon.StreamTags('StreamTag', ['Physics', 'Debug'], emon.Logic.OR),
        emon.StatusWord(0, False))
        
    try:
	with emon.EventIterator(partition, address, criteria) as iterator:
	    event = iterator.nextEvent(10)
	    print(event)
	    event = iterator.tryNextEvent()
	    print(event)
    except ers.Issue as e:
	ers.log( e )
