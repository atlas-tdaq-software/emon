#!/usr/bin/env python2
"""
Script aimed to show an interactive overview of ATLAS Event Monitoring using
the IS information for MonitorInfo and SamplerInfo object. To run the script,
one needs to set environment so that tdaq, matplotlib and numpy are accessible
"""
from __future__ import print_function

from past.builtins import cmp
from builtins import str
from builtins import object
import sys
import argparse
import time
import os
import pickle
import datetime

import numpy as np
import matplotlib.pyplot as pl

try:
    pl.figure()
except:
    print("ERROR: Can't access display, falling back to Agg backend, interactive plotting will not work")
    pl.switch_backend('Agg') #Using the backend that can work even when X server is not running

from matplotlib.widgets import Button
from matplotlib.patches import Rectangle

# If a monitor/sampler is info not updated for no_update_limit secs, it is considered not active
NO_UPDATE_LIMIT = 100

# Format of datetime strings used over the program
DATETIME_FORMAT = "%Y%m%d_%H%M%S"
FONT_SIZE = 12

monitor_attributes = ["m_host", "m_pid", "m_active", "m_sampler_type", "m_sampler_names",
    "m_selection_criteria", "m_group_name", "m_received_events", "m_buffered_events", "m_forwarded_events",
    "m_dropped_events", "m_child_monitors", "m_dispersion", "m_buffer_size", "m_buffer_occupancy"]

def compare_monitors_by_nchild(monitor1, monitor2):
    """ Compare 2 Monitor objects in terms of m_child, used as predicate in sorting.
        Can be used as:
        monitors.sort(cmp=compare_monitors)"""
    c1 = monitor1.is_dict["m_child_monitors"]
    c2 = monitor2.is_dict["m_child_monitors"]
    return cmp(c2, c1)

def compare_chains_by_size(chain1, chain2):
    """ Compare 2 MonitorChain objects in terms of size, used as predicate in sorting.
        Can be used as:
        chains.sort(cmp=compare_chains)"""
    return cmp(chain2.size, chain1.size)

def create_plot_name(dt):
    """ create plot file name """
    return dt.strftime("emon_" + DATETIME_FORMAT + ".png")

class MonitorChain(object):
    """ Represents a chain of monitors. """
    def __init__(self, last_monitor=None):
        super(MonitorChain, self).__init__()
        self.criteria = last_monitor.is_dict["m_selection_criteria"]
        self.criteriaobject = None
        self.monitors = []
        self.group_name = last_monitor.is_dict["m_group_name"]
        self.criteria_group = self.criteria + " - " + self.group_name
        self.last_monitor = last_monitor # Monitor with 0 childs in the chain
        self.xpos = 0    # x position of the last child monitor
        self.size = 0    # Size of chain, maximum of m_child_monitors attribute of
        self.hosts = []
        self.add_monitor(self.last_monitor)

    def __repr__(self):
        return "Criteria: {}, group name: {}, last monitor: {}, size: {}".format(self.criteria,
                                                          self.group_name, self.last_monitor.name, self.size)

    def set_xpos(self, pos):
        """ Set position of all monitors in chain"""
        difference = pos - self.xpos
        for m in self.monitors:
            m.xpos += difference
        self.xpos = pos

    def add_monitor(self, monitor):
        """ Add monitor to chain, updates chain attribute of monitor """
        self.monitors.append(monitor)
        monitor.chain = self
        self.update_size()
        if monitor.is_dict["m_host"] not in self.hosts:
            self.hosts.append(monitor.is_dict["m_host"])
            self.hosts.sort()

    def update_size(self):
        """ Update size checking monitors """
        self.size = max([m.is_dict["m_child_monitors"] for m in self.monitors]) + 1

    def draw(self, textbox=False):
        """ Draw monitors and criteria """
        for m in self.monitors:
            m.draw_parent_connections()
            m.draw(textbox=textbox)
        self.draw_criteria()

    def redraw(self, textbox=False):
        """ Redraw only monitors """
        for m in self.monitors:
            m.redraw(textbox=textbox)

    def draw_criteria(self):
        """ draw criteria box on top of plot """
        ax = pl.gca()
        self.criteriaobject = ax.text(self.xpos, self.size, self.criteria_group,
                                      verticalalignment='bottom', horizontalalignment='center',
                                      transform=ax.transData, color="black", alpha=0.8,
                                      fontsize=FONT_SIZE,
                                      bbox=dict(facecolor="white", alpha=0.5, lw=1))

    def toggle_criteria(self):
        """ Show/hide criteria """
        if self.is_criteria_visible():
            alpha = 0.
        else:
            alpha = 0.8
        self.criteriaobject.set_alpha(alpha)
        self.criteriaobject.get_bbox_patch().set_alpha(alpha)

    def is_criteria_visible(self):
        """ True if criteria is visible"""
        return self.criteriaobject.get_alpha() > 0

    def get_monitor_by_name(self, monitorname):
        """ Get monitor using it's IS name """
        for m in self.monitors:
            if monitorname == m.name:
                return m

        raise RuntimeError("Monitor not found in chain!")

class SamplerMonitorVisualizer(object):
    """ Represents graphical visualization of the sampler/monitors """
    def __init__(self, visualizer_type):
        super(SamplerMonitorVisualizer, self).__init__()
        self.plottextbox = None
        self.plotmarker = None
        self.criteriabox = None
        self.xpos = 0    # x position
        self.ypos = 0    # y position
        self.color = None
        self.size = 10
        self.text = "None"
        if visualizer_type == "Monitor":
            self.lw = 1
            self.marker = "o"
        elif visualizer_type == "Sampler":
            self.lw = 2
            self.marker = "s"
            self.color = "white"
        else:
            raise RuntimeError("Visualizer type unknown!!!")

    def draw(self, textbox=False):
        """ Draw a marker or text box with information of Monitor/Sampler in it"""
        ax = pl.gca()
        self.plottextbox = ax.text(self.xpos, self.ypos, self.text, verticalalignment='bottom',
                                   horizontalalignment='center', transform=ax.transData,
                                   color="black", fontsize=FONT_SIZE,
                                   bbox=dict(facecolor=self.color, lw=self.lw),
                                   picker=1, label=self.name)
        self.plotmarker = pl.plot(self.xpos, self.ypos, marker=self.marker, ms=self.size,
                                  color=self.color, picker=5, label=self.name)[0]
        self.redraw(textbox=textbox)

    def redraw(self, textbox=False):
        """ Show only textbox or marker, by setting transparency of other to 0 """
        if textbox:
            self.plottextbox.set_alpha(0.8)
            self.plottextbox.get_bbox_patch().set_alpha(0.8)
            self.plottextbox.set_picker(1) # If textbox is visible, listen to click events on it
            self.plotmarker.set_alpha(0)
        else:
            self.plottextbox.set_alpha(0)
            self.plottextbox.get_bbox_patch().set_alpha(0)
            self.plottextbox.set_picker(None)
            self.plotmarker.set_alpha(0.8)

class Sampler(SamplerMonitorVisualizer):
    """Represents the sampler object"""
    def __init__(self, name, issampler=None):
        super(Sampler, self).__init__("Sampler")
        self.is_dict = dict()
        self.update_time = None
        self.active = False
        self.name = name

        # Fill monitor info from is object
        if issampler != None:

            #Dump all is info into dict attribute (Not all is information is needed)
            self.is_dict = dict(issampler)

            # Unix time stamp of IS publication
            self.update_time = issampler.time().c_time()

            if (time.time() - self.update_time < NO_UPDATE_LIMIT) and self.is_dict["m_active"]:
                self.active = True

        self.text = self.name.replace("Monitoring./Sampler/", "")

    def __repr__(self):
        return "{0}, {1}".format(self.name, self.is_dict)

class Monitor(SamplerMonitorVisualizer):
    """Represents the monitor object"""
    def __init__(self, name, ismonitor=None):
        """ Create a monitor object. If ismonitor is free, one has to set xpos by hand"""
        super(Monitor, self).__init__("Monitor")
        self.is_dict = dict()
        self.update_time = None
        self.parents = []
        self.active = False
        self.child = None #Child monitor (object, not name)
        self.chain = None #MonitorChain which monitor belongs to
        self.name = name

        # Fill monitor info from is object
        if ismonitor != None:

            #Dump all is info into dict attribute (Not all is information is needed)
            self.is_dict = dict(ismonitor)

            # Unix time stamp of IS publication
            self.update_time = ismonitor.time().c_time()

            if (time.time() - self.update_time < NO_UPDATE_LIMIT) and self.is_dict["m_active"]:
                self.active = True

            self.ypos = self.is_dict["m_child_monitors"]

        self.text = self.name.replace("Monitoring./Monitor/", "")

    def get_child_monitor_name(self, monitors):
        """ Returns child monitor name if it's already set, otherwise set it and then return.
            It uses list of all monitors"""
        try: # In case we add a new attribute to IS info
            return self.is_dict["m_child_monitor_name"]
        except KeyError:
            if self.is_dict["m_child_monitors"] == 0:
                self.is_dict["m_child_monitor_name"] = "None"
                return self.is_dict["m_child_monitor_name"]
            for m in monitors:
                child_monitors_condition = m.is_dict["m_child_monitors"] == self.is_dict["m_child_monitors"]-1
                criteria_condition = m.is_dict["m_selection_criteria"] == self.is_dict["m_selection_criteria"]
                group_name_condition = m.is_dict["m_group_name"] == self.is_dict["m_group_name"]
                same_samplers_condition = self.uses_same_samplers(m)
                if child_monitors_condition and criteria_condition and same_samplers_condition and group_name_condition:
                    self.is_dict["m_child_monitor_name"] = m.name
                    return self.is_dict["m_child_monitor_name"]

    def uses_same_samplers(self, m2):
        """ If 2 monitors are attached to at least 1 same sampler, return true"""
        samplers1 = set(self.is_dict["m_sampler_names"])
        samplers2 = set(m2.is_dict["m_sampler_names"])
        return samplers1.intersection(samplers2) != set()

    def fill_parent_positions(self):
        """ Fills position of each parent, depending on number of parents """
        nparents = len(self.parents)
        if nparents == 1: # In case of single parent, draw above
            xpositions = [self.xpos]
        elif nparents > 1: # In case of multiple parents, draw centering on the current monitor
            xstep = 1./pow(2, self.ypos+1)
            center = self.xpos
            xpositions = np.linspace(center - xstep, center + xstep, nparents)

        i = 0
        for p in self.parents:
            p.xpos = xpositions[i]
            i += 1

    def fill_parents(self, monitors):
        """ By browsing through list of monitors, find parents of the monitor and fills:
            - parents member variable
            - child member variable of parents
            - chain member variable of parents
            This method is recursive, it tells parents to find their parents too.
            Running on the last monitor in chain, it will fill information of the whole chain
            """
        for m in monitors:
            child_monitor_name = m.get_child_monitor_name(monitors)
            if child_monitor_name == self.name:
                self.parents.append(m)
                m.child = self

        self.fill_parent_positions()

        if self.is_dict["m_child_monitors"] == 0:
            self.create_chain()
        else:
            self.child.chain.add_monitor(self) # Adapt chain of child

        # Run recursively for all parents
        for p in self.parents:
            p.fill_parents(monitors)

    def create_chain(self):
        """ Create new chain for the monitor and add it to chain attribute """
        MonitorChain(last_monitor=self)

    def __repr__(self):
        return "{0} -> type: {1}, active: {2}, criteria: {3}, n_childs: {4}".format(
              self.name, self.is_dict["m_sampler_type"], self.active,
              self.is_dict["m_selection_criteria"], self.is_dict["m_child_monitors"])

    def summary(self):
        """ Return summary string """
        summary = "{:20s} : {}\n".format("Name", self.name)
        for key in monitor_attributes:
            if key not in self.is_dict:
                summary += "{:20s} : {}\n".format(key, "None")
                continue
            if key == "m_sampler_names":
                summary += "{:20s} : {}\n".format("Total samplers", len(self.is_dict[key]))
            else:
                summary += "{:20s} : {}\n".format(key, self.is_dict[key])
        return summary

    def draw_parent_connections(self):
        """ Draws connections of child with all parents """
        for p in self.parents:
            pl.plot([self.xpos, p.xpos], [self.ypos, p.ypos], color="black", alpha=0.3, lw=2)

    def draw_samplers(self):
        """ Draw samplers of the current monitor """

        sampler_names = self.is_dict["m_sampler_names"]
        nsamplers = len(sampler_names)
        if nsamplers == 1: # In case of single sampler
            xpositions = [self.xpos]
        elif nsamplers > 1: # In case of multiple samplers, draw centering on the current monitor
            xstep = 1./pow(2, self.chain.size+1)
            center = self.xpos
            xpositions = np.linspace(center - xstep, center + xstep, nsamplers)

        i = 0
        for name in sampler_names:
            s = Sampler(name)
            s.xpos = xpositions[i]
            s.ypos = self.chain.size + 1
            i += 1
            pl.plot([self.xpos, s.xpos], [self.ypos, s.ypos], color="green", alpha=0.3, lw=2)
            s.draw(textbox=False)

class MonitorManager(object):
    """Class that manages list of monitors"""
    def __init__(self, monitors, sampler_type=None, criteria=None, min_chain_size=1):
        super(MonitorManager, self).__init__()
        self.monitors = filter_monitors(monitors, sampler_type, criteria)
        self.monitors.sort(cmp=compare_monitors_by_nchild)
        self.hosts = self.get_monitoring_hosts()
        self.colormap = self.create_colormap()
        self.min_chain_size = min_chain_size
        self.leg = None
        self.chains = []

        if len(self.monitors) == 0:
            print("No monitors with such criteria/sampler type, exiting")
            sys.exit(0)

    def build_monitoring_tree(self):
        """ Build tree going through few steps """
        print("Building Monitoring Tree")
        self.fill_all_childs()
        self.fill_all_parents()
        self.remove_monitors_without_chain()
        self.create_and_filter_chains()
        self.set_chain_xpos()
        self.set_monitor_colors()

    def fill_all_childs(self):
        """ Fills child monitors for all monitors """
        for m in self.monitors:
            m.get_child_monitor_name(self.monitors)

    def fill_all_parents(self):
        """ Fills parents of all monitors """

        monitors_with_zero_child = 0

        # Start from monitors without childs, and find all parents
        for m in self.monitors:
            if m.is_dict["m_child_monitors"] == 0:
                monitors_with_zero_child += 1
                m.fill_parents(self.monitors)

        if not monitors_with_zero_child:
            print("There are no monitors with 0 child, something is wrong. Is partition running?")
            sys.exit(0)

    def remove_monitors_without_chain(self):
        """ Remove monitors whose chain member is None.
            This probably means we are early in the run and monitoring tree is not consistent"""

        monitors_before_removal = set(self.monitors)
        self.monitors = [m for m in self.monitors if m.chain != None]
        monitors_after_removal = set(self.monitors)
        removed = monitors_before_removal.difference(monitors_after_removal)
        if removed:
            removed_str = "\n".join([str(m) for m in removed])
            print("Removed monitors that doesn't belong to a chain:\n{}".format(removed_str))

    def create_and_filter_chains(self):
        """ Create chain list, and filter it by allowed maximum of monitors in chain """
        chainlist = [m.chain for m in self.monitors if m.chain.size >= self.min_chain_size]
        self.chains = list(set(chainlist))
        self.chains.sort(cmp=compare_chains_by_size)
        monitors = []
        for c in self.chains:
            monitors += c.monitors
        self.monitors = monitors
        if len(self.chains) == 0:
            print("No monitoring chains that satisfy chain length criteria, exiting...")
            sys.exit(0)

    def set_chain_xpos(self):
        """ Sets position of each chain """
        xpos = 0 # Start from 0 in x position, each chain should be put +2 further
        for chain in self.chains:
            chain.set_xpos(xpos)
            xpos += 4

    def set_monitor_colors(self):
        """ sets colors of each monitors using host names. """
        for m in self.monitors:
            host = m.is_dict["m_host"]
            m.color = self.colormap[host]

    def get_monitoring_hosts(self):
        """ Find and return list of all hosts using list of monitors"""
        hosts = []
        for m in self.monitors:
            host = m.is_dict["m_host"]
            if not host in hosts:
                hosts.append(host)
        hosts.sort() # Sorting hosts, so color mapping will always be same for same list of hosts
        return hosts

    def get_chain_from_monitorname(self, monitorname):
        """ Get chain that monitor belongs to, using monitors IS name """
        for c in self.chains:
            if monitorname in [m.name for m in c.monitors]:
                return c

        # Raise exception if not found in any chain
        raise RuntimeError("Monitor not found in any chains!")

    def create_colormap(self):
        """ Return a dictionary of hosts and associated colors """

        #Create a map of minimum 10 colors
        #cmap = pl.cm.get_cmap("Paired", max(len(hosts), 10))
        cmap = pl.cm.get_cmap("Set2", max(len(self.hosts), 10))

        colors = {}
        i = 0
        for host in self.hosts:
            colors[host] = cmap(i)
            i += 1

        return colors

    def draw_tree(self, textbox=False):
        """ Draw connections and nodes of monitors for all chains"""

        print ("Drawing all monitors")
        for chain in self.chains:
            chain.draw(textbox=textbox)

    def redraw_tree(self, textbox=False):
        """ Draw only nodes of monitors for all chains"""

        print ("Redrawing all monitors")
        for chain in self.chains:
            chain.redraw(textbox=textbox)

    def draw_legend(self, chain=None):
        """ Draw legend using host names and nodes of monitors
            If a chain in given as argument, only draw the hosts in the chain"""

        ax = pl.gca()

        all_recs = []
        if chain:
            hosts = chain.hosts
        else:
            hosts = self.hosts

        for host in hosts:
            # Put them out of axis as we only care for the legend
            rec = Rectangle((2.0, 2.0), 0.1, 0.1, transform=ax.transAxes, color=self.colormap[host],
                            fill=True, alpha=0.5, label=host)
            ax.add_patch(rec)
            all_recs.append(rec)

        self.leg = pl.legend(all_recs, hosts, loc=0, fancybox=True)
        self.leg.get_frame().set_alpha(0.4)

    def draw_all_samplers(self):
        """ Draw samplers of monitors without parents
            With this implementation, one misses monitors that have both parents
            and direct sampler connections"""

        print ("Drawing all samplers")
        for m in self.monitors:
            if m.parents == []:
                m.draw_samplers()

def retrieve_from_pbeast(server, partition, timestamp, object_type='Monitor'):
    """ Find all monitors in the partition
        Return a list of monitor objects"""

    import libpbeastpy
    pbeast = libpbeastpy.ServerProxy(server)
    print("Getting {}s from pbeast".format(object_type))

    # Retrieve only some parameters to make it faster.
    if object_type == 'Monitor':
        attributes = ["m_active", "m_sampler_type", "m_host", "m_child_monitors",
            "m_selection_criteria", "m_sampler_names", "m_dropped_events",
            "m_forwarded_events", "m_buffered_events", "m_received_events"]
        is_type = "MonitorInfo"
    elif object_type == 'Sampler':
        attributes = ["m_active", "m_type", "m_host"]
        is_type = "SamplerInfo"
    else:
        raise RuntimeError("No such type: {}, please use 'Monitor' or 'Sampler'".format(object_type))

    timestamp = int(timestamp)
    object_names = pbeast.get_objects(partition, is_type, 'm_active',
                                 'Monitoring./{}/.*'.format(object_type),
                                 since=timestamp*1000000, until=timestamp*1000000)
    objects = []
    i = 0
    for is_name in object_names:
        percent = i * 100 // len(object_names)
        # wget like progress bar
        sys.stdout.write('\r%3s%% [%s>%s]' % (percent, '='*percent, ' '*(100-percent)))
        sys.stdout.flush()
        i += 1
        is_dict = {}
        try:
            for attr in attributes:
                pbeastdata = pbeast.get_data(partition, is_type, attr, is_name, False,
                                             timestamp * 1000000, timestamp * 1000000)
                is_dict[attr] = pbeastdata[0].data[is_name][0].value

            if object_type == 'Monitor':
                obj = Monitor(name=is_name)
                obj.ypos = is_dict["m_child_monitors"]
            else:
                obj = Sampler(name=is_name)

            obj.is_dict = is_dict
            obj.active = obj.is_dict["m_active"]
            objects.append(obj)
        except Exception as e:
            print("Can't read object: {0} due to: {1} ".format(is_name, e))

    print("")
    return objects

def retrieve_from_is(partition, object_type='Monitor'):
    """ Find all monitors/samplers in the partition
        Return a list of Monitor/Sampler instances """

    from ispy import IPCPartition, ISInfoStream, ISCriteria, ISInfoDynAny, IS
    import libispy
    print("Getting {}s from IS".format(object_type))

    if object_type == "Monitor":
        is_type = IS.MonitorInfo
        ObjectClass = Monitor
    elif object_type == "Sampler":
        is_type = IS.SamplerInfo
        ObjectClass = Sampler
    else:
        raise RuntimeError("No such type: {}, use 'Monitor' or 'Sampler'".format(object_type))

    p = IPCPartition(partition)
    stream = ISInfoStream(p, 'Monitoring', ISCriteria('/{}/.*'.format(object_type),
                          is_type(p, '').type(), libispy.Logic.AND), False, 1)
    objects = []

    while not stream.eof():
        object_name = stream.name()
        isobject = ISInfoDynAny()
        stream.get(isobject)
        newobject = ObjectClass(object_name, isobject)
        objects.append(newobject)

    return objects

def retrieve_from_file(filename):
    """ Read from pickle file instead of IS/Pbeast"""
    f = open(filename, "r")
    monitors, samplers = pickle.load(f)
    f.close()
    return monitors, samplers

def dump_info_to_file(monitors, samplers, dt):
    """ Save info to pickle file """
    filename = "dumpemon_{0}.dat".format(dt.strftime(DATETIME_FORMAT))
    f = open(filename, "w")
    pickle.dump([monitors, samplers], f)
    f.close()
    print("Dumped monitors/samplers into file: {}".format(filename))

def filter_monitors(all_monitors, sampler_type, criteria):
    """ Returns monitors with selected criteria, sampler type """

    selected_monitors = []
    for m in all_monitors:
        criteria_satisfied = m.is_dict["m_selection_criteria"] == criteria or criteria == None
        sampler_type_satisfied = m.is_dict["m_sampler_type"] == sampler_type or sampler_type == "all"
        if sampler_type_satisfied and m.active and criteria_satisfied:
            selected_monitors.append(m)

    return selected_monitors

def filter_samplers(all_samplers, sampler_type):
    """ Return filtered sampler list """
    selected_samplers = []
    for s in all_samplers:
        sampler_type_satisfied = s.is_dict["m_type"] == sampler_type or sampler_type == "all"
        if sampler_type_satisfied and s.active:
            selected_samplers.append(s)

    return selected_samplers

def parse_arguments():
    """ Parse command line arguments """

    epilog = "This program makes an interactive plot of the monitoring chains\n"
    epilog += "that fit certain criterias\n\n"
    epilog += "\n"
    epilog += "Examples: \n\n"
    epilog += "monitor_emon.py -p ATLAS\n"
    epilog += "  (Plot all monitors of current ATLAS run)\n\n"
    epilog += "monitor_emon.py -p ATLAS -c \"(*:*:*:*)\"\n"
    epilog += "  (Plot monitors of current ATLAS run, that has criteria (*:*:*:*))\n\n"
    epilog += "monitor_emon.py -p ATLAS -t dcm\n"
    epilog += "  (Plot monitors of current ATLAS run, that is connected to dcm samplers\n\n"
    epilog += "monitor_emon.py -F dumpemon_20170817_135832.dat -l 4\n"
    epilog += "  (Plot monitors from dump file, for chains with more than 4 monitors)\n\n"
    epilog += "monitor_emon.py -p ATLAS -T 1502971922\n"
    epilog += "  (Plot monitors for ATLAS partition from pbeast at a certain time (GPN))\n\n"
    epilog += "monitor_emon.py -p ATLAS -T 1502971922 -s http://pc-tdq-bst-02.cern.ch:8080\n"
    epilog += "  (Plot monitors for ATLAS partition from pbeast at certain time (P1))\n\n"
    epilog += "monitor_emon.py -p ATLAS --noplot\n"
    epilog += "  (Dump serialized information for ALL monitors into a file, don't plot)\n"

    parser = argparse.ArgumentParser(epilog=epilog, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-p', '--partition', help="Partition name", default="ATLAS")
    parser.add_argument('-t', '--samplertype', default="all",
                        help="Sampler type (dcm/ReadoutApplication/...or all), default: all")
    parser.add_argument('-c', '--criteria', default=None,
                        help="Sampling criteria such as (*:*:*:*), default: All criterias")
    parser.add_argument('-l', '--chainsizelimit', default=0, type=int,
                        help="Discard monitoring chains with smaller size")
    parser.add_argument('--dump', action="store_true",
                        help="Save the state of monitoring in a dump file in current directory."
                             "The saved file can be used later with -F option")
    parser.add_argument('--noplot', action="store_true",
                        help="Don't plot, only save the serialized objects in a dump file."
                             " Can't be used with -F and -T options")
    parser.add_argument('-F', '--filename', default=None,
                        help="Read monitors and samplers from previous dump file"
                             " instead of from online environment. Can't be used"
                             " with -T option")

    # Following arguments are used for checking a past run from pbeast
    pbeastgroup = parser.add_argument_group("Arguments used for reading a state of "
                                            "Monitors/Samplers from a previous run "
                                            "from pbeast")
    pbeastgroup.add_argument('-T', '--timestamp',
                             help="Unix timestamp for reading information from pbeast."
                                  " Can't be used with -F option")
    pbeastgroup.add_argument('-s', '--server', default='https://atlasop.cern.ch',
                             help="Pbeast server url. For GPN: https://atlasop.cern.ch, "
                                  "for P1: http://pc-tdq-bst-02.cern.ch:8080")

    args = parser.parse_args()
    return args

class PlotEventHandler(object):
    """ Handler class for plotting and event handling in plot such as:
        - button actions
        - clicks on monitors
        - tooltip when moving mouse cursor hovers on monitors
    """

    def __init__(self, monitor_manager):
        self.manager = monitor_manager
        self.textbox = False
        self.current_chain = None
        self.last_event = None
        self.manager.draw_tree()
        #self.manager.draw_all_samplers() # This doesn't work as well
        self.ax = pl.gca()
        self.fig = pl.gcf()
        self.ind = -1 # Index used to count chains

        # Boolean, True if criteria is plotted
        self.criteriavisible = self.manager.chains[0].is_criteria_visible()

        # Text box to be displayed when clicked on a monitor
        self.detailbox = self.ax.text(0.0, 0.5, "",
                                      verticalalignment="center",
                                      transform=self.ax.transAxes,
                                      fontdict={"family":"monospace"},
                                      bbox=dict(facecolor="white", alpha=0.7))
        self.detailbox.set_visible(False)

        # Text box to show when cursor hovers on monitor marker
        self.hover_textbox = self.ax.text(-1, -1, "",
                              verticalalignment='bottom', horizontalalignment='left',
                              transform=self.ax.transData, color="blue", alpha=0.8,
                              fontsize=FONT_SIZE,
                              bbox=dict(facecolor="white", alpha=0.95, lw=1))
        self.hover_textbox.set_visible(False)

        # Criteria box to show when cursor hovers on monitor marker
        self.criteria_textbox = self.ax.text(-1, -1, "",
                              verticalalignment='bottom', horizontalalignment='center',
                              transform=self.ax.transData, color="black", alpha=0.8,
                              fontsize=FONT_SIZE,
                              bbox=dict(facecolor="white", alpha=0.95, lw=1))
        self.criteria_textbox.set_visible(False)

        self.create_buttons()

        self.fig.canvas.mpl_connect('pick_event', self.onpick)
        self.fig.canvas.mpl_connect('motion_notify_event', self.onhover)
        self.draw_cid = self.fig.canvas.mpl_connect('draw_event', self.grab_background)
        self.set_axis_props()

        self.show_all(event=None)

        #self.fig.set_size_inches(20, 14)
        #pl.savefig(create_plot_name(dt), bbox_inches='tight')
        #self.show_all(event=None)

        pl.show()

    def grab_background(self, event=None):
        """
        When the figure is resized, hide the points, draw everything,
        and update the background.
        """
        #self.points.set_visible(False)
        self.safe_draw()

        # With most backends (e.g. TkAgg), we could grab (and refresh, in
        # self.blit) self.ax.bbox instead of self.fig.bbox, but Qt4Agg, and
        # some others, requires us to update the _full_ canvas, instead.
        self.background = self.fig.canvas.copy_from_bbox(self.fig.bbox)

        #self.points.set_visible(True)
        self.blit()

    def safe_draw(self):
        """Temporarily disconnect the draw_event callback to avoid recursion"""
        canvas = self.fig.canvas
        canvas.mpl_disconnect(self.draw_cid)
        canvas.draw()
        self.draw_cid = canvas.mpl_connect('draw_event', self.grab_background)

    def create_buttons(self):
        """ Create buttons and set props """
        axdetail = pl.axes([0.09, 0.05, 0.1, 0.075])
        axcrit = pl.axes([0.2, 0.05, 0.1, 0.075])
        axtoggle = pl.axes([0.31, 0.05, 0.1, 0.075])
        axprev = pl.axes([0.5, 0.05, 0.1, 0.075])
        axall = pl.axes([0.61, 0.05, 0.1, 0.075])
        axnext = pl.axes([0.72, 0.05, 0.1, 0.075])

        self.bdetail = Button(axdetail, 'Hide\nDetails')
        self.bcrit = Button(axcrit, 'Toggle\nCriteria')
        self.btoggle = Button(axtoggle, 'Toggle\nMarker/Box')
        self.bnext = Button(axnext, 'Next\nChain')
        self.ball = Button(axall, 'Show All\nChains')
        self.bprev = Button(axprev, 'Previous\nChain')

        self.bdetail.on_clicked(self.remove_detailbox)
        self.bcrit.on_clicked(self.toggle_criteria)
        self.btoggle.on_clicked(self.toggle_marker_textbox)
        self.bnext.on_clicked(self.next)
        self.ball.on_clicked(self.show_all)
        self.bprev.on_clicked(self.prev)

        self.set_button_props()

        # Set current axis back to standard one
        pl.sca(self.ax)

    def set_axis_props(self):
        """ Set axis properties """
        pl.subplots_adjust(bottom=0.2)
        self.ax.set_axis_off()
        self.ax.margins(0, 0, tight=True)
        self.fig.patch.set_facecolor('white')

    def hover_watchdog(self):
        """ Watchdog to remove hover textbox when nothing is on it,
            need to be started in seperate thread"""
        while 1:
            time.sleep(0.5)
            self.remove_hover_textbox()

    def set_criteria_button_props(self):
        if self.criteriavisible:
            self.bcrit.label.set_text("Hide\ncriteria")
        else:
            self.bcrit.label.set_text("Show\ncriteria")

    def set_toggle_button_props(self):
        if self.textbox:
            self.btoggle.label.set_text("Hide\nmonitor names")
        else:
            self.btoggle.label.set_text("Show\nmonitor names")

    def set_button_props(self):
        self.set_criteria_button_props()
        self.set_toggle_button_props()
        self.bdetail.label.set_color("gray")
        self.bdetail.set_active(False)

    def draw_current(self):
        """ Set index and draw current chain """
        self.ind = self.ind % len(self.manager.chains)
        self.current_chain = self.manager.chains[self.ind]
        self.draw_current_chain()

    def draw_current_chain(self):
        """ Focus on current chain """
        self.remove_detailbox()
        self.remove_hover_textbox()
        self.remove_criteria_textbox()
        self.ball.label.set_color("black")
        self.ball.set_active(True)

        xpos = self.current_chain.xpos
        size = self.current_chain.size
        self.manager.draw_legend(self.current_chain)
        self.ax.set_xlim(xpos-2, xpos+1)
        self.ax.set_ylim(-1, size+3)
        if not self.textbox:
            self.toggle_marker_textbox(None, True)
        if not self.criteriavisible:
            self.toggle_criteria(None, True)
        title = "Click on a monitor to display detailed information"
        title += "\nShowing chain {}/{}".format(self.ind+1, len(self.manager.chains))
        pl.title(title)
        pl.draw()

    def next(self, event):
        """ Show next chain """
        print("Show next chain")
        self.ind += 1
        self.draw_current()

    def prev(self, event):
        """ Show previous chain """
        print("Show previous chaun")
        if self.ind != -1:
            self.ind -= 1
        self.draw_current()

    def show_all(self, event):
        """ Show all chains """
        self.remove_detailbox()
        print("Show all chains")
        self.ind = -1
        max_xpos = max([chain.xpos for chain in self.manager.chains])
        max_size = max([chain.size for chain in self.manager.chains])
        self.manager.draw_legend()

        self.ax.set_xlim(-max_xpos*0.25, max_xpos+max_xpos*0.25)
        self.ax.set_ylim(-1, max_size+3)
        if self.textbox:
            self.toggle_marker_textbox(None, True)
        pl.title("Click on a monitor to focus on monitoring chain")

        if self.criteriavisible:
            self.toggle_criteria(None, True)

        self.ball.label.set_color("gray")
        pl.draw()

    def toggle_marker_textbox(self, event=None, nodraw=False):
        """ Toggle between plot marker and text box"""
        print("Toggling marker/textbox")
        if self.textbox:
            self.manager.redraw_tree(textbox=False)
        else:
            self.manager.redraw_tree(textbox=True)

        self.textbox = not self.textbox
        self.set_toggle_button_props()
        if not nodraw:
            pl.draw()

    def toggle_criteria(self, event, nodraw=False):
        """ Show/hide criteria"""
        print("Toggling criteria")
        for chain in self.manager.chains:
            chain.toggle_criteria()
        self.criteriavisible = not self.criteriavisible
        self.set_criteria_button_props()
        if not nodraw:
            pl.draw()

    def onpick(self, event):
        """ Actions to take when a marker/textbox on the plot is clicked on """
        thisobject = event.artist
        name = thisobject.get_label()

        # Sometimes a click will trigger multiple actions, next try/except block prevents
        # multiple actions to be taken by same click, all but first are ignored
        if self.last_event != None:
            if self.last_event.mouseevent == event.mouseevent:
                return 0

        if self.ind == -1: # If we are NOT focused on a chain, focus on it
            self.current_chain = self.manager.get_chain_from_monitorname(name)
            print("Clicked on chain: {}".format(self.current_chain))
            self.ind = self.manager.chains.index(self.current_chain)
            self.draw_current_chain()
        else: # If we are focused on a chain, print details of a monitor that is clicked at
            # A click can create multiple events, if the events are originated from same click,
            # don't do anything for the other events
            self.current_chain = self.manager.get_chain_from_monitorname(name)
            monitor = self.current_chain.get_monitor_by_name(name)
            print("Clicked on monitor: {}".format(monitor.name))
            print(monitor.summary())
            self.detailbox.set_text(monitor.summary())
            self.detailbox.set_visible(True)
            self.bdetail.label.set_color("black")
            self.bdetail.set_active(True)

        # Update the last event, so next events that are originated from same click will be ignored
        self.last_event = event

        pl.draw()

    def remove_hover_textbox(self):
        """ Remove textbox that is displayed on hover"""

        if not self.hover_textbox.get_visible():
            #print("already invisible")
            return 0
        self.hover_textbox.set_visible(False)
        self.hover_textbox.set_position([-1, -1])

        #pl.draw()
        self.blit() # For some reason blit doesn't work when removing the textbox

    def remove_criteria_textbox(self):
        """ Remove criteria textbox that is displayed on hover"""

        if not self.criteria_textbox.get_visible():
            #print("already invisible")
            return 0
        self.criteria_textbox.set_visible(False)
        self.criteria_textbox.set_position([-1, -1])

        #pl.draw()
        self.blit() # For some reason blit doesn't work when removing the textbox

    def onhover(self, event):
        """ Actions to take when the cursor hovers on marker/textbox"""

        if self.textbox:
            return 0 # Do nothing if we displays textboxes instead of markers

        hover_on_marker = False
        for m in self.manager.monitors:
            if m.plotmarker.contains(event)[0]: # Cursor is on the monitor marker
                hover_on_marker = True
                current = m
                break # monitor being hovered on already found

        # Cursor not on any marker (it can be on a textbox or other plot element)
        if not hover_on_marker:
            #print ("Not on marker")
            self.remove_hover_textbox()
            self.remove_criteria_textbox()
            return 0

        # Cursor is on the same marker as last time
        if (current.xpos, current.ypos) == self.hover_textbox.get_position():
            #print("Same one, do nothing")
            return 0

        self.remove_hover_textbox()
        self.remove_criteria_textbox()

        self.hover_textbox.set_position((current.xpos, current.ypos))
        self.hover_textbox.set_text(current.text)
        self.hover_textbox.set_visible(True)

        self.criteria_textbox.set_position((current.chain.xpos, current.chain.size))
        self.criteria_textbox.set_text(current.chain.criteria_group)
        self.criteria_textbox.set_visible(True)

        self.blit()

    def blit(self):
        """ Efficiently redraw hover_textbox without drawing everything
            doesn't work for removal of the textbox for some reason """
        canvas = self.fig.canvas

        # restore the background region
        canvas.restore_region(self.background)

        # redraw just the current textbox
        self.ax.draw_artist(self.hover_textbox)
        self.ax.draw_artist(self.criteria_textbox)

        # blit just the redrawn area
        canvas.blit(self.ax.bbox)

    def remove_detailbox(self, event=None):
        """ Remove detailbox from the plot """
        if self.detailbox.get_visible():
            self.detailbox.set_visible(False)
            self.bdetail.label.set_color("gray")
            self.bdetail.set_active(False)
            pl.draw()

def main():
    """ main function to run if script is called directly """
    args = parse_arguments()

    # Get datetime from the file, if reading from file
    if args.filename:
        filename = os.path.basename(args.filename)
        datetimestr = filename.lstrip("dumpemon_").rstrip(".dat")
        dt = datetime.datetime.strptime(datetimestr, DATETIME_FORMAT)
    else:
        dt = datetime.datetime.now()

    print("Arguments: {0}".format(args))

    if args.filename: # Read from file
        try:
            samplers = []
            monitors = []
            monitors, samplers = retrieve_from_file(args.filename)
        except Exception as e:
            print("Can't get stuff from file due to: {0}".format(e))
            raise
    elif args.timestamp: # Read from pbeast
        try:
            samplers = []
            monitors = []
            #samplers = retrieve_from_pbeast(server=args.server, partition=args.partition,
            #                                timestamp=args.timestamp, object_type='sampler')
            monitors = retrieve_from_pbeast(server=args.server, partition=args.partition,
                                            timestamp=args.timestamp, object_type='Monitor')
        except Exception as e:
            print("Can't get stuff from pbeast due to: {0}".format(e))
            raise
    else: # Read from IS
        try:
            samplers = []
            monitors = []
            samplers = retrieve_from_is(partition=args.partition, object_type='Sampler')
            monitors = retrieve_from_is(partition=args.partition, object_type='Monitor')
        except Exception as e:
            print("Can't get stuff from IS due to: {0}".format(e))
            raise

    if (args.dump or args.noplot) and not args.filename:
        dump_info_to_file(monitors, samplers, dt)
    if args.noplot:
        print("'noplot' option selected, exiting...")
        sys.exit(0)

    samplers = filter_samplers(samplers, args.samplertype)

    manager = MonitorManager(monitors=monitors, criteria=args.criteria,
                             sampler_type=args.samplertype, min_chain_size=args.chainsizelimit)

    try:
        manager.build_monitoring_tree()
    except:
        print("Monitoring tree not consistent")
        raise

    print("{0} active monitors, {1} active samplers found".format(len(manager.monitors),
                                                                  len(samplers)))

    #print("\n".join(map(str, manager.monitors)))

    PlotEventHandler(manager)

if __name__ == "__main__":
    main()
