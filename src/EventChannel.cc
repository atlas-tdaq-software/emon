////////////////////////////////////////////////////////////////////////
//      EventChannel.cc
//
//	EventChannel implementation
//
//      Serguei Kolos, 2020-04-13
//     
////////////////////////////////////////////////////////////////////////
#include <memory>

#include <ers/ers.h>

#include <emon/EventChannel.h>
#include <emon/EventSampler_impl.h>
#include <emon/Util.h>
#include <emon/compress.h>

using namespace emon;

/*!
 * This will create a new event channel using the push model API. Users may use
 * this event channel object in order to push events to Monitoring Tasks. The EventChannel
 * will be automatically created by the parent EventSampler
 * \param criteria the selection criteria of this event channel
 * \param monitor a CORBA object reference to the root Monitoring Task
 * \param parent CORBA object reference to the parent EventSampler, which has created the EventChannel
 */
EventChannel::EventChannel(const EventMonitoring::SelectionCriteria &criteria,
        const std::string group_name, EventMonitoring::EventMonitor_ptr monitor) :
        m_criteria(criteria),
        m_group_name(group_name),
        m_split_events(!group_name.empty()),
        m_events_sampled(0),
        m_events_sent(0),
        m_events_rejected(0),
        m_active_monitors(0),
        m_terminated(false),
        m_push_sampling(0),
        m_pull_sampling(0)
{
    ERS_LOG("Created push-style Event Channel");
    connectMonitor(monitor);
}

/*!
 * This will create a new EventChannel using the pull model API. This will automatically start
 * a new pull sampling thread, using the users custom PullSampling object in order to sample events from
 *  The EventChannel will be automatically created by the parent EventSampler
 * \param criteria the selection criteria of this EventChannel
 * \param monitor a CORBA object reference to the root Monitoring Task
 * \param parent CORBA object reference to the parent EventSampler, which has created the EventChannel
 * \param sampler the users PullSampling object that will be used by the pull sampling thread in order sample events from
 */
EventChannel::EventChannel(const EventMonitoring::SelectionCriteria &criteria,
        const std::string group_name, EventMonitoring::EventMonitor_ptr monitor,
        PullSampling *sampling) :
        m_criteria(criteria),
        m_group_name(group_name),
        m_split_events(!group_name.empty()),
        m_events_sampled(0),
        m_events_sent(0),
        m_events_rejected(0),
        m_active_monitors(0),
        m_terminated(false),
        m_push_sampling(0),
        m_pull_sampling(sampling),
        m_pull_thread(std::bind(&EventChannel::run, this))
{
    ERS_LOG("Created pull-style Event Channel");
    connectMonitor(monitor);
}

EventChannel::~EventChannel() {
    ERS_DEBUG(1, "Destroying Event Channel");
    m_terminated = true;
    delete m_push_sampling;

    m_condition.notify_one();
    if (m_pull_sampling) {
        m_pull_thread.join();
        delete m_pull_sampling;
    }
    ERS_LOG("Event Channel '" << construct_name(m_criteria,m_group_name) << "'has been destroyed");
}

void EventChannel::getMonitors(std::vector<EventMonitoring::EventMonitor_var> &monitors) {
    std::unique_lock lock(m_mutex);
    monitors.reserve(m_monitors.size());
    for (auto it = m_monitors.begin(); it != m_monitors.end(); ++it) {
        monitors.push_back(it->second->monitor());
    }
}

void EventChannel::connectMonitor(EventMonitoring::EventMonitor_ptr monitor) {
    ERS_LOG("New monitor addition requested");

    std::unique_lock lock(m_mutex);
    auto it = std::find_if(m_monitors.begin(), m_monitors.end(), [monitor](auto &pair) -> bool {
        return pair.second->match(monitor);
    });
    if (it != m_monitors.end()) {
        throw EventMonitoring::AlreadyConnected();
    }
    m_monitors.insert(std::make_pair(Clock::now(), std::make_shared<Monitor>(monitor)));
    m_active_monitors = m_monitors.size();
    ERS_LOG("New monitor has been added, total number of monitors " << m_active_monitors);
    m_condition.notify_one();
}

void EventChannel::disconnectMonitor(EventMonitoring::EventMonitor_ptr monitor) {
    ERS_LOG("Monitor removal requested");

    std::unique_lock lock(m_mutex);
    auto it = std::find_if(m_monitors.begin(), m_monitors.end(), [monitor](auto &pair) -> bool {
        return pair.second->match(monitor);
    });
    if (it != m_monitors.end()) {
        m_monitors.erase(it);
        m_active_monitors = m_monitors.size();
        ERS_LOG("Monitor has been removed, total number of monitors " << m_active_monitors);
        m_condition.notify_one();
        return;
    }
    ERS_LOG("Monitor has not been found, total number of monitors " << m_active_monitors);
}

/*!
 * If this function returns false, a subsequent call to pushEvent may drop the given event
 * otherwise it's guaranteed that event will be sent to one of the connected monitors
 */
bool EventChannel::readyToSendEvent() {
    std::unique_lock lock(m_mutex);
    MonitorMap::iterator it = m_monitors.begin();
    if (it == m_monitors.end()) {
        ERS_DEBUG(4, "No monitors connected");
        return false;
    }

    return (it->first < Clock::now());
}

EventChannel::TimePoint EventChannel::Monitor::sendEvent(const EventMonitoring::Event &event) {
    ERS_DEBUG(3, "Sending event to "
            << IPCCore::objectToString(m_monitor, IPCCore::Corbaloc) << " monitor");

    EventMonitoring::Event compressed_event;

    CORBA::ULong delay;
    if (compress(event, compressed_event)) {
        delay = m_monitor->push_event(compressed_event);
    } else {
        delay = m_monitor->push_event(event);
    }

    ERS_DEBUG(3, "Event has been sent to "
            << IPCCore::objectToString(m_monitor, IPCCore::Corbaloc)
    << " monitor, next event is expected after " << delay << "mks");

    return Clock::now() + std::chrono::microseconds(delay);
}

void EventChannel::pushToOne(const EventMonitoring::Event &event) {
    ERS_ASSERT_MSG(0, "This function must not be used");
}

void EventChannel::pushToAll(const EventMonitoring::Event &event) {
    ERS_ASSERT_MSG(0, "This function must not be used");
}

void EventChannel::pushEvent(const EventMonitoring::Event &event) {
    ERS_DEBUG(3, "Pushing event to the '"
            << construct_name(m_criteria, m_group_name) << "' channel");

    std::unique_lock lock(m_mutex);
    MonitorMap local_copy(m_monitors);

    for (auto it = local_copy.begin(); it != local_copy.end(); ++it) {

        if (it->first > Clock::now()) {
            break;
        }

        lock.unlock();

        std::shared_ptr<Monitor> monitor = it->second;
        bool success = false;
        TimePoint new_limit;
        try {
            new_limit = monitor->sendEvent(event);
            success = true;
        }
        catch (CORBA::SystemException &ex) {
            ERS_LOG("Got CORBA exception " << ex._name()
                    << " when sending event, monitor will be removed");
        }

        lock.lock();
        auto mit = std::find_if(m_monitors.begin(), m_monitors.end(),
                [&monitor](auto &pair) -> bool {
                    return pair.second->match(monitor->monitor());
                });
        if (mit != m_monitors.end()) {
            m_monitors.erase(mit);
            if (success) {
                m_monitors.insert(std::make_pair(new_limit, monitor));
                if (m_split_events) {
                    break;
                }
            }
        }
        if (m_monitors.empty()) {
            break;
        }
    }

    m_active_monitors = m_monitors.size();

    if (m_monitors.empty()) {
        m_events_rejected++;
        ERS_DEBUG(1, "No monitors exist for the '"
                << construct_name(m_criteria, m_group_name) << "' channel");
    } else {
        m_events_sent++;
    }
}

/*! This method pushes an event to the connected monitors, guaranteeing that event memory
 * may be freed as soon as this function returns. This function does not guarantee
 * event delivery. It may drop event if the monitoring task is not ready to process it,
 * which may happen if the monitoring task can not sustain the given sampling rate.
 * This method shall be used for segmented memory events.
 * \param fragments an iovec with event fragments to push to the monitors
 * \param count the number of event fragments in the iovec event
 */
void EventChannel::pushEvent(iovec *fragments, size_t count) {
    // construct an event consisting of several fragments, do not copy here!
    EventMonitoring::Event event(count);
    event.length(count);

    for (size_t i = 0; i < count; i++) {
        unsigned int length = fragments[i].iov_len / sizeof(unsigned int);
        event[i].replace(length, length, (CORBA::ULong*) fragments[i].iov_base, false);
    }

    pushEvent(event);
    m_events_sampled++;
}

/*! This method pushes an event to the connected Monitoring Tree, guaranteeing that event memory
 * may be freed as soon as this function returns. This function does not guarantee event delivery.
 * It may drop event if the monitoring task is not ready to process it, which may happen if the
 * monitor can not sustain the given sampling rate. This method shall be used for contiguous
 * memory events.
 * \param event the event to push to the Monitoring Tasks
 * \param event_size the size of the event to push
 */
void EventChannel::pushEvent(unsigned int *data, size_t size) {
    // construct event consisting of one fragment, do not copy here!
    EventMonitoring::Event event(1);
    event.length(1);
    event[0].replace(size, size, (CORBA::ULong*) data, false);

    pushEvent(event);
    m_events_sampled++;
}

void EventChannel::run() {
    while (!m_terminated) {
        wait();

        ERS_DEBUG(4, "Asking to sample event");
        m_pull_sampling->sampleEvent(*this);
        ERS_DEBUG(4, "Finish sampling event");
    }
}

void EventChannel::wait() {
    std::unique_lock lock(m_mutex);

    while (!m_monitors.empty() && !m_terminated) {
        TimePoint first = m_monitors.begin()->first;
        TimePoint now = Clock::now();
        if (first < now) {
            return;
        }
        TimePoint timeout = now + std::chrono::milliseconds(10);
        m_condition.wait_until(lock, timeout < first ? timeout : first);
    }

    m_condition.wait(lock, [this]{ return !m_monitors.empty() || m_terminated; });
}
