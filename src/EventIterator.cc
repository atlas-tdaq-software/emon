#include <emon/EventIterator.h>

/*!
 * This constructor creates an iterator but leaves it the unconnected state.
 * A user must call one of the connect() functions to attach this iterator to
 * and event sampler and start getting events.
 * \param partition TDAQ partition
 * \param address the sampling address of the sampling application to connect to
 * \param criteria the sampling criteria the sampled events shall satisfy
 * \param buffer_size size of the event buffer
 * \param group_name the group name shared by monitors that want to receive different
 *               events from the same criteria. If it's a non-empty string, events
 *               will be sent by samplers directly to each monitor, all receiving
 *               different events. Otherwise events will also be moved between
 *               monitors; this iterator and all the other ones which are attached
 *		to it will see the same events.
 * \exception emon::Exception
 */
emon::EventIterator::EventIterator(
        const IPCPartition &partition, const SamplingAddress &address,
        const SelectionCriteria &criteria, size_t buffer_size, const std::string group_name) :
        m_monitor(new EventMonitor_impl(partition, address, criteria, group_name, buffer_size))
{
    m_monitor->connect();
}

/*!
 * This constructor creates an iterator but leaves it the unconnected state.
 * A user must call one of the connect() functions to attach this iterator to
 * and event sampler and start getting events.
 * \param partition TDAQ partition
 * \param params DB object which contains the sampling parameters.
 * \exception emon::Exception
 */
emon::EventIterator::EventIterator(const IPCPartition &partition,
        const emon::dal::SamplingParameters &params)
try : m_monitor(new EventMonitor_impl(
        partition, SamplingAddress(params), SelectionCriteria(params),
        params.get_GroupName(), params.get_BufferSize()))
{
    m_monitor->connect();
}
catch (daq::config::Generic &ex) {
    throw emon::Exception( ERS_HERE, ex);
}
