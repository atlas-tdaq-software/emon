////////////////////////////////////////////
//    EventMonitor_impl.cc
//    Implementation of the Emon Monitor
//    Ingo Scholtes and Serguei Kolos
//    Last modified: Nov. 29 2006
////////////////////////////////////////////

#include <stdlib.h>

#include <algorithm>
#include <chrono>
#include <limits>

#include <emon/EventMonitor_impl.h>
#include <emon/Util.h>
#include <emon/compress.h>

namespace {
    int read_from_environment( const char * name, int default_value )
    {
        int value = default_value;
        const char * env = ::getenv( name );
        if ( env )
        {
            if ( sscanf( env, "%d", &value ) != 1 )
            {
                ERS_LOG( "Wrong value \"" << env
                        << "\" is given for the \"" << name << "\" environment" );
            }
        }
        return value;
    }
}

using namespace emon;
using namespace std::chrono;
using namespace std::chrono_literals;

/*! This creates an EventMonitor_impl object.
 * \param partition the IPCPartition object
 * \param address the sampling address of the EventSampler we want to connect to
 * \param criteria the selection criteria the sampled events should satisfy
 * \param group_name the group name shared by monitors that want to receive different events
 * \param buffer_size the maximum amount of events to buffer in the event buffer
 */
EventMonitor_impl::EventMonitor_impl(   const IPCPartition & partition,
                                        const SamplingAddress & address,
                                        const SelectionCriteria & criteria,
                                        const std::string group_name,
                                        size_t buffer_size)
  : MonitorInfoNamed( partition, construct_info_name( "Monitor" ) ),
    InfoPublisher( *this, 10 ),
    m_partition( partition ),
    m_criteria( criteria ),
    m_address( address ),
    m_terminated( false ),
    m_adapter(m_sampler_names)
{    
    m_main_buffer.set_capacity(buffer_size);

    m_buffer_size = buffer_size;
    m_sampler_type = (const char*) address.type;

    m_group_name = group_name;
}

void EventMonitor_impl::connect() {
    m_selection_criteria = construct_name(m_criteria);
    std::string name = construct_name(m_criteria, m_group_name);
    ERS_LOG("Connecting monitor to the '" << name << "' event channel");

    try {
        EventMonitoring::Conductor_var conductor = m_partition.lookup<EventMonitoring::Conductor>(
                EventMonitoring::Conductor::Name);
        conductor->add_monitor(m_address, m_criteria(), m_group_name.c_str(), _this());

        for (size_t i = 0; i < m_address.names.length(); ++i) {
            m_sampler_names.push_back((const char*) m_address.names[i]);
        }

        ERS_LOG("Monitor is connected to '" << name << "' event channel");
    } catch (const daq::ipc::Exception &ex) {
        throw Exception(ERS_HERE, ex);
    } catch (const EventMonitoring::BadAddress &ex) {
        throw BadAddress(ERS_HERE, construct_name(m_address));
    } catch (const EventMonitoring::BadCriteria &ex) {
        throw BadCriteria(ERS_HERE, construct_name(m_address), construct_name(m_criteria));
    } catch (const EventMonitoring::NoResources &ex) {
        throw NoResources(ERS_HERE, construct_name(m_address), ex.limit);
    } catch (CORBA::SystemException &ex) {
        throw Exception(ERS_HERE, daq::ipc::CorbaSystemException(ERS_HERE, &ex));
    }
}

void EventMonitor_impl::destroy() {
    ERS_LOG("Shutting down monitor");

    m_terminated = true;
    try {
        EventMonitoring::Conductor_var conductor = m_partition.lookup<EventMonitoring::Conductor>(
                EventMonitoring::Conductor::Name);
        conductor->remove_monitor(m_address, m_criteria(), m_group_name.c_str(), _this());
        ERS_DEBUG(1, "Successfully removed subscription from Conductor.");
    } catch (const daq::ipc::Exception &ex) {
        ERS_LOG("Can't remove subscription in Conductor: " << ex);
    } catch (const EventMonitoring::NotFound &ex) {
        ERS_LOG("Can't remove subscription in Conductor - monitor is not known");
    } catch (const CORBA::SystemException &ex) {
        ERS_LOG("Can't remove subscription in Conductor: " << ex._name());
    }

    _destroy(true);
}

EventMonitor_impl::~EventMonitor_impl() {
    ERS_LOG("Monitor has been destroyed");
}

/*!
 * This will take care of the child Monitoring Tasks,
 * when all samplers this Monitoring Task is connected to have exited.
 * It will invoke sampler_exit of every child Monitoring 
 * Task and it will cause a SamplerStopped exception in the
 * next invocation of nextEvent to be thrown.
 * This is an IPC published method.
 * \sa SamplerStopped
 */
void EventMonitor_impl::sampler_exit(const char * m) {
    ERS_LOG("Got '" << m << "' sampler notification");

    std::string message(m);
    if (message.size() < 3 or message[1] != ':') {
        return;
    }
    std::string cmd(m, 1);
    std::string name(m + 2);
    std::unique_lock lock(m_evt_mutex);
    auto it = std::find(m_sampler_names.begin(), m_sampler_names.end(), std::string(name));
    if ("D" == cmd) {
        if (it != m_sampler_names.end()) {
            m_sampler_names.erase(it);
            ERS_LOG("Sampler gone acknowledged, total number of samplers connected is "
                    << m_sampler_names.size());
            m_evt_condition.notify_one();
        }
    } else if ("R" == cmd) {
        if (it == m_sampler_names.end()) {
            m_sampler_names.push_back(name);
            ERS_LOG("Sampler resurrection acknowledged, total number of samplers connected is "
                    << m_sampler_names.size());
            m_evt_condition.notify_one();
        }
    }
}

void EventMonitor_impl::ping() {
    ERS_DEBUG(3, "Received ping");
}

/*!
 * It will Create a new event consisting of a single EventFragment
 * and returns a smart pointer to it.
 */
smart_event_ptr EventMonitor_impl::serialize(const EventMonitoring::Event &in) const {
    smart_event_ptr out(new EventMonitoring::Event(1));
    out->length(in.length());

    for (CORBA::ULong i = 0; i < in.length(); ++i) {
        CORBA::ULong length = in[i].length();
        (*out)[i].replace(length, length,
                const_cast<EventMonitoring::EventFragment&>(in[i]).get_buffer(true), true);
    }
    return out;
}

/*!
 * This method is called remotely by the connected event sampler.
 * Events will be stored in a local buffer, where they are waiting for being
 * processed (by the user) and for being forwarded to children Monitoring Tasks
 * This is an IPC published method.
 * \param event event to push to the buffer
 * \sa Event
 */
CORBA::ULong EventMonitor_impl::push_event(const EventMonitoring::Event &event) {
    ERS_DEBUG(3, "enter push_event");

    smart_event_ptr event_ptr = serialize(event);
    size_t period = 0;

    {
        std::unique_lock lock(m_evt_mutex);
        ++m_received_events;

        while (!m_main_buffer.try_push(event_ptr)) {
            smart_event_ptr e;
            m_main_buffer.pop(e);
            ++m_dropped_events;
            --m_buffered_events;
            ERS_DEBUG(3, "Buffer is full, event will be dropped. Total number of dropped events is "
                    << m_dropped_events);
        }
        ++m_buffered_events;

        period = m_adapter.eventReceived();
    }
    m_evt_condition.notify_one();

    ERS_DEBUG(3, "Pushed event to the main buffer, total number of buffered events "
            << m_buffered_events << ", next event is anticipated in " << period << " microseconds");

    return period;
}

/*! This method will return the first event available in the event buffer.
 * Note: The behaviour of this function depends on the value of async flag.
 * If it is true, a call to nextEvent will throw NoMoreEvents when there
 * is no event available. In case of async set to false, the function will block
 * until an event is available or the specified timeout is exceeded. Zero timeout
 * will cause this function blocking forever. If the EventSampler
 * crashes/stops in the middle of event sampling, this method will throw
 * SamplerStopped
 * \return a smart pointer to an event
 * \exception SamplerStopped
 * \exception NoMoreEvents
 */
smart_event_ptr EventMonitor_impl::nextEvent(bool async, size_t timeout) {

    smart_event_ptr event;

    {
        std::unique_lock lock(m_evt_mutex);

        m_adapter.eventConsumed(false);

        while (m_main_buffer.empty() and not m_terminated) {
            if (m_sampler_names.empty()) {
                throw SamplerStopped(ERS_HERE, construct_name(m_address));
            }
            if (async) {
                throw NoMoreEvents(ERS_HERE);
            }

            if (m_evt_condition.wait_for(lock, timeout * 1ms) == std::cv_status::timeout) {
                throw NoMoreEvents(ERS_HERE);
            }
        }

        m_adapter.eventConsumed(true);

        m_main_buffer.pop(event);
        m_buffer_occupancy = m_main_buffer.size();
    }

    smart_event_ptr uncompressed_event(new EventMonitoring::Event());
    if (uncompress(*event, *uncompressed_event)) {
        return uncompressed_event;
    } else {
        return event;
    }
}

void EventMonitor_impl::RateAdapter::eventConsumed(bool begin) {
    TimePoint now = Clock::now();
    if (begin) {
        m_start = now;
        return;
    }

    if (m_start != TimePoint::min()) {
        m_period = duration_cast<microseconds>(now - m_start).count();
        m_start = TimePoint::min();
    }
}

size_t EventMonitor_impl::RateAdapter::eventReceived() {
    static size_t max_delay = read_from_environment(
            "TDAQ_EMON_MAX_SAMPLING_DELAY", 20000000);

    size_t delay = m_period;
    if (m_start != TimePoint::min() and delay == 0) {
        TimePoint now = Clock::now();
        delay = duration_cast<microseconds>(now - m_start).count();
    }
    delay *= m_samplers.size();
    if (delay > max_delay) {
        delay = max_delay;
    }

    return delay;
}
