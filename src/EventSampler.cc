#include <emon/EventSampler.h>

/*
 * This method will create a new EventSampler application following the push model
 * and will associate a custom PushSamplingFactory with the newly created EventSampler
 * \param partition the partition to work in
 * \param address the Sampling address this Event Sampler should have
 * \param factory The push sampling factory to be used
 * \param max_channels the maximum number of EventChannels allowed (default 100)
 * \sa PushSamplingFactory PushSampling
 * \exception emon::Exception
 */
emon::EventSampler::EventSampler(   const IPCPartition & partition,
				    const SamplingAddress & address,
				    PushSamplingFactory * factory,
				    size_t max_channels ) 
  : m_sampler( new EventSampler_impl( partition, address, factory, max_channels ) )
{
    m_sampler -> connect();
}


/*
 * This method will create a new EventSampler application following the pull model and will associate a
 * custom PullSamplingFactory with the newly created EventSampler
 * \param partition the partition to work in
 * \param address the Sampling address this EventSampler should have
 * \param factory The factory to be used
 * \param max_channels the maximum number of channels allowed (default 100)
 * \sa PullSamplingFactory PullSampling
 * \exception emon::Exception
 */
emon::EventSampler::EventSampler(   const IPCPartition & partition,
				    const SamplingAddress & address,
				    PullSamplingFactory * factory,
				    size_t max_channels ) 
  : m_sampler( new EventSampler_impl( partition, address, factory, max_channels ) )
{
    m_sampler -> connect();
}
