////////////////////////////////////////////////////////////////////////
//	    EventSampler_impl.cc
//
//	    EventSampler implementation
//
//	    Ingo Scholtes, 2004-09-01
//
//	    description:
//		    
////////////////////////////////////////////////////////////////////////
#include <ipc/signal.h>

#include <emon/EventSampler_impl.h>
#include <emon/Util.h>

using namespace emon;

/*!  Creates push model sampler implementation.
 * \param partition the IPCPartition to start this EventSampler in
 * \param address the sampling address of this EventSampler
 * \param factory a reference to the users pull sampling factory object which will be used to pull events from EventChannel
 * \param max_criteria the maximum amount of direct Monitoring Task connections this EventSampler will accept
 * \sa PullSampling
 * \sa PullSamplingFactory
 */
EventSampler_impl::EventSampler_impl(const IPCPartition &partition,
        const SamplingAddress &address, PushSamplingFactory *factory, size_t max_channels) :
        IPCNamedObject<POA_EventMonitoring::EventSampler>(partition, construct_name(address)),
        SamplerInfoNamed(partition, construct_info_name("Sampler")),
        InfoPublisher(*this, 10),
        m_events_sampled_history(0),
        m_events_sent_history(0),
        m_events_rejected_history(0),
        m_address(address),
        m_push_factory(factory)
{
    ERS_ASSERT(factory);
    ERS_ASSERT(address.names.length() == 1);
    m_maximum_channels = max_channels;
    m_type = address.type;
    SamplerInfoNamed::m_name = address.names[0];
}

/*! Creates pull model sampler implementation.
 * \param partition the IPCPartition to start this EventSampler in
 * \param address the sampling address of this EventSampler
 * \param factory a reference to the users push sampling factory which will be used to create push sampling threads upon request
 * \param max_criteria the maximum amount of direct Monitoring Task connections this EventSampler will accept
 * \sa PushSampling
 * \sa PushSamplingFactory
 */
EventSampler_impl::EventSampler_impl(const IPCPartition &partition,
        const SamplingAddress &address, PullSamplingFactory *factory, size_t max_channels) :
        IPCNamedObject<POA_EventMonitoring::EventSampler>(partition, construct_name(address)),
        SamplerInfoNamed(partition, construct_info_name("Sampler")),
        InfoPublisher(*this, 10),
        m_events_sampled_history(0),
        m_events_sent_history(0),
        m_events_rejected_history(0),
        m_address(address),
        m_pull_factory(factory)
{
    ERS_ASSERT(factory);
    ERS_ASSERT(address.names.length() == 1);
    m_maximum_channels = max_channels;
    m_type = address.type;
    SamplerInfoNamed::m_name = address.names[0];
}

EventSampler_impl::~EventSampler_impl() {
    std::unique_lock lock(m_mutex);

    SamplingChannels::iterator it = m_channels.begin();
    while (it != m_channels.end()) {
        ERS_DEBUG(0, "Destroying '" << it->first << "' Event Channel");
        it = m_channels.erase(it);
    }
}

void EventSampler_impl::updateStatistics() {
    std::unique_lock lock(m_mutex);
    m_events_sampled = m_events_sent = m_events_rejected = 0;
    m_connected_monitors = 0;
    for (SamplingChannels::iterator it = m_channels.begin(); it != m_channels.end();) {
        if (it->second->idle()) {
            m_events_sampled_history += it->second->eventsSampled();
            m_events_sent_history += it->second->eventsSent();
            m_events_rejected_history += it->second->eventsRejected();
            ERS_LOG("Event Channel '" << it->first
                    << "' is destroyed, total number of channels is " << (m_channels.size() - 1));
            it = m_channels.erase(it);
        } else {
            m_connected_monitors += it->second->monitorsConnected();
            m_events_sampled += it->second->eventsSampled();
            m_events_sent += it->second->eventsSent();
            m_events_rejected += it->second->eventsRejected();
            ++it;
        }
    }
    m_events_sampled += m_events_sampled_history;
    m_events_sent += m_events_sent_history;
    m_events_rejected += m_events_rejected_history;
    m_active_channels = m_channels.size();
}

/*!
 * This method initialises the sampling process, by connecting the EventSampler
 * to the Conductor and publishing this sample in the given partition
 */
void EventSampler_impl::connect() {
    try {
        EventMonitoring::Conductor_var conductor;
        conductor = partition().lookup<EventMonitoring::Conductor>(
                EventMonitoring::Conductor::Name);
        publish();
        conductor->add_sampler(m_address, _this());
    } catch (daq::ipc::Exception &ex) {
        throw Exception( ERS_HERE, ex);
    } catch (EventMonitoring::SamplerAlreadyExists &ex) {
        std::string n = construct_name(m_address);
        m_address = SamplingAddress(EventMonitoring::SamplingAddress());
        throw SamplerAlreadyExists( ERS_HERE, n );
    } catch (CORBA::SystemException &ex) {
        throw Exception( ERS_HERE, daq::ipc::CorbaSystemException( ERS_HERE, &ex));
    }
}

/*! This will stop all sampling threads, unregister the EventSampler in the Conductor
 *  and destroy the IPCObject. This is the only valid way of destroying objects of this class.
 *  It is guaranteed that all sampling threads have been terminated when this method returns.
 */
void EventSampler_impl::destroy() {
    if (m_address.names.length() == 1) {
        try {
            EventMonitoring::Conductor_var conductor;
            conductor = partition().lookup<EventMonitoring::Conductor>(
                    EventMonitoring::Conductor::Name);
            withdraw();
            conductor->remove_sampler(m_address);
        } catch (daq::ipc::Exception &ex) {
            ers::warning(ex);
        } catch (CORBA::SystemException &ex) {
            ers::warning(daq::ipc::CorbaSystemException( ERS_HERE, &ex));
        } catch (EventMonitoring::NotFound &ex) {
            ERS_LOG("Sampler is not registered");
        }
    }

    _destroy(true);
}

void EventSampler_impl::ping() {
    ERS_DEBUG(4, "Ping Request from Conductor received.");
}

/*! This method adds another Monitoring Task to the subscribers list and updates
 * information in IS server. Beside that it will start a new sampling thread, depending
 * on the model of the associated sampling object.
 * If pull model is active, then this method will start the internal pullSamplingThread 
 * of class EventChannel on the PullSampling object created by the factory.
 * If push model is active, it will create a PushSampling object. It is up to the users
 * responsibility to ensure, that the creation of this object itself will start a sampling
 * thread.
 * This is an IPC published method.
 * \param criteria the selection criteria the sampled events should satisfy
 * \param monitor a CORBA reference to the Monitoring Task to ship events to (the root Monitoring Task)
 * \exception AlreadyConnected
 * \exception BadCriteria
 * \exception NoResources
 */
void EventSampler_impl::connect_monitor(const EventMonitoring::SelectionCriteria &criteria,
        const char *group_name, EventMonitoring::EventMonitor_ptr monitor) {
    std::string name = construct_name(criteria, std::string(group_name));
    ERS_LOG("Connect monitor '" << name << "' request received");

    std::unique_lock lock(m_mutex);

    SamplingChannels::iterator it = m_channels.find(name);
    if (it == m_channels.end()) {
        if (m_maximum_channels <= m_channels.size()) {
            ERS_LOG("number of maximum channels ("<< m_maximum_channels << ") is reached");
            throw EventMonitoring::NoResources(m_maximum_channels);
        }

        EventChannel *channel;
        try {
            if (m_pull_factory.get()) {
                // create the PullSampling object
                PullSampling *pull = m_pull_factory->startSampling(criteria);

                // and create a new event channel, which will start the internal pull sampling thread
                channel = new EventChannel(criteria, std::string(group_name), monitor, pull);
            } else {
                //create the sampling channel
                channel = new EventChannel(criteria, std::string(group_name), monitor);

                // create the PushSampling object, which will create the push sampling thread
                PushSampling *push = m_push_factory->startSampling(criteria, channel);

                // set the activity...
                channel->setActivity(push);
            }
        } catch (BadCriteria &ex) {
            ers::log(ex);
            throw EventMonitoring::BadCriteria();
        } catch (NoResources &ex) {
            ers::log(ex);
            throw EventMonitoring::NoResources();
        }

        m_channels[name] = std::shared_ptr<EventChannel>(channel);
        m_active_channels++;
        ERS_LOG("Event Channel '" << name
                << "' has been successfully created, total number of channels is " << m_active_channels);
    } else {
        ERS_LOG("The Event Channel \"" << name << "\" already exists.");
        it->second->connectMonitor(monitor);
    }
}

/*! This methods deletes one of the root Monitoring Tasks from the subscription list. It will then
 * stop the associated sampling process. This is an IPC published method.
 * \param criteria the selection criteria the sampled events should satisfy
 * \param group_name 
 * \param monitor a CORBA reference to the Monitoring Task to ship events to (the root Monitoring Task)
 */
void EventSampler_impl::disconnect_monitor(const EventMonitoring::SelectionCriteria &criteria,
        const char *group_name, EventMonitoring::EventMonitor_ptr monitor) {

    ERS_LOG("monitor removal is requested.");

    std::string name = construct_name(criteria, std::string(group_name));
    std::unique_lock lock(m_mutex);
    SamplingChannels::iterator it = m_channels.find(name);
    if (it != m_channels.end()) {
        it->second->disconnectMonitor(monitor);
    } else {
        ERS_LOG("Event Channel '" << name << "' does not exist.");
        throw EventMonitoring::NotFound();
    }
}

/*! Returns event channel information about this EventSampler, requested by a Conductor that has crashed and restarted.
 *  When starting up the Conductor, it will find all EventSamplers published in the partition and will ask them about
 *  selection criteria, root Monitoring Tasks and sampling address.
 * \param monitors a CORBA sequence of MonitorInfo objects, containing information about all monitors connected to this sampler
 */
void EventSampler_impl::get_monitors(EventMonitoring::MonitorList_out monitors) {
    ERS_DEBUG(0, "Event Channel recovery information has been requested by Conductor.");
    monitors = new EventMonitoring::MonitorList;

    std::unique_lock lock(m_mutex);
    CORBA::ULong length = 0;
    for (auto it = m_channels.begin(); it != m_channels.end(); ++it) {
        length += it->second->monitorsConnected();
    }

    monitors->length(length);
    CORBA::ULong index = 0;
    for (auto it = m_channels.begin(); it != m_channels.end(); ++it) {
        std::vector<EventMonitoring::EventMonitor_var> mm;
        it->second->getMonitors(mm);
        for (size_t i = 0; i < mm.size(); ++i) {
            EventMonitoring::MonitorInfo minfo;
            minfo.criteria = it->second->criteria();
            minfo.group_name = it->second->group_name().c_str();
            minfo.reference = mm[i];
            monitors[index++] = minfo;
        }
    }
}

void EventSampler_impl::get_address(EventMonitoring::SamplingAddress_out address) {
    ERS_DEBUG(0, "Sampling address has been requested by Conductor.");
    address = new EventMonitoring::SamplingAddress(m_address);
}

void EventSampler_impl::shutdown() {
    daq::ipc::signal::raise();
}
