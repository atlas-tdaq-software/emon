#include <emon/InfoPublisher.h>

emon::InfoPublisher::InfoPublisher(ProcessIdentityNamed &info, size_t period) :
        m_info(info),
        m_alarm(period, std::bind(&InfoPublisher::executePublication, this))
{
    char buffer[256];
    gethostname(buffer, sizeof(buffer));

    m_info.m_host = buffer;
    m_info.m_pid = getpid();
}

emon::InfoPublisher::~InfoPublisher() {
    m_alarm.suspend();
    executePublication();
}

bool emon::InfoPublisher::executePublication() {
    updateStatistics();
    try {
        m_info.checkin();
        ERS_DEBUG(2, "IS information published.");
    } catch (daq::is::Exception &ex) {
        ERS_DEBUG(2, "Can't publish '" << m_info.name() << "' information object: " << ex);
    }
    return true;
}
