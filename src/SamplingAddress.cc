////////////////////////////////////////////////////////////////////////
// SamplingAddress.cc
//
// Ingo Scholtes, 2005-10-12
//		
////////////////////////////////////////////////////////////////////////

#include <emon/SamplingAddress.h>

/*!
 * Constructor
 * Creates a copy of sampling address sa
 * \param sa the sampling address to copy
 */
emon::SamplingAddress::SamplingAddress( const EventMonitoring::SamplingAddress & address )
  : EventMonitoring::SamplingAddress( address )
{ ; }

/*!
 * Constructor
 * \param key sampler type
 * \param value sampler name
 */
emon::SamplingAddress::SamplingAddress( const std::string & key, const std::string & value )
{
    type = key.c_str();
    names.length( 1 );
    names[0] = value.c_str();
    number = 1;
}

/*!
 * Constructor
 * \param key sampler type
 * \param values sampler names
 */
emon::SamplingAddress::SamplingAddress( const std::string & key, const std::vector<std::string> & values )
{
    type = key.c_str();
    names.length( values.size() );
    for ( size_t i = 0; i < values.size(); ++i )
    {
    	names[i] = values[i].c_str();
    }
    number = 1;
}

/*!
 * \param key sampler type
 * \param number number of samplers to be connected
 */
emon::SamplingAddress::SamplingAddress( const std::string & key, unsigned short num )
{
    type = key.c_str();
    number = num;
}

/*!
 * Constructor
 * Creates sampling address from database
 * \param address the database object describing selection parameters
 */
emon::SamplingAddress::SamplingAddress( const emon::dal::SamplingAddress & address ) 
{
    type = address.get_SamplerType().c_str();

    const std::vector<std::string> & values = address.get_SamplerNames();
    names.length( values.size() );
    for ( size_t i = 0; i < values.size(); ++i )
    {
    	names[i] = values[i].c_str();
    }
    
    number = address.get_NumberOfSamplers();
}
