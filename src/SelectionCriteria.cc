////////////////////////////////////////////////////////////////////////
// SelectionCriteria.cc
//
// Ingo Scholtes, 2005-10-12
//		
////////////////////////////////////////////////////////////////////////

#include <emon/SelectionCriteria.h>

/*!
 * Creates a copy of the value of type generated from the emon IDL file
 * \param value smart value to copy
 */
emon::SmartStreamValue::SmartStreamValue( const EventMonitoring::SmartStreamValue & value )
  : m_type( (const char*)value.type ),
    m_logic ( value.logic )
{
    for ( size_t i = 0; i < value.names.length(); ++i )
    {
    	m_names.push_back( (const char*)value.names[i] );
    }
}

/*!
 * Creates a copy of the value of type generated from the emon IDL file
 * \param value smart value to copy
 */
emon::SmartBitValue::SmartBitValue( const EventMonitoring::SmartBitValue & value )
  : m_origin( value.origin ),
    m_logic ( value.logic )
{
    for ( size_t i = 0; i < value.bit_positions.length(); ++i )
    {
    	m_bit_positions.push_back( value.bit_positions[i] );
    }
}

emon::SelectionCriteria::SelectionCriteria( const emon::dal::SelectionCriteria & criteria ) 
{
    m_lvl1_trigger_type.m_value = criteria.get_L1TriggerType();
    m_lvl1_trigger_type.m_ignore = criteria.get_L1TriggerType_Ignore();
    
    m_status_word.m_value = criteria.get_StatusWord();
    m_status_word.m_ignore = criteria.get_StatusWord_Ignore();
    
    std::string ll = criteria.get_L1Bits_Logic();
    std::string oo = criteria.get_L1Bits_Origin();
    m_lvl1_trigger_info = SmartBitValue( criteria.get_L1Bits(), 
    				ll == "AND" ? logic::AND : ll == "OR" ? logic::OR : logic::IGNORE,
                                oo == "BEFORE_PRESCALE" ? origin::BEFORE_PRESCALE : oo == "AFTER_PRESCALE" 
                                                        ? origin::AFTER_PRESCALE  : origin::AFTER_VETO );
                                                        
    ll = criteria.get_Stream_Logic();
    m_stream_tags = SmartStreamValue( criteria.get_StreamType(), criteria.get_StreamNames(),
    				ll == "AND" ? logic::AND : ll == "OR" ? logic::OR : logic::IGNORE );
}

EventMonitoring::SelectionCriteria
emon::SelectionCriteria::operator()() const
{
    EventMonitoring::SelectionCriteria criteria;
    criteria.lvl1_trigger_type.value = m_lvl1_trigger_type.m_value;
    criteria.lvl1_trigger_type.ignore = m_lvl1_trigger_type.m_ignore;

    criteria.status_word.value = m_status_word.m_value;
    criteria.status_word.ignore = m_status_word.m_ignore;

    criteria.lvl1_trigger_info.logic = m_lvl1_trigger_info.m_logic;
    criteria.lvl1_trigger_info.origin = m_lvl1_trigger_info.m_origin;    
    criteria.lvl1_trigger_info.bit_positions.length( m_lvl1_trigger_info.m_bit_positions.size() );
    for ( size_t i = 0; i < m_lvl1_trigger_info.m_bit_positions.size(); ++i )
    	criteria.lvl1_trigger_info.bit_positions[i] = m_lvl1_trigger_info.m_bit_positions[i];
                                        
    criteria.stream_tags.type = m_stream_tags.m_type.c_str();
    criteria.stream_tags.logic = m_stream_tags.m_logic;
    criteria.stream_tags.names.length( m_stream_tags.m_names.size() );
    for ( size_t i = 0; i < m_stream_tags.m_names.size(); ++i )
    	criteria.stream_tags.names[i] = m_stream_tags.m_names[i].c_str();
    
    return criteria;
}
