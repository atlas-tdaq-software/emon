////////////////////////////////////////////////////////////////////////
//	    Util.cc
//
//	    Common utility functions and operators
//
//	    Ingo Scholtes, 2005-10-12
//		
////////////////////////////////////////////////////////////////////////

#include <emon/Util.h>
#include <sstream>

std::string emon::construct_info_name(const std::string &type) {
    const char *name = getenv("TDAQ_APPLICATION_NAME");

    std::ostringstream out;
    out << EventMonitoring::StatisticsServerName << "./" << type << "/";

    if (name) {
        out << name;
    } else {
        char buffer[256];
        gethostname(buffer, sizeof(buffer));
        out << buffer << "/" << getpid();
    }
    return out.str();
}

std::string emon::construct_name(const emon::SamplingAddress &address) {
    std::ostringstream out;
    out << address.type << ":";

    for (size_t i = 0; i < address.names.length(); ++i) {
        out << (const char*) address.names[i];
        if (i + 1 < address.names.length())
            out << ",";
    }

    return out.str();
}

std::string emon::construct_name(const emon::SamplingAddress &address,
        const emon::SelectionCriteria &criteria) {
    return construct_name(address) + "/" + construct_name(criteria);
}

std::string emon::construct_name(const emon::SelectionCriteria &criteria) {
    std::ostringstream out;
    out << "(" << criteria.m_lvl1_trigger_type << ":" << criteria.m_lvl1_trigger_info << ":"
            << criteria.m_status_word << ":" << criteria.m_stream_tags << ")";
    return out.str();
}

std::string emon::construct_name(const emon::SelectionCriteria &criteria,
        const std::string group_name) {
    std::ostringstream out;
    out << group_name;
    out << "(" << criteria.m_lvl1_trigger_type << ":" << criteria.m_lvl1_trigger_info << ":"
            << criteria.m_status_word << ":" << criteria.m_stream_tags << ")";

    return out.str();
}

std::ostream&
emon::operator<<(std::ostream &out, const emon::Origin &org) {
    switch (org) {
    case origin::BEFORE_PRESCALE:
        out << "BEFORE_PRESCALE";
        break;
    case origin::AFTER_PRESCALE:
        out << "AFTER_PRESCALE";
        break;
    case origin::AFTER_VETO:
        out << "AFTER_VETO";
        break;
    default:
        out << "BAD_ORIGIN_VALUE(" << (int) org << ")";
    }
    return out;
}

std::ostream&
emon::operator<<(std::ostream &out, const emon::Logic &ll) {
    switch (ll) {
    case logic::AND:
        out << "AND";
        break;
    case logic::OR:
        out << "OR";
        break;
    case logic::IGNORE:
        out << "IGNORE";
        break;
    default:
        out << "BAD_LOGIC_VALUE(" << (int) ll << ")";
    }
    return out;
}

std::ostream&
emon::operator<<(std::ostream &out, const emon::SmartStreamValue &value) {
    if (value.m_logic == logic::IGNORE) {
        out << "*";
    } else {
        out << value.m_type << ":";
        for (size_t i = 0; i < value.m_names.size(); ++i) {
            out << value.m_names[i];
            if (i + 1 < value.m_names.size())
                out << "<" << value.m_logic << ">";
        }
    }
    return out;
}

std::ostream&
emon::operator<<(std::ostream &out, const emon::SmartBitValue &value) {
    if (value.m_logic == logic::IGNORE) {
        out << "*";
    } else {
        out << value.m_origin << ":";
        out << value.m_logic << ":";
        for (size_t i = 0; i < value.m_bit_positions.size(); ++i) {
            out << (short) value.m_bit_positions[i];
            if (i + 1 < value.m_bit_positions.size())
                out << "<" << value.m_logic << ">";
        }
    }
    return out;
}

std::ostream&
emon::operator<<(std::ostream &out, const emon::SelectionCriteria &criteria) {
    out << "Selection criteria:" << std::endl;
    out << "  L1 trigger type = " << criteria.m_lvl1_trigger_type << std::endl;
    out << "  L1 trigger bits = " << criteria.m_lvl1_trigger_info << std::endl;
    out << "  status_word = " << criteria.m_status_word << std::endl;
    out << "  stream tags = " << criteria.m_stream_tags << std::endl;
    return out;
}
