#include <zlib.h>
#include <stdlib.h>

#include <ers/ers.h>

#include <emon/compress.h>

namespace {
    CORBA::ULong buffer_size(CORBA::ULong s) {
        return (s / sizeof(CORBA::ULong) + (s % sizeof(CORBA::ULong) ? 1 : 0) + 2);
    }

    CORBA::ULong get_threshold_value() {
        static const CORBA::ULong DefaultThreshold = 10000000;

        char *var = getenv("TDAQ_EMON_COMPRESSION_THRESHOLD"); // default value (in bytes)
        if (var) {
            char *endptr = 0;
            CORBA::ULong value = strtol(var, &endptr, 10);
            if (endptr && !*endptr) {
                ERS_DEBUG(0, "TDAQ_EMON_COMPRESSION_THRESHOLD was set to " << value << " bytes");
                return value;
            } else {
                ERS_DEBUG(0, "Invalid value '" << var
                        << "' has been used for the TDAQ_EMON_COMPRESSION_THRESHOLD variable."
                        << " Default value (" << DefaultThreshold << ") will be used instead");
            }
        }

        return DefaultThreshold;
    }

    const CORBA::ULong TDAQ_EMON_COMPRESSION_THRESHOLD = get_threshold_value();
}

namespace emon {

    void serialize(const EventMonitoring::Event &in, EventMonitoring::Event &out) {
        CORBA::ULong size = 0;
        for (CORBA::ULong i = 0; i < in.length(); ++i) {
            size += in[i].length();
        }

        CORBA::ULong *data = new CORBA::ULong[size];
        CORBA::ULong s = 0;
        for (CORBA::ULong i = 0; i < in.length(); i++) {
            memcpy(data + s, in[i].get_buffer(), in[i].length() * sizeof(CORBA::ULong));
            s += in[i].length();
        }

        out.length(1);
        out[0].replace(size, size, data, true);

        return;
    }

    bool compress(const EventMonitoring::Event &in, EventMonitoring::Event &out) {
        CORBA::ULong size = 0;
        for (CORBA::ULong i = 0; i < in.length(); ++i) {
            size += in[i].length() * sizeof(CORBA::ULong);
        }

        if (size < TDAQ_EMON_COMPRESSION_THRESHOLD) {
            ERS_DEBUG(4, "Event is too small for compression (" << size << " B)");
            return false;
        }

        out.length(in.length());

        for (CORBA::ULong i = 0; i < in.length(); ++i) {
            if (!compress(in[i], out[i]))
                return false;
        }

        return true;
    }

    bool compress(const EventMonitoring::EventFragment &in, EventMonitoring::EventFragment &out) {
        CORBA::Octet *data = (CORBA::Octet*) in.get_buffer();
        uLongf uncompressed_size = in.length() * sizeof(CORBA::ULong);

        //////////////////////////////////////////////////////////////////
        // compressed buffer must be at least 0.1% larger
        // than sourceLen plus 12 bytes as suggested by zlib
        // plus sizeof(CORBA::ULong) bytes for ulong alignment
        // plus sizeof(CORBA::ULong) to store the plain size
        // plus sizeof(CORBA::ULong) to store the compressed size
        //////////////////////////////////////////////////////////////////
        uLongf compressed_size = (uLongf) ((float) uncompressed_size * 1.001 + 12
                + 3 * sizeof(CORBA::ULong));
        CORBA::Octet *compressed_data = CORBA::OctetSeq::allocbuf(compressed_size);

        //////////////////////////////////////////////////////////////////
        // compress with maximum speed level (1)
        //////////////////////////////////////////////////////////////////
        int err = ::compress2(compressed_data, &compressed_size, data, uncompressed_size, 1);
        if (err != Z_OK) {
            ERS_LOG("::compress2 returns error " << err);
            delete[] compressed_data;
            return false;
        };

        CORBA::ULong length = buffer_size(compressed_size);
        ERS_DEBUG(4, "event size = " << uncompressed_size << ", compressed event size = " << compressed_size);

        out.replace(length, length, (CORBA::ULong*) compressed_data, true);
        out[length - 2] = (CORBA::ULong) uncompressed_size;
        out[length - 1] = (CORBA::ULong) compressed_size;

        return true;
    }

    bool uncompress(const EventMonitoring::Event &in, EventMonitoring::Event &out) {
        out.length(1);

        if (in.length() == 1) {
            return uncompress(in[0], out[0]);
        }

        EventMonitoring::Event tmp;
        tmp.length(in.length());
        bool compressed = true;
        for (CORBA::ULong i = 0; i < in.length(); ++i) {
            if (!uncompress(in[i], tmp[i])) {
                compressed = false;
                break;
            }
        }

        serialize(compressed ? tmp : in, out);

        return true;
    }

    bool uncompress(const EventMonitoring::EventFragment &in, EventMonitoring::EventFragment &out) {
        CORBA::Octet *data = (CORBA::Octet*) in.get_buffer();
        CORBA::ULong length = in.length();

        if (length < 2) {
            // event is not compressed
            return false;
        }

        uLongf compressed_size = in[length - 1];
        uLongf uncompressed_size = in[length - 2];

        if (length != buffer_size(compressed_size)) {
            // event is not compressed
            return false;
        }
        //////////////////////////////////////////////////////////////////
        // allocate uncompressed buffer
        //////////////////////////////////////////////////////////////////
        CORBA::Octet *uncompressed_data = CORBA::OctetSeq::allocbuf(uncompressed_size);

        //////////////////////////////////////////////////////////////////
        // uncompress
        //////////////////////////////////////////////////////////////////
        int err = ::uncompress(uncompressed_data, &uncompressed_size, data, compressed_size);
        if (err != Z_OK) {
            ERS_LOG("::uncompress returns error " << err);
            delete[] uncompressed_data;
            return false;
        };

        length = uncompressed_size / sizeof(CORBA::ULong);
        out.replace(length, length, (CORBA::ULong*) uncompressed_data, true);
        ERS_DEBUG(4, "compressed event size = " << compressed_size
                << ", uncompressed event size = " << uncompressed_size);

        return true;
    }
}
